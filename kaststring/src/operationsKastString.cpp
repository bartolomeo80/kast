/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string> // string
#include <iostream> // cout
#include <fstream> // file streams
#include <sstream> // string streams
#include <algorithm> // count
#include <stdlib.h> // printf and exit
#include <vector> // vectors
#include <map> // maps

/// own libraries
#include "kaststring/operationsKastString.h" // own functions


/// namespace
using namespace std;

/**
function: extractBareRepresentation
description: extracts the bare tree structure
arguments:
@sourceString the string to be analyzed
returns the bare string
*/
string extractBareRepresentation(string sourceString)
{
    string targetString;
    for(long  i=0; i < (long)sourceString.length(); i++)
    {
        if(sourceString[i] == '(')
        {
            targetString += "(1"; // assume 1 as default weight
        }
        else if(sourceString[i] == ')')
        {
            targetString += ")";
        }
    }

    /// return the bare string
    return targetString;
}

/**
function: extractStringRepresentation
description: extracts the string tree structure without debug info
arguments:
@sourceString the string to be analyzed
returns the string
*/
string extractStringRepresentation(string sourceString)
{
    string targetString;
    bool skip = false;
    for(long  i=0; i < (long)sourceString.length(); i++)
    {
        if(sourceString[i] == ';') // if a semicolon is found, skip it
        {
            skip = true;
        }
        else if(sourceString[i] == '(')
        {
            skip = false;
        }
        else if(sourceString[i] == ')')
        {
            skip = false;
        }

        if(!skip)
        {
            targetString += sourceString[i];
        }
    }

    /// return the string structure
    return targetString;
}

/**
function: parseString
description: checks the validity of the tree
arguments:
@sourceString the string to be analyzed
@openChar opening bracket
@closeChar closing baracket
returns the string with possible errors at the end
*/
string parseString(string sourceString, char openChar, char closeChar)
{
    string errorString =  "";
    int balanceIndex = 0;
    char lastChar = openChar;
    for(unsigned int i=0; i < sourceString.length(); i++ )
    {
        if(sourceString[i] == openChar)
        {
            balanceIndex++;
        }
        else if(sourceString[i] == closeChar)
        {
            balanceIndex--;
        }
        else if(lastChar == closeChar)
        {
            errorString += "Unexpected token near to position " + static_cast<ostringstream*>( &(ostringstream() << i) )->str() + ";";
            break;
        }
        if(balanceIndex < 1 && i != sourceString.length() -1)
        {
            errorString += "Possible error near to position " + static_cast<ostringstream*>( &(ostringstream() << i) )->str() + ";";
            break;
        }
        lastChar = sourceString[i];
    }
    if(balanceIndex > 0)
    {
        errorString += "Some tokens are missing: " + static_cast<ostringstream*>( &(ostringstream() << balanceIndex) )->str() + ";";
    }
    else if(balanceIndex < 0)
    {
        errorString += "Too many tokens;";
    }

    /// give back the string plus the errors where they happened
    return sourceString + errorString;
}

/**
function: extractFlatRepresentation
description: extract Flat Representation
arguments:
@sourceString a string to analyze
@isIntegerValue the type of extraction
returns the strings for this handle
*/
string extractFlatRepresentation(string sourceString, bool isIntegerValue)
{
    /// variable preparation
    string targetString = "";
    bool skip = false;
    int levelUp = 0;
    long i = 0;

    /// tokens start with [
    if(!isIntegerValue)
    {
        targetString += "[";
    }

    /// loop string
    for(i = 0; i < (long)sourceString.length(); i++)
    {
        /// end of a token?
        if(sourceString[i] == ';')
        {
            /// do not write twice
            if(targetString.back() != ';' && isIntegerValue)
            {
                targetString += ";"; // separator
            }
            skip = true; // skip
        }
        else if(sourceString[i] == '(' && i > 0) // start of a level
        {
            /// tokens finish with ]
            if(!isIntegerValue)
            {
                targetString += "]";
            }

            /// do not write twice
            if(targetString.back() != ';')
            {
                targetString += ";"; // separator
            }

            /// tokens start with [
            if(!isIntegerValue)
            {
                targetString += "[";
            }
            skip = false; // do not skip

            /// register the level info
            if(i > 0 && levelUp > 0)
            {
                /// for integer values
                if(isIntegerValue)
                {
                    targetString += static_cast<ostringstream*>( &(ostringstream() << levelUp) )->str() + ";"; // register the level
                }
                else
                {
                    targetString += "___LEVEL_UP___];["; // close with a special token and open a token
                }
            }

            /// reset the level up flag
            levelUp = 0;
        }
        else if(sourceString[i] == ')') // end of a level
        {
            skip = true; // skip
            levelUp++; // level up
        }

        /// register the character
        if(!skip && sourceString[i] != '(')
        {
            targetString += sourceString[i];
        }
    }

    /// final level up registration
    if(i > 0 && levelUp > 0)
    {
        /// register the level up
        if(isIntegerValue)
        {
            targetString += ";" + static_cast<ostringstream*>( &(ostringstream() << levelUp) )->str() + ";"; // as a number
        }
        else
        {
            targetString += "];[___LEVEL_UP___];"; // as a token
        }
    }

    /// separator at the end
    if(targetString.back() != ';')
    {
        targetString += ";"; // separator
    }

    /// return the flat string
    return targetString;
}

/**
function: expandString
description: show a string as a tree
arguments:
@stringRepresentation target string
returns the tree-like string
*/
string expandString(string stringRepresentation)
{
    /// variable preparation
    string expandedRepresentation = "";
    int tabulation = -1;

    /// loop tokens
    for(int i = 0; i < (int)stringRepresentation.length(); i++)
    {
        /// a start of a node found
        if(stringRepresentation[i] == (int)'(')
        {
            tabulation++; // indent forward
            expandedRepresentation += "\n"; // line break
            for(int j=0; j < tabulation; j++)
            {
                expandedRepresentation += "   "; // print all indentation
            }
        }
        else if(stringRepresentation[i] == ')') // end of a node
        {
            expandedRepresentation += "\n"; // line break
            for(int j=0; j < tabulation; j++)
            {
                expandedRepresentation += "   "; // print all indentation
            }
            tabulation--; // indent backwards
        }
        expandedRepresentation += stringRepresentation[i]; // add the char
    }

    /// return the tree-like string
    return expandedRepresentation;
}

void compressRepresentation(string &flatRepresentation, string &bareRepresentation)
{
    string newFlatRepresentation = "";
    string newBareRepresentation = "";
    istringstream fr(flatRepresentation);
    string lastToken, tokenFr;
    istringstream br(bareRepresentation);
    string lastWeight, tokenBr;
    getline(fr, lastToken, ';');
    getline(br, lastWeight, ';');
    int accumulator = atoi(lastWeight.c_str());
    bool skip = false;
    if(lastToken == "[ForStmt]" || lastToken == "[WhileStmt]")
    {
        lastToken = "[LoopedStmt]";
    }
    while(getline(fr, tokenFr, ';') && getline(br, tokenBr, ';'))
    {
        if(tokenFr == "[ForStmt]" || tokenFr == "[WhileStmt]")
        {
            tokenFr = "[LoopedStmt]";
        }
        if(lastToken == tokenFr)
        {
            accumulator += atoi(tokenBr.c_str());
            skip = true;
        }
        else if(lastToken == "[ImplicitCastExpr]" || lastToken == "[ParenExpr]" || lastToken == "[CStyleCastExpr]" || lastToken == "[CallExpr]")
        {
            lastToken = tokenFr;
            accumulator += atoi(tokenBr.c_str());
            skip = true;
        }
        else if(lastToken == "[DeclStmt]" && tokenFr != "[___LEVEL_UP___]")
        {
            accumulator += atoi(tokenBr.c_str());
            skip = true;
        }
        else
        {
            skip = false;
        }
        if(!skip)
        {
            newFlatRepresentation += lastToken + ";";
            newBareRepresentation += static_cast<ostringstream*>( &(ostringstream() << accumulator) )->str() + ";";
            lastToken = tokenFr;
            lastWeight = tokenBr;
            accumulator = atoi(lastWeight.c_str());
        }
    }
    newFlatRepresentation += lastToken + ";";
    newBareRepresentation += static_cast<ostringstream*>( &(ostringstream() << accumulator) )->str() + ";";
    flatRepresentation = newFlatRepresentation;
    bareRepresentation = newBareRepresentation;
    newFlatRepresentation = "";
    newBareRepresentation = "";

    istringstream nfr(flatRepresentation);
    istringstream nbr(bareRepresentation);
    string lastToken1, lastToken2, lastToken3, lastToken4;
    int accumulator1, accumulator2, accumulator3, accumulator4;
    while(getline(nfr, tokenFr, ';') && getline(nbr, tokenBr, ';'))
    {
        lastToken1 = lastToken2;
        lastToken2 = lastToken3;
        lastToken3 = lastToken4;
        lastToken4 = tokenFr;
        accumulator1 = accumulator2;
        accumulator2 = accumulator3;
        accumulator3 = accumulator4;
        accumulator4 = atoi(tokenBr.c_str());
        if(lastToken1 != "" && lastToken2 != "" && lastToken3 != "" && lastToken4 != "")
        {
            if(lastToken1 == lastToken3 && lastToken2 == lastToken4 )
            {
                lastToken1 = "";
                lastToken2 = "";
                accumulator3 += accumulator1;
                accumulator4 += accumulator2;
            }
            else
            {
                newFlatRepresentation += lastToken1 + ";";
                newBareRepresentation += static_cast<ostringstream*>( &(ostringstream() << accumulator1) )->str() + ";";
                lastToken1 = "";
                accumulator1 = 0;
            }
        }
    }
    if(lastToken1 != "")
    {
        newFlatRepresentation += lastToken1 + ";";
        newBareRepresentation += static_cast<ostringstream*>( &(ostringstream() << accumulator1) )->str() + ";";
        lastToken1 = "";
        accumulator1 = 0;
    }
    if(lastToken2 != "")
    {
        newFlatRepresentation += lastToken2 + ";";
        newBareRepresentation += static_cast<ostringstream*>( &(ostringstream() << accumulator2) )->str() + ";";
        lastToken2 = "";
        accumulator2 = 0;
    }
    if(lastToken3 != "")
    {
        newFlatRepresentation += lastToken3 + ";";
        newBareRepresentation += static_cast<ostringstream*>( &(ostringstream() << accumulator3) )->str() + ";";
        lastToken3 = "";
        accumulator3 = 0;
    }
    if(lastToken4 != "")
    {
        newFlatRepresentation += lastToken4 + ";";
        newBareRepresentation += static_cast<ostringstream*>( &(ostringstream() << accumulator4) )->str() + ";";
        lastToken4 = "";
        accumulator4 = 0;
    }
    flatRepresentation = newFlatRepresentation;
    bareRepresentation = newBareRepresentation;
    newFlatRepresentation = "";
    newBareRepresentation = "";
}
