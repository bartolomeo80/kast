/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <sstream> // strean streasms
#include <fstream> // file streams

/// own libraries
#include "kaststring/ArgumentValidator.h" // argument validation
#include "kaststring/ClangASTTraversal.h" // traversal for LLVM AST
#include "kaststring/IRDumper.h" // dumper for LLVM IR

/// namespace
using namespace std;

/**
function: main
description: main
arguments:
@argc argument count
@argv argument array
returns ?
*/
int main(int argc, const char **argv)
{
    /// validate arguments
    ArgumentValidator argumentVal(argc,argv);
    if(!argumentVal.isValid())
    {
        return 0; // bye
    }

    /// generate string from the AST
    MyClangTool(argc, argv, argumentVal.getASTStringFileName());

    /// generate string from the IR
    IRDumper(argumentVal.getBitcodeFileName(), argumentVal.getIRStringFileName());

    /// terminate nicely
    return 0;
}
