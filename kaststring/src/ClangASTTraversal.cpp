/*
Project: K-AST-IR-IO
Integrated by: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string>
#include <sstream>
#include <fstream>
#include <iostream>

/// clang and llcm libraries
#include "clang/Tooling/CommonOptionsParser.h"
#include "llvm/Support/CommandLine.h"

/// own headers
#include "kaststring/ClangASTTraversal.h"
#include "kaststring/operationsKastString.h"
#include "ASTDumper.cpp"

/// namespaces
using namespace clang::tooling;
using namespace llvm;
using namespace std;

/// globals
string outGlobalFileName;

/// Apply a custom category to all command-line options so that they are the
/// only ones displayed.
static cl::OptionCategory MyToolCategory("my-tool options");

/// CommonOptionsParser declares HelpMessage with a description of the common
/// command-line options related to the compilation database and input files.
/// It's nice to have this help message in all tools.
static cl::extrahelp CommonHelp(CommonOptionsParser::HelpMessage);

/// A help message for this specific tool can be added afterwards.
static cl::extrahelp MoreHelp("\nMore help text...");

/**
function: CreateStringFromASTConsumer
description: constructor
arguments:
@Context parsing context of clang
@InFile the name of the parsed file
returns the object
*/
CreateStringFromASTConsumer::CreateStringFromASTConsumer(ASTContext *Context, StringRef InFile)
    :currentFile(InFile)
{

}

/**
function: HandleTranslationUnit
description: performs the traversal and builds the string from the AST
arguments:
@Context parsing context of clang
returns nothing
*/
void CreateStringFromASTConsumer::HandleTranslationUnit(clang::ASTContext &Context)
{
    /// instantiate an AST Dumper
    ASTDumper Dumper(llvm::outs(), nullptr, &Context.getSourceManager(), currentFile.str() );

    /// dump the info into strings, start from the root node
    Dumper.dumpDecl(Context.getTranslationUnitDecl());

    /// get the string info
    infoRepresentation = Dumper.SS.str();

    /// delete ghost chars
    infoRepresentation.erase(infoRepresentation.find_last_not_of(" \n\r\t")+1);

    /// convert to falt token representation
    bareRepresentation = extractFlatRepresentation(extractBareRepresentation(infoRepresentation),true);
    stringRepresentation = extractStringRepresentation(infoRepresentation);
    flatRepresentation = extractFlatRepresentation(infoRepresentation, false);
    expandedRepresentation = expandString(infoRepresentation); // show a tree

    /// compress representation
    string previousFlatRepresentation = "";
    do
    {
        previousFlatRepresentation = flatRepresentation;
        compressRepresentation(flatRepresentation, bareRepresentation);
    }
    while(previousFlatRepresentation != flatRepresentation);

    /// parse strings
    stringRepresentation = parseString(stringRepresentation,'(',')');
    infoRepresentation = parseString(infoRepresentation,'(',')');

    /// write to string file
    ofstream strFileStream;
    strFileStream.open (outGlobalFileName.c_str());
    strFileStream << bareRepresentation << endl << stringRepresentation << endl << flatRepresentation << endl << infoRepresentation << expandedRepresentation << endl;
    strFileStream.close();
}

/**
function: CreateASTConsumer
description: Create AST Consumer
arguments:
@Compiler compiler context
@InFile source file name
returns AST consumer
*/
std::unique_ptr<clang::ASTConsumer> CreateStringFromASTAction::CreateASTConsumer(clang::CompilerInstance &Compiler, llvm::StringRef InFile)
{
    /// return the consumer
    return std::unique_ptr<clang::ASTConsumer>(
        new CreateStringFromASTConsumer(&Compiler.getASTContext(),InFile));
}

/**
function: MyClangTool
description: uses the clang consumers to traverse an AST
arguments:
@argc argument count
@argv argument array
@outputFileName source file name
returns AST consumer
*/
int MyClangTool(int argc, const char **argv, string outputFileName)
{
    /// variable preparation
    outGlobalFileName = outputFileName;

    /// create a parser for the source code
    CommonOptionsParser OptionsParser(argc, argv, MyToolCategory);

    /// create a traversing tool using the parser data
    ClangTool Tool(OptionsParser.getCompilations(), OptionsParser.getSourcePathList());

    /// run the tool with an AST consumer and return
    return Tool.run(newFrontendActionFactory<CreateStringFromASTAction>().get());
}
