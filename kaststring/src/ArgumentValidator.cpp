/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include<iostream>

/// own include
#include "kaststring/ArgumentValidator.h"

/// namespace
using namespace std;

/**
function: ArgumentValidator
description: validates arguments
arguments:
@argc number of arguments
@argv array of arguments
returns the object
*/
ArgumentValidator::ArgumentValidator(int &argc, const char **argv)
{
    /// initialization
    valid = false;
    BitcodeFileName = "";
    ASTStringFileName= "";
    IRStringFileName= "";

    /// variable preparation
    string BitcodeRepresentation = "";
    string ASTStringRepresentation = "";
    string IRStringRepresentation = "";

    /// order is important, read the first argument after the llvm arguments
    string commandArgument1(argv[argc-6]);
    if(commandArgument1 == "-bc") // bit code file?
    {
        BitcodeRepresentation = commandArgument1;
        BitcodeFileName = argv[argc-5];
    }
    else // no
    {
        cout << "Bitcode representation argument (-bc) was not was specified" << endl ;
        printUsage();
        return; // bye
    }

    /// string file for AST?
    string commandArgument2(argv[argc-4]);
    if(commandArgument2 == "-aststr")
    {
        ASTStringRepresentation = commandArgument2;
        ASTStringFileName = argv[argc-3];
    }
    else // no
    {
        cout << "AST representation argument (-aststr) was not was specified" << endl ;
        printUsage();
        return; // bye
    }

    /// string file for IR?
    string commandArgument3(argv[argc-2]);
    if(commandArgument3 == "-irstr")
    {
        IRStringRepresentation = commandArgument3;
        IRStringFileName = argv[argc-1];
    }
    else // no
    {
        cout << "IR representation argument (-irstr was not was specified" << endl ;
        printUsage();
        return; // bye
    }

    /// no bit code file?
    if(BitcodeFileName == "")
    {
        cout << "Bitcode file argument was not specified" << endl ;
        printUsage();
        return; // bye
    }

    /// no string file for AST?
    if(ASTStringFileName == "")
    {
        cout << "AST String target file argument was not specified" << endl ;
        printUsage();
        return; // bye
    }

    /// no string file for IR?
    if(IRStringFileName == "")
    {
        cout << "IR String target file argument was not specified" << endl ;
        printUsage();
        return; // bye
    }

    /// delete the processed arguments, the remaining ones correspond to the LLVM functions
    argc = argc - 6;

    /// all valid
    valid = true;
}

/**
function: printUsage
description: print Usage
arguments: none
returns nothing
*/
void ArgumentValidator::printUsage()
{
    cout << "kaststring <file> -- <clang compiler options> -bc <Bitcode file> -aststr <AST string target file> -irstr <IR string target file>" << endl;
}

/**
function: getASTStringFileName
description: get AST String FileName
arguments: none
returns AST String File Name
*/
string ArgumentValidator::getASTStringFileName()
{
    return ASTStringFileName;
}

/**
function: getIRStringFileName
description: get IR String File Name
arguments: none
returns IR String File Name
*/
string ArgumentValidator::getIRStringFileName()
{
    return IRStringFileName;
}

/**
function: getBitcodeFileName
description: get Bitcode File Name
arguments: none
returns Bitcode File Name
*/
string ArgumentValidator::getBitcodeFileName()
{
    return BitcodeFileName;
}

/**
function: isValid
description: is Valid
arguments: none
returns valid or not
*/
bool ArgumentValidator::isValid()
{
    return valid;
}
