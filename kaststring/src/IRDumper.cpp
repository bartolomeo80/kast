/*
Project: K-AST-IR-IO
Integrated by: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <iostream>
#include <sstream>
#include <fstream>

/// llvm headers
#include <llvm/Support/MemoryBuffer.h>
#include <llvm/Support/ErrorOr.h>
#include <llvm/IR/Module.h>
#include <llvm/IR/LLVMContext.h>
#include <llvm/Bitcode/ReaderWriter.h>
#include <llvm/Support/raw_ostream.h>

/// own libraries
#include "kaststring/operationsKastString.h"

/// namespace
using namespace llvm;

/**
function: IRDumper
description: converts the IR into a string
arguments:
@BitcodeFileName bitcode file
@outGlobalFileName string file
returns ?
*/
int IRDumper(std::string BitcodeFileName, std::string outGlobalFileName)
{
    /// variable preparation
    std::stringstream SS;
    StringRef filename = BitcodeFileName.c_str();
    LLVMContext context;

    /// can I open the file?
    ErrorOr<std::unique_ptr<MemoryBuffer>> fileOrErr = MemoryBuffer::getFileOrSTDIN(filename);
    if (std::error_code ec = fileOrErr.getError())
    {
        std::cerr << " Error opening input file: " + ec.message() << std::endl;
        return 2;
    }

    /// is the file syntax correct?
    auto moduleOrErr = parseBitcodeFile(fileOrErr.get()->getMemBufferRef(), context);
    if (std::error_code ec = fileOrErr.getError())
    {
        std::cerr << "Error reading Moduule: " + ec.message() << std::endl;
        return 3;
    }

    /// loop each function
    auto m = moduleOrErr->get();
    SS << "(Module;0-0"; // module is the root of a file
    for (auto iter1 = m->getFunctionList().begin(); iter1 != m->getFunctionList().end(); iter1++)
    {
        Function &f = *iter1;
        SS << "(Function;0-0"; // create an entry for a function

        /// iterate basic blocks of a function
        for (auto iter2 = f.getBasicBlockList().begin(); iter2 != f.getBasicBlockList().end(); iter2++)
        {
            BasicBlock &bb = *iter2;
            SS << "(BasicBlock;0-0"; // create an entry for a basic block

            /// iterate instructions of a basic block
            for (auto iter3 = bb.begin(); iter3 != bb.end(); iter3++)
            {
                Instruction &inst = *iter3;
                SS << "(" << inst.getOpcodeName() << ";0-0"; // create an entry with the instruction name

                /// iterare the operands of an instruction
                unsigned int  i = 0;
                unsigned int opnt_cnt = inst.getNumOperands();
                for(; i < opnt_cnt; ++i)
                {
                    Value *opnd = inst.getOperand(i);
                    std::string o;

                    /// if the operand has a name
                    if (opnd->hasName())
                    {
                        SS << "(NamedOperand;0-0)"; // create an entry for a Named operand
                    }
                    else
                    {
                        SS << "(PointerOperand;0-0)"; // otherwise for a Pointer operand
                    }
                }
                SS << ")"; // close all levels
            }
            SS << ")";
        }
        SS << ")";
    }
    SS << ")";

    /// get the strings
    std::string infoRepresentation = SS.str(); // from the stream
    std::string bareRepresentation = extractFlatRepresentation(extractBareRepresentation(infoRepresentation), true); // the weight string
    std::string stringRepresentation = extractStringRepresentation(infoRepresentation); // the debug string
    std::string flatRepresentation = extractFlatRepresentation(infoRepresentation, false); // the tokenized string
    std::string expandedRepresentation = expandString(infoRepresentation); // show a tree

    /// delete ghost chars
    bareRepresentation.erase(bareRepresentation.find_last_not_of(" \n\r\t")+1);
    stringRepresentation.erase(stringRepresentation.find_last_not_of(" \n\r\t")+1);
    infoRepresentation.erase(infoRepresentation.find_last_not_of(" \n\r\t")+1);
    flatRepresentation.erase(flatRepresentation.find_last_not_of(" \n\r\t")+1);

    /// parse strings
    stringRepresentation = parseString(stringRepresentation,'(',')');
    infoRepresentation = parseString(infoRepresentation,'(',')');

    /// write them into the string file
    ofstream strFileStream;
    strFileStream.open (outGlobalFileName.c_str());
    strFileStream << bareRepresentation << endl << stringRepresentation << endl << flatRepresentation << endl << infoRepresentation << expandedRepresentation << endl;
    strFileStream.close();

    /// return
    return 0;
}
