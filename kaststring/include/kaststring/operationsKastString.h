/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string> // strings

/// namespace
using namespace std;

/**
function: extractBareRepresentation
description: extracts the bare tree structure
arguments:
@sourceString the string to be analyzed
returns the bare string
*/
string extractBareRepresentation(string sourceString);

/**
function: extractStringRepresentation
description: extracts the string tree structure without debug info
arguments:
@sourceString the string to be analyzed
returns the string
*/
string extractStringRepresentation(string sourceString);

/**
function: parseString
description: checks the validity of the tree
arguments:
@sourceString the string to be analyzed
@openChar opening bracket
@closeChar closing baracket
returns the string with possible errors at the end
*/
string parseString(string sourceString, char openChar, char closeChar);

/**
function: expandString
description: show a string as a tree
arguments:
@stringRepresentation target string
returns the tree-like string
*/
string expandString(string stringRepresentation);

/**
function: extractFlatRepresentation
description: extract Flat Representation
arguments:
@sourceString a string to analyze
@isIntegerValue the type of extraction
returns the strings for this handle
*/
string extractFlatRepresentation(string sourceString, bool isIntegerValue);

void compressRepresentation(string &flatRepresentation, string &bareRepresentation);

