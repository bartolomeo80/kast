/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string>

/// namespace
using namespace std;

/**
class: ArgumentValidator
description: validates arguments
*/
class ArgumentValidator
{
    private:
        string BitcodeFileName; // bit code file name
        string ASTStringFileName; // ast file name
        string IRStringFileName; // IR file name
        bool valid;
    public:
        ArgumentValidator(int &argc, const char **argv); // constructor
        void printUsage(); // print usage
        string getBitcodeFileName();
        string getASTStringFileName();
        string getIRStringFileName();
        bool isValid(); // valid arguments or not
};
