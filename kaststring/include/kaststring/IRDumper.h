/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// control defines
#ifndef IRDUMPER_H_INCLUDED
#define IRDUMPER_H_INCLUDED

/// libraries
#include <string>

/**
function: IRDumper
description: converts the IR into a string
arguments:
@BitcodeFileName bitcode file
@outGlobalFileName string file
returns ?
*/
int IRDumper(std::string BitcodeFileName, std::string outGlobalFileName);

#endif // IRDUMPER_H_INCLUDED
