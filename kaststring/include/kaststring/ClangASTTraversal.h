/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string>

/// clang and llvm libraries
#include "clang/AST/ASTConsumer.h"
#include "clang/AST/RecursiveASTVisitor.h"
#include "clang/Frontend/CompilerInstance.h"
#include "clang/Frontend/FrontendAction.h"
#include "clang/Tooling/Tooling.h"

/// namespaces
using namespace clang;
using namespace std;

/**
class: CreateStringFromASTConsumer
description: a custom consumer of ASTs
*/
class CreateStringFromASTConsumer : public clang::ASTConsumer
{
    private:
        StringRef currentFile; // the source file
        string outFileName; // the string file
        string bareRepresentation; // the weight string
        string stringRepresentation; // the normal string
        string infoRepresentation; // the debug string
        string flatRepresentation; // the flat token representation
        string expandedRepresentation; // the expanded tree
    public:
        CreateStringFromASTConsumer(ASTContext *Context, StringRef InFile); // constructor
        virtual void HandleTranslationUnit(clang::ASTContext &Context); // traverse function
};

/**
class: CreateStringFromASTAction
description: a custom action for ASTs
*/
class CreateStringFromASTAction : public clang::ASTFrontendAction
{
    public:
        virtual std::unique_ptr<clang::ASTConsumer> CreateASTConsumer(clang::CompilerInstance &Compiler, llvm::StringRef InFile); // action function
};

/**
function: MyClangTool
description: uses the clang consumers to traverse an AST
arguments:
@argc argument count
@argv argument array
@outputFileName source file name
returns AST consumer
*/
int MyClangTool(int argc, const char **argv, string outputFileName);


