/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string>

/// own headers
#include "kaststring-io/KastString-IO.h"

/// namespace
using namespace std;

/**
class: OperationList
description: a structure for the data of an operation
*/
class OperationList
{
    public:

        /// attributes
        string operation; // name
        string assumedBytes; // byte number
        string filehandle; // file handle number
        string realBytes; // byte number
        string FilePointer; // file pointer number
        string startLineNumber; // debug info
        string endLineNumber; // debug info
        size_t repetitions; // repetitions
        KastString representation; // strings

        /// functions
        OperationList(); // constructor
};
