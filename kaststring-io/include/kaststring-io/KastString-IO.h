/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string>

/// namespace
using namespace std;

/**
class: KastString
description: just a struct of 3 strings
*/
class KastString
{

public:

    /// attributes
    string bareRepresentation; // weights
    string stringRepresentation; // normal
    string infoRepresentation; // debug

    /// functions
    KastString(); // constructor

};
