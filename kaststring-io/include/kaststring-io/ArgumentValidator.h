/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string>

/// namespace
using namespace std;

/**
class: ArgumentValidator
description: validates arguments
*/
class ArgumentValidator
{
    private:
        string IOFilename; // IO acces pattern file
        string IOStringFileName; // string file
        string BytesFlag; // use or ignore bytes
        string MutateFlag;
        bool valid; // valid or not
    public:
        ArgumentValidator(int &argc, const char **argv); // constructor
        void printUsage(); // print usage
        string getIOFilename(); // get functions
        string getIOStringFileName();
        string getBytesFlag();
        string getMutateFlag();
        bool isValid(); // is it valid?
};
