/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string> // strings

/// namespace
using namespace std;

/**
function: extractFlatRepresentation
description: extract Flat Representation
arguments:
@sourceString a string to analyze
@isIntegerValue the type of extraction
returns the strings for this handle
*/
string extractFlatRepresentation(string sourceString, bool isIntegerValue);

/**
function: convertToString
description: converts an IO access pattern file to a string
arguments:
@sourceFileName IO file
@targetFileName string file
@generalizeAssumedBytes use or ignore bytes
@stringVariant generate normal string or a variant
*/
void convertToString(string sourceFileName, string targetFileName, string generalizeAssumedBytes, string stringVariant);
