/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// own libraries
#include "kaststring-io/OperationList.h"

/**
function: OperationList
description: constructor of an Operation List
arguments: none
returns an object OperationList
*/
OperationList::OperationList()
{
    /// initilaize members
    operation = "";
    assumedBytes = "0";
    filehandle = "";
    realBytes = "0";
    FilePointer = "";
    startLineNumber = "0";
    endLineNumber = "0";
    repetitions = 1;
}
