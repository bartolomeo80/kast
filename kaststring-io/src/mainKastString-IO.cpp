/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <sstream> // strean streasms
#include <fstream> // file streams
#include <stdlib.h> // exit and printf

/// own libraries
#include "kaststring-io/ArgumentValidator.h" // argument validation
#include "kaststring-io/operationsKastString-IO.h" // own functions

/// namespace
using namespace std;

/**
function: main
description: main
arguments:
@argc argument count
@argv argument array
returns ?
*/
int main(int argc, const char **argv)
{
    /// validate arguments
    ArgumentValidator argumentVal(argc,argv);
    if(!argumentVal.isValid())
    {
        return 0; //bye bye
    }

    /// convert to string
    convertToString(argumentVal.getIOFilename(), argumentVal.getIOStringFileName() + ".v0.str", argumentVal.getBytesFlag(), "normal");

    /// generate 4 mutated copies
    if(argumentVal.getMutateFlag() == "yes")
    {
        convertToString(argumentVal.getIOFilename(), argumentVal.getIOStringFileName() + ".v1.str", argumentVal.getBytesFlag(), "v1");
        convertToString(argumentVal.getIOFilename(), argumentVal.getIOStringFileName() + ".v2.str", argumentVal.getBytesFlag(), "v2");
        convertToString(argumentVal.getIOFilename(), argumentVal.getIOStringFileName() + ".v3.str", argumentVal.getBytesFlag(), "v3");
        convertToString(argumentVal.getIOFilename(), argumentVal.getIOStringFileName() + ".v4.str", argumentVal.getBytesFlag(), "v4");
    }

    /// go out nicely
    return 0;
}
