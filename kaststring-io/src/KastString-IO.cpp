/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// own headers
#include "kaststring-io/KastString-IO.h"

/**
function: KastString
description: initialization
arguments: none
returns a KastString object
*/
KastString::KastString()
{
    bareRepresentation = "";
    stringRepresentation = "";
    infoRepresentation = "";
}
