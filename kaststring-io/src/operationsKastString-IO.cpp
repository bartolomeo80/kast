/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string> // string
#include <iostream> // cout
#include <fstream> // file streams
#include <sstream> // string streams
#include <algorithm> // count
#include <stdlib.h> // printf and exit
#include <vector> // vectors
#include <map> // maps

/// own libraries
#include "kaststring-io/operationsKastString-IO.h" // own functions
#include "kaststring-io/OperationList.h" // own container class

/// namespace
using namespace std;

/**
function: extractFlatRepresentation
description: extract Flat Representation
arguments:
@sourceString a string to analyze
@isIntegerValue the type of extraction
returns the strings for this handle
*/
string extractFlatRepresentation(string sourceString, bool isIntegerValue)
{
    /// variable preparation
    string targetString = "";
    bool skip = false;
    int levelUp = 0;
    long i = 0;

    /// tokens start with [
    if(!isIntegerValue)
    {
        targetString += "[";
    }

    /// loop string
    for(i = 0; i < (long)sourceString.length(); i++)
    {
        /// end of a token?
        if(sourceString[i] == ';')
        {
            /// do not write twice
            if(targetString.back() != ';')
            {
                targetString += ";"; // separator
            }
            skip = true; // skip
        }
        else if(sourceString[i] == '(' && i > 0) // start of a level
        {
            /// tokens finish with ]
            if(!isIntegerValue)
            {
                targetString += "]";
            }

            /// do not write twice
            if(targetString.back() != ';')
            {
                targetString += ";"; // separator
            }

            /// tokens start with [
            if(!isIntegerValue)
            {
                targetString += "[";
            }
            skip = false; // do not skip

            /// register the level info
            if(i > 0 && levelUp > 0)
            {
                /// for integer values
                if(isIntegerValue)
                {
                    targetString += static_cast<ostringstream*>( &(ostringstream() << levelUp) )->str() + ";"; // register the level
                }
                else
                {
                    targetString += "___LEVEL_UP___];["; // close with a special token and open a token
                }
            }

            /// reset the level up flag
            levelUp = 0;
        }
        else if(sourceString[i] == ')') // end of a level
        {
            skip = true; // skip
            levelUp++; // level up
        }

        /// register the character
        if(!skip && sourceString[i] != '(')
        {
            targetString += sourceString[i];
        }
    }

    /// final level up registration
    if(i > 0 && levelUp > 0)
    {
        /// register the level up
        if(isIntegerValue)
        {
            targetString += ";" + static_cast<ostringstream*>( &(ostringstream() << levelUp) )->str() + ";"; // as a number
        }
        else
        {
            targetString += "];[___LEVEL_UP___];"; // as a token
        }
    }

    /// separator at the end
    if(targetString.back() != ';')
    {
        targetString += ";"; // separator
    }

    /// return the flat string
    return targetString;
}

/**
function: applyVariant
description: perform one of four possibilities of mutation on an operation list
arguments:
@operations operation vector
@stringVariant which variant
@handleCount handle
*/
void applyVariant(vector<OperationList> *operations, string stringVariant, int handleCount)
{
    /// apply only to vectors of an specific size and only to the first vector
    if(handleCount == 0 && operations->size() > 10)
    {
        /// variable preparation
        int deletionCount = 0;
        int insertionCount = 0;

        /// loop from the middle of the vector
        for(size_t i = operations->size()/2; i < operations->size(); i++ )
        {
            /// ignore control operations
            if(operations->at(i).operation != "BLOCK" && operations->at(i).operation != "CLOSE_BLOCK")
            {
                /// variants
                if(stringVariant == "v1") // 1, delete one operation
                {
                    operations->erase(operations->begin()+i);
                    break;
                }
                else if(stringVariant == "v2") // 2, clone an operation
                {
                    OperationList tempOperation = operations->at(i);
                    vector<OperationList>::iterator it = operations->begin() + i;
                    operations->insert(it, tempOperation);
                    break;
                }
                else if(stringVariant == "v3") // 3, delete two operations
                {
                    operations->erase(operations->begin()+i);
                    deletionCount++;
                    i--;
                    if(deletionCount > 1)
                    {
                        break;
                    }
                }
                else if(stringVariant == "v4") // 4, clone two operations
                {
                    OperationList tempOperation = operations->at(i);
                    vector<OperationList>::iterator it = operations->begin() + i;
                    operations->insert(it, tempOperation);
                    insertionCount++;
                    if(insertionCount > 1)
                    {
                        break;
                    }
                }
            }
        }
    }
}


/**
function: buildTreeExpressionFromOperations
description: build Tree Expression From Operations
arguments:
@operations a group of operations of the same file handle
returns the strings for this handle
*/
KastString buildTreeExpressionFromOperations(vector<OperationList> *operations)
{
    /// variable preparation
    KastString retString;
    string currentStatus;

    /// repeat refinement twice
    for(int cycle = 0; cycle < 2 ; cycle ++)
    {
        /// each stage has to come in the right order
        for(int stage = 0; stage < 4; stage++)
        {
            currentStatus = "start"; // flag

            /// loop the vector
            for(size_t j = 0; j < operations->size(); j++ )
            {
                /// count consequtive operations inside a block
                if(currentStatus == "block_started") // a block is aleady started
                {
                    if  (  // same operation same bytes (only in stage 0)
                        j > 0 && stage == 0  &&
                        operations->at(j).operation != "CLOSE_BLOCK" &&
                        operations->at(j).operation != "BLOCK" &&
                        operations->at(j-1).operation == operations->at(j).operation &&
                        operations->at(j-1).assumedBytes == operations->at(j).assumedBytes
                        )
                    {
                        operations->at(j-1).repetitions += operations->at(j).repetitions ;
                        operations->at(j-1).endLineNumber = operations->at(j).endLineNumber;
                        operations->erase(operations->begin()+j);
                        j--;
                    }
                    else if  ( // same operation different number (only in stage 1)
                        j > 0 && stage == 1  &&
                        operations->at(j).operation != "CLOSE_BLOCK" &&
                        operations->at(j).operation != "BLOCK" &&
                        operations->at(j-1).operation == operations->at(j).operation &&
                        operations->at(j-1).assumedBytes != operations->at(j).assumedBytes &&
                        operations->at(j-1).assumedBytes.find("-") == string::npos &&
                        operations->at(j).assumedBytes.find("-") == string::npos &&
                        operations->at(j-1).repetitions == 1 &&
                        operations->at(j).repetitions ==1
                        )
                    {
                        operations->at(j-1).assumedBytes = operations->at(j-1).assumedBytes + "-" + operations->at(j).assumedBytes; // summarize
                        operations->at(j-1).endLineNumber = operations->at(j).endLineNumber;
                        operations->erase(operations->begin()+j); // compact
                        j--;
                    }
                    else if  ( // different operation same number (only in stage 2)
                        j > 0 && stage == 2 &&
                        operations->at(j).operation != "CLOSE_BLOCK" &&
                        operations->at(j).operation != "BLOCK" &&
                        operations->at(j-1).operation != "CLOSE_BLOCK" &&
                        operations->at(j-1).operation != "BLOCK" &&
                        operations->at(j-1).assumedBytes == operations->at(j).assumedBytes &&
                        operations->at(j-1).operation.find("-") == string::npos && // "-" is the separator of two operations
                        operations->at(j).operation.find("-") == string::npos &&
                        operations->at(j-1).repetitions == 1 &&
                        operations->at(j).repetitions ==1
                        )
                    {
                        operations->at(j-1).operation = operations->at(j-1).operation + "-" + operations->at(j).operation; // summarize
                        operations->at(j-1).endLineNumber = operations->at(j).endLineNumber;
                        operations->erase(operations->begin()+j); // compact
                        j--;
                    }
                    else if  ( // different operation, one operation has zero as bytes (only in stage 3)
                        j > 0 && stage == 3 &&
                        operations->at(j).operation != "CLOSE_BLOCK" &&
                        operations->at(j).operation != "BLOCK" &&
                        operations->at(j-1).operation != "CLOSE_BLOCK" &&
                        operations->at(j-1).operation != "BLOCK" &&
                        operations->at(j-1).assumedBytes != operations->at(j).assumedBytes &&
                        ( operations->at(j-1).assumedBytes == "0" || operations->at(j).assumedBytes == "0" ) &&
                        operations->at(j-1).operation.find("-") == string::npos && // "-" is the separator of two operations
                        operations->at(j).operation.find("-") == string::npos &&
                        operations->at(j-1).repetitions == 1 &&
                        operations->at(j).repetitions ==1
                        )
                    {
                        /// use the non-zero values
                        string newAssumedBytes;
                        if(operations->at(j-1).assumedBytes != "0")
                        {
                            newAssumedBytes = operations->at(j-1).assumedBytes;
                        }
                        else
                        {
                            newAssumedBytes = operations->at(j).assumedBytes;
                        }
                        operations->at(j-1).operation = operations->at(j-1).operation + "-" + operations->at(j).operation; // sumarize
                        operations->at(j-1).endLineNumber = operations->at(j).endLineNumber;
                        operations->at(j-1).assumedBytes = newAssumedBytes; // update
                        operations->erase(operations->begin()+j); // compact
                        j--;
                    }

                }

                /// start a block?
                if(operations->at(j).operation == "BLOCK")
                {
                    currentStatus = "block_started"; // started
                }
                else if(operations->at(j).operation == "CLOSE_BLOCK") // close a block
                {
                    operations->erase(operations->begin()+j); // compact
                    j--;
                    currentStatus = "block_finished";
                }
                else if(currentStatus == "start" || currentStatus == "block_finished") // a block has not been started yet
                {
                    /// insert a block node
                    OperationList tempOperation;
                    tempOperation.operation = "BLOCK";
                    vector<OperationList>::iterator it = operations->begin() + j;
                    operations->insert(it, tempOperation);

                    /// make sure that new node is processed
                    j--;
                }
            }
        }
    }

    /// enclose all blocks nicely
    size_t i=0;
    currentStatus = "start";

    /// loop all nodes
    for(i = 0; i < operations->size(); i++ )
    {
        /// ignore a block without nodes
        if(operations->at(i).operation == "BLOCK" && i == operations->size() -1)
        {
            continue;
        }

        /// ignore a starting block before another block
        if(i > 0 && operations->at(i).operation == "BLOCK" && operations->at(i-1).operation == "BLOCK")
        {
            continue;
        }

        /// for the rest of the blocks
        if(operations->at(i).operation == "BLOCK")
        {
            /// already in a block?
            if(currentStatus != "start") // close the strings
            {
                retString.bareRepresentation += ")";
                retString.stringRepresentation +=")";
                retString.infoRepresentation += ")";
            }
            currentStatus = "block_started"; // establish a block started state
        }

        /// create two parts of the info
        string part1 = operations->at(i).operation + "_" + operations->at(i).assumedBytes;
        string part2 = operations->at(i).startLineNumber + "-" + operations->at(i).endLineNumber;

        /// write the strings
        retString.bareRepresentation += "(" + static_cast<ostringstream*>( &(ostringstream() << operations->at(i).repetitions) )->str(); // only weights
        retString.stringRepresentation += "(" + part1; // only tokens
        retString.infoRepresentation +=  "(" + part1 + ";" + part2; // tokens and debug info

        /// all non-block nodes are closed
        if(operations->at(i).operation != "BLOCK")
        {
            retString.bareRepresentation += ")";
            retString.stringRepresentation +=")";
            retString.infoRepresentation += ")";
        }
    }

    /// all the strings are closed
    if(retString.bareRepresentation != "")
    {
        retString.bareRepresentation += ")";
        retString.stringRepresentation +=")";
        retString.infoRepresentation += ")";
    }

    /// return the strings
    return retString;
}

/**
function: createStringFromIOAccessPattern
description: create String From IO Access Pattern
arguments:
@InputFileStreamPtr IO file stream
@generalizeAssumedBytes use bytes or not
@stringVariant variant
returns the strings of this file
*/
KastString createStringFromIOAccessPattern(ifstream *InputFileStreamPtr, string generalizeAssumedBytes, string stringVariant)
{
    /// variable preparation
    map< string, vector<OperationList> > operationsMap;
    map< string, string> filePointerMap;
    KastString IOKastString;
    string line;
    int startLineNumber = 0;
    string defaultAssumedBytes = "0";
    string defaultRepetitions = "1";

    /// loop all the operations in the IO access pattern file
    while ( getline (*InputFileStreamPtr,line) )
    {
        startLineNumber++; // guess line number
        istringstream ss(line); //
        OperationList currentOperation; //
        ss >> currentOperation.operation; // get the name of the operation

        /// discard some operations
        if(currentOperation.operation == "fileno" || currentOperation.operation == "mmap" || currentOperation.operation == "fscanf")
        {
            continue;
        }

        /// process the other tokens
        while(ss)
        {
            string sub;
            ss >> sub;

            /// register values
            if(sub.find("filehandle=") != string::npos) // a filehandle?
            {
                sub = sub.substr(sub.find("=") + 1); // extract only the filehandle number
                currentOperation.filehandle = sub;
            }
            else if(sub.find("BytesToWrite=") != string::npos || sub.find("BytesToRead=") != string::npos) // byte info?
            {
                /// use or ignore the byte number
                if(generalizeAssumedBytes == "ignore")
                {
                    currentOperation.assumedBytes = defaultAssumedBytes; // ignore
                }
                else
                {
                    sub = sub.substr(sub.find("=") + 1); // extract only the byte number
                    currentOperation.assumedBytes = sub; // use
                }
            }
            else if(sub.find("BytesWritten=") != string::npos || sub.find("BytesRead=") != string::npos) // byte info?
            {
                sub = sub.substr(sub.find("=") + 1); // extract only the byte number
                currentOperation.realBytes = sub;
            }
            else if(sub.find("FilePointer=") != string::npos) // file pointer?
            {
                sub = sub.substr(sub.find("=") + 1); // extract only the file pointer number
                currentOperation.FilePointer = sub;
            }
            else if(sub.find("count=") != string::npos) // count?
            {
                /// use or ignore the byte number
                if(generalizeAssumedBytes == "ignore")
                {
                    currentOperation.assumedBytes = defaultAssumedBytes; // ignore
                }
                else
                {
                    sub = sub.substr(sub.find("=") + 1); // extract only the byte number
                    currentOperation.assumedBytes = sub; // use
                }
            }
        }

        /// simplify and improve the representation
        if(currentOperation.operation == "open")
        {
            currentOperation.operation = "BLOCK";
        }
        else if(currentOperation.operation == "close" || currentOperation.operation == "fclose")
        {
            currentOperation.operation = "CLOSE_BLOCK";
        }
        else if(currentOperation.operation == "fopen")
        {
            /// some open operations come empty
            if (currentOperation.filehandle == "" || currentOperation.FilePointer == "")
            {
                continue;
            }

            /// if not empty, create a relationship between file handle and file pointer
            filePointerMap[currentOperation.FilePointer] = currentOperation.filehandle;
            currentOperation.operation = "BLOCK";
        }
        else if(currentOperation.filehandle == "" && currentOperation.FilePointer =="") // other operations might come with both fields empty
        {
            continue;
        }
        else if(currentOperation.FilePointer != "" && currentOperation.filehandle == "" ) // others lack a file handle but make reference to a file number
        {
            /// search for a proper filehandle
            currentOperation.filehandle = filePointerMap[currentOperation.FilePointer];
        }

        /// the operation should have a proper file handle now. If not, it is discarded
        if (currentOperation.filehandle != "")
        {
            /// preserve some debug info
            currentOperation.endLineNumber = currentOperation.startLineNumber = static_cast<ostringstream*>( &(ostringstream() << startLineNumber) )->str();

            /// if the file handle is new, create a group for it
            if (operationsMap.count(currentOperation.filehandle) == 0)
            {
                /// create a new vector for a new handle
                vector<OperationList> tempOperationVector;
                operationsMap[currentOperation.filehandle] = tempOperationVector ;
            }

            /// add the operation to its corresponding group
            operationsMap[currentOperation.filehandle].push_back(currentOperation);
        }
    }

    /// prepare strings
    IOKastString.bareRepresentation = "(" + defaultRepetitions; // root weight is always 1
    IOKastString.stringRepresentation = "(";
    IOKastString.infoRepresentation = "(";
    IOKastString.stringRepresentation += "ROOT_" + defaultAssumedBytes;
    IOKastString.infoRepresentation += "ROOT_" + defaultAssumedBytes + "_" + ";0-" +  static_cast<ostringstream*>( &(ostringstream() << startLineNumber) )->str();

    /// iterate each file handle group
    map< string, vector<OperationList> >::iterator ite; // iterator
    int handleCount = 0;
    for(ite = operationsMap.begin();ite != operationsMap.end();ite++)
    {
        /// apply variants (mutation)
        if(stringVariant != "normal")
        {
            applyVariant(&ite->second, stringVariant, handleCount);
        }

        /// get the string for this handle
        KastString retString = buildTreeExpressionFromOperations(&ite->second);

        /// enclose it nicely
        if(retString.bareRepresentation != "")
        {
            handleCount++;
            IOKastString.bareRepresentation += "(" + defaultRepetitions;
            IOKastString.stringRepresentation += "(HANDLE_" + defaultAssumedBytes;
            IOKastString.infoRepresentation += "(HANDLE_" + defaultAssumedBytes + ";0-0";
            IOKastString.bareRepresentation += retString.bareRepresentation;
            IOKastString.stringRepresentation += retString.stringRepresentation;
            IOKastString.infoRepresentation += retString.infoRepresentation;
            IOKastString.bareRepresentation += ")";
            IOKastString.stringRepresentation += ")";
            IOKastString.infoRepresentation += ")";
        }

    }

    /// close each string
    IOKastString.bareRepresentation += ")";
    IOKastString.stringRepresentation += ")";
    IOKastString.infoRepresentation += ")";

    /// return them back
    return IOKastString;
}

/**
function: expandString
description: show a string as a tree
arguments:
@stringRepresentation target string
returns the tree-like string
*/
string expandString(string stringRepresentation)
{
    /// variable preparation
    string expandedRepresentation = "";
    int tabulation = -1;

    /// loop tokens
    for(int i = 0; i < (int)stringRepresentation.length(); i++)
    {
        /// a start of a node found
        if(stringRepresentation[i] == (int)'(')
        {
            tabulation++; // indent forward
            expandedRepresentation += "\n"; // line break
            for(int j=0; j < tabulation; j++)
            {
                expandedRepresentation += "   "; // print all indentation
            }
        }
        else if(stringRepresentation[i] == ')') // end of a node
        {
            expandedRepresentation += "\n"; // line break
            for(int j=0; j < tabulation; j++)
            {
                expandedRepresentation += "   "; // print all indentation
            }
            tabulation--; // indent backwards
        }
        expandedRepresentation += stringRepresentation[i]; // add the char
    }

    /// return the tree-like string
    return expandedRepresentation;
}

/**
function: convertToString
description: converts an IO access pattern file to a string
arguments:
@sourceFileName IO file
@targetFileName string file
@generalizeAssumedBytes use or ignore bytes
@stringVariant generate normal string or a variant
*/
void convertToString(string sourceFileName, string targetFileName, string generalizeAssumedBytes, string stringVariant)
{
    /// prepare file stream
    ifstream fileStream (sourceFileName.c_str());
    if (!fileStream.is_open())
    {
        cout << "IO access pattern file "<< sourceFileName.c_str() <<" does not exist" << endl ;
        return; // bye
    }

    /// convert
    KastString IOKastString; // register 3 forms of strings
    IOKastString = createStringFromIOAccessPattern(&fileStream, generalizeAssumedBytes, stringVariant);

    /// clean file streams
    fileStream.close();

    /// format strings
    IOKastString.bareRepresentation = extractFlatRepresentation(IOKastString.bareRepresentation, true); // extract weights only
    string expandedRepresentation = expandString(IOKastString.infoRepresentation); // show a tree
    string flatRepresentation = extractFlatRepresentation(IOKastString.stringRepresentation, false); // extract tokens only

    /// write the strings on a file
    ofstream strFileStream;
    strFileStream.open (targetFileName.c_str());
    strFileStream   << IOKastString.bareRepresentation << endl << IOKastString.stringRepresentation << endl
                    << flatRepresentation << endl <<IOKastString.infoRepresentation << expandedRepresentation << endl;
    strFileStream.close();
}
