/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include<iostream>

/// own headers
#include "kaststring-io/ArgumentValidator.h"

///defines
#define MAX_ARGUMENTS 9

/// namespace
using namespace std;

/**
function: ArgumentValidator
description: validates arguments
arguments:
@argc number of arguments
@argv array of arguments
returns the object
*/
ArgumentValidator::ArgumentValidator(int &argc, const char **argv)
{
    /// initialization
    valid = false;
    MutateFlag = "";
    IOFilename = "";
    IOStringFileName = "";
    BytesFlag = "";

    /// variable preparation
    string IOFileOption = "";
    string IOStringFileOption = "";
    string BytesOption = "";
    string MutateOption = "";
    int argumentNum = MAX_ARGUMENTS; // expected number of arguments

    /// too few arguments
    if(argc < argumentNum)
    {
        cout << "Too few arguments. " << argumentNum - 1  << " expected" << endl ;
        return;
    }

    /// too many arguments
    if(argc > argumentNum)
    {
        cout << "Too many arguments. " << argumentNum - 1 << " expected" << endl ;
        return;
    }

    /// loop the arguments
    for(int i = 1;i < argc ; i = i + 2) // loop each two
    {
        string commandArgument(argv[i]); // convert to string
        if(commandArgument == "-io") // io file
        {
            IOFileOption = commandArgument; // get the option argument
            IOFilename = argv[i+1]; // get the name of the file
        }
        else if(commandArgument == "-str") // str file
        {
            IOStringFileOption = commandArgument;
            IOStringFileName = argv[i+1];
        }
        else if(commandArgument == "-b") // ignore or use bytes info
        {
            BytesOption = commandArgument;
            BytesFlag = argv[i+1];
        }
        else if(commandArgument == "-m") // mutate file
        {
            MutateOption = commandArgument;
            MutateFlag = argv[i+1];
        }
    }

    /// IO file is mandatory
    if(IOFileOption == "")
    {
        cout << "IO file argument (-io) was not specified" << endl ;
        return;
    }

    /// string file should be given
    if(IOStringFileOption == "")
    {
        cout << "String file argument (-str) was not specified" << endl ;
        return;
    }

    /// use or ignore bytes option should be given
    if(BytesOption == "")
    {
        cout << "Use or ignore bytes argument (-b) was not specified" << endl ;
        return;
    }

    /// mutate argument option should be given
    if(MutateOption == "")
    {
        cout << "Mutate argument (-m) was not specified" << endl ;
        return;
    }

    /// only here the arguments are valid
    valid = true;
}

/**
function: printUsage
description: print Usage
arguments: none
returns nothing
*/
void ArgumentValidator::printUsage()
{
    cout << "kaststring-io -io <I/O access patern file> -str <string target file> -b <bytes flag[use/ignore]> -m <mutate flag [yes/no]>" << endl;
}

/**
function: getIOFilename
description: get IO Filename
arguments: none
returns IO Filename
*/
string ArgumentValidator::getIOFilename()
{
    return IOFilename;
}

/**
function: getIOStringFileName
description: get IO String File Name
arguments: none
returns IO String File Name
*/
string ArgumentValidator::getIOStringFileName()
{
    return IOStringFileName;
}

/**
function: getBytesFlag
description: get Bytes Flag
arguments: none
returns Bytes Flag
*/
string ArgumentValidator::getBytesFlag()
{
    return BytesFlag;
}

/**
function: getMutateFlag
description: get Mutate Flag
arguments: none
returns Mutate Flag
*/
string ArgumentValidator::getMutateFlag()
{
    return MutateFlag;
}

/**
function: isValid
description: is Valid
arguments: none
returns valid or not
*/
bool ArgumentValidator::isValid()
{
    return valid;
}
