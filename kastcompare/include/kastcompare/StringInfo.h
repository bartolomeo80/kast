/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// standard libraries
#include <string>
#include <vector>

/// namespaces
using namespace std;

/**
class: StringInfo
description: string info
*/
class StringInfo
{
    public:
        /// attributes
        string originalString;
        string debugString;
        int totalWeight;
        int tokenCount;
        vector<int> weights;
        long productXX;
        string filename;

        /// functions
        void init(string fileName, int indexString, int indexWeights);
        string extractStringFromFile(int lineIndex);
};
