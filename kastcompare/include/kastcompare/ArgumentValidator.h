/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string>

/// namespace
using namespace std;

/**
class: ArgumentValidator
description: validates arguments
*/
class ArgumentValidator
{
    private:
        string listNameA; // list file
        string listNameB; // list file
        string cutWeight; // minimim weight
        string output; // verbose level
        string kernelName; // all blended k kast
        bool timeMeasured; // time
        bool valid; // are the arguments valid?
    public:
        ArgumentValidator(int argc, char **argv); // constructor
        string getListNameA(); //
        string getListNameB(); //
        string getCutWeight(); //
        string getOutput(); //
        string getKernelName(); //
        bool isTimeMeasured(); //
        bool isValid(); //
};

