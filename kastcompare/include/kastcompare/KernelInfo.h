/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// standard libraries
#include <string>
#include <vector>

/// namespaces
using namespace std;

/**
class: KernelInfo
description: kernel info
*/
class KernelInfo
{
    public:
        /// attributes
        vector<string> repeatedStrings;
        string type;
        long product;

        /// auxiliary attributes
        bool BMatchFound; // reset
        int independentWeightsA; // to store the weights of A
        int independentWeightsB; // and B
        int containedWeightsA; // to store the contained weights of A
        int containedWeightsB; // and B
        int allWeightsA; // to store the contained weights of A
        int allWeightsB; // and B
        int collectedWeightsA;
        int collectedWeightsB;
        int independentCountA;
        int independentCountB;
        int collectedCountA;
        int collectedCountB;
        int nonReplacedTokensA;
        int nonReplacedTokensB;

        /// functions
        KernelInfo();
};

