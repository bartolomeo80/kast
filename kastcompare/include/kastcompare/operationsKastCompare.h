/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// standard libraries
#include <string>
#include <map>

/// own headers
#include "kastcompare/StringInfo.h"
#include "kastcompare/KernelInfo.h"

/// namespace
using namespace std;

/// declarations

void getStringsInfo(string kernelName, vector<string> filesX, int exampleCountX, int cutWeight, string output, StringInfo *StringsX);

vector<string> getFileVector(string listName);

/**
function: printTime
description: print time
arguments: none
returns void
*/
void printTime();

/**
function: getTotalTokenWeight
description: obtain the weight
arguments:
@targetString weight string
returns the weight
*/
int getTotalTokenWeight(string &targetString);


/**
function: getSubBareString
description: obtain the weight substring
arguments:
@StringX string info
@subBigString substring
@position start position of the substring in the original one
returns the weight substring
*/
string getWeightSubstring(StringInfo &StringX, string &subBigString, int position);

/**
function: collectMatchesAndWeights
description: collect the info from a string, its matches and more
arguments:
@stringX original string info
@thisKernel the current kernel
@indexString store in A or B
@subString the substring
@sizeSubString its size
@cutWeight the cut value
@mode report all matches or restrict the matching
returns nothing
*/
void collectMatchesAndWeights(StringInfo &StringX,KernelInfo &thisKernel,string indexString,string subString,int sizeSubString,int cutWeight, string &copiedX, string mode);

/**
function: calculateKernels
description: performs the inner product calculation
arguments:
@kernelName name of the kernel all blended k kast
@StringA String object A
@StringB String object B
@currentTokenCount how many tokens to take into account
@cutWeight the minimum weight
@verbose no -> print basics, yes -> print debug info, vector -> print the similarity vector only
returns nothing
*/
void calculateKernels(string &shortStringA,string &shortStringB,vector<int> &weightsA,vector<int> &weightsB,  int tokenCountA, int tokenCountB, KernelInfo &KernelAB, int currentTokenCount, int cutWeight, string &copiedA, string &copiedB, string output);


/**
function: extractStringFromFile
description: extract String From File
arguments:
@fileName file
@lineIndex line number
returns the extracted string
*/
string extractStringFromFile(string fileName, int lineIndex);

int countTokens(string &s, int position);

std::map<string,string> buildDictionary(string &originalString);

string getShortString(string &originalString, map<string,string> &dictionary);
