/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

#include <sstream> // string stream functions
#include <stdlib.h>
#include <fstream> // file io
#include <iostream> // cout functions

/// own headers
#include "kastcompare/StringInfo.h"

/**
function: StringInfo
descriptor: constructor
arguments: none
returns the object
*/
void StringInfo::init(string fileName, int indexString, int indexWeights)
{
    debugString = "";
    totalWeight = 0;
    tokenCount = 0;
    productXX = 0;

    filename = fileName;
    originalString = extractStringFromFile(indexString);

    string weightString = extractStringFromFile(indexWeights);
    if(weightString == "")
    {
        exit(0);
    }
    stringstream ss(weightString); // make a stream
    string s; // collector string
    while (getline(ss, s, ';')) // tokenize
    {
        int thisWeight = atoi(s.c_str());
        totalWeight += thisWeight;
        tokenCount++;
        weights.push_back(thisWeight); // sum up all the weights
    }
}

/**
function: extractStringFromFile
description: extract String From File
arguments:
@fileName file
@lineIndex line number
returns the extracted string
*/
string StringInfo::extractStringFromFile(int lineIndex)
{
    string line; // the extracted line
    ifstream fileStream (filename.c_str()); // do a stream
    if (!fileStream.is_open()) // check validity
    {
        cout << "File does not exist" << endl ;
        exit(1); // bye bye
        return ""; // dummy
    }
    int currentLineNumber = 0; //
    while ( getline (fileStream,line) ) // read line
    {
        if(currentLineNumber == lineIndex) // skip to the wanted line
        {
            /// return solicited line
            return line;
        }
        currentLineNumber++;
    }

    /// empty
    return "";
}
