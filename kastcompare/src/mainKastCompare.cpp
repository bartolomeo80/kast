/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// standard libraries
#include <iostream> // cout functions
#include <algorithm> // count capabilities
#include <math.h> // basic math
#include <ctime> // measure time

/// own headers
#include "kastcompare/ArgumentValidator.h" // validation of arguments
#include "kastcompare/operationsKastCompare.h" // own functions

/// namespace
using namespace std;

/**
function: main
description: main
arguments:
@argc argument count
@argv argument pointer
returns exit status
*/
int main(int argc, char **argv)
{
    /// validate arguments
    ArgumentValidator argumentVal(argc,argv);
    if(!argumentVal.isValid())
    {
        return 0; //bye bye
    }

    /// time info
    time_t begin;
    if(argumentVal.isTimeMeasured())
    {
        begin = time(NULL);
    }

    /// process list files
    vector<string> filesA = getFileVector(argumentVal.getListNameA());
    vector<string> filesB = getFileVector(argumentVal.getListNameB());

    /// variable preparation
    string output = argumentVal.getOutput(); // how much detail level on the output
    string kernelName = argumentVal.getKernelName(); // all blended k kast
    int cutWeight = atol(argumentVal.getCutWeight().c_str()); // the minimum token count
    int exampleCountA =filesA.size();
    int exampleCountB =filesB.size();
    float matrixValues[exampleCountA][exampleCountB];

    /// extract strings and kernels againt themselvess
    StringInfo *StringsA = new StringInfo[exampleCountA];
    getStringsInfo(kernelName, filesA, exampleCountA, cutWeight, output, StringsA);
    StringInfo *StringsB = new StringInfo[exampleCountB];
    getStringsInfo(kernelName, filesB, exampleCountB, cutWeight, output, StringsB);

    std::map<string,string> dictionary;
    {
        string bigString = "";
        for (int iteA = 0; iteA < exampleCountA; iteA++) // get file name
        {
            bigString += StringsA[iteA].originalString;
        }
        for (int iteB = 0; iteB < exampleCountB; iteB++) // get file name
        {
            bigString += StringsB[iteB].originalString;
        }
        dictionary = buildDictionary(bigString);
    }

    #pragma omp parallel for schedule(dynamic,1) shared(matrixValues, exampleCountA, exampleCountB, output, cutWeight, StringsA, StringsB, kernelName) collapse(2)
    for (int iteA = 0; iteA < exampleCountA; iteA++) // get file name
    {
        for (int iteB = 0; iteB < exampleCountB; iteB++) // get file name
        {
            /// if file is compared agsainst itself
            if(StringsA[iteA].filename == StringsB[iteB].filename)
            {
                if(StringsA[iteA].totalWeight >= cutWeight && (kernelName == "kast" || kernelName == "kast1" || kernelName == "kast2"))
                {
                    matrixValues[iteA][iteB] = 1.0;
                    continue;
                }
                else if(StringsA[iteA].totalWeight <= cutWeight && (kernelName == "k" || kernelName == "blended"))
                {
                    matrixValues[iteA][iteB] = 1.0;
                    continue;
                }
            }


            string shortStringA = getShortString(StringsA[iteA].originalString, dictionary);
            string shortStringB = getShortString(StringsB[iteB].originalString, dictionary);
            string copiedA = shortStringA;
            string copiedB = shortStringB;

            /// create kernel package
            KernelInfo KernelAB;
            KernelAB.nonReplacedTokensA = StringsA[iteA].tokenCount;
            KernelAB.nonReplacedTokensB = StringsB[iteB].tokenCount;
            KernelAB.type = kernelName;

            /// prepare variables
            int maxTokenLimit = StringsA[iteA].tokenCount; //

            /// choose the smaller string token count window frame
            if(StringsA[iteA].tokenCount > StringsB[iteB].tokenCount)
            {
                maxTokenLimit = StringsB[iteB].tokenCount;
            }

            /// kernel A to B
            for(int c = maxTokenLimit; c > 0 ; c--)
            {
                /// compare and store values
                calculateKernels(shortStringA,shortStringB,StringsA[iteA].weights,StringsB[iteB].weights,StringsA[iteA].tokenCount,StringsB[iteB].tokenCount,KernelAB,c,cutWeight,copiedA,copiedB,output);
            }

            /// only for kernel kast2
            if(KernelAB.type == "kast2")
            {
                if(KernelAB.collectedCountA > 0 && KernelAB.collectedCountB > 0 )
                {
                    KernelAB.product =
                        (KernelAB.collectedWeightsA - KernelAB.collectedCountA + 1) *
                    (KernelAB.collectedWeightsB - KernelAB.collectedCountB + 1) ;
                }
                else
                {
                    KernelAB.product = 0;
                }
            }

            /// normalized values
            if(KernelAB.type  == "blended" || KernelAB.type  == "k")
            {
                matrixValues[iteA][iteB] = (float)KernelAB.product/(float)sqrt((float)StringsA[iteA].productXX*StringsB[iteB].productXX);
            }
            else if(KernelAB.type  == "kast" || KernelAB.type  == "kast1" || KernelAB.type  == "kast2")
            {
                matrixValues[iteA][iteB] = (float)KernelAB.product/(float)(StringsA[iteA].productXX*StringsB[iteB].productXX);

                /// values exceding the maximum are substracted
                if(matrixValues[iteA][iteB] > 1)
                {
                    matrixValues[iteA][iteB] = 1 + (1-matrixValues[iteA][iteB]);
                }

                /// values with negative similarity are normalized to zero
                if(matrixValues[iteA][iteB] < 0)
                {
                    matrixValues[iteA][iteB] = 0;
                }
            }
        }
    }

    delete [] StringsA;
    delete [] StringsB;

    if(output == "value")
    {
        cout.precision(10);
        for(int iteA = 0; iteA < exampleCountA; iteA++)
        {
            for(int iteB = 0; iteB < exampleCountB; iteB++)
            {
                cout << fixed << matrixValues[iteA][iteB] << " ";
            }
            cout << endl;
        }

    }

    /// time info
    if(argumentVal.isTimeMeasured())
    {
        time_t end = time(NULL);
        double elapsed_secs = double(end - begin);
        cout << elapsed_secs << " seconds" << endl;
    }

    /// everything ran smoothly
    return 0;
}
