/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// standard libraries
#include <iostream> // cout functions
#include <fstream> // file io
#include <sstream> // string stream functions
#include <algorithm> // count capabilities
#include <math.h> // basic math
#include <map>
#include <string>

/// own headers
#include "kastcompare/operationsKastCompare.h" // own functions

/// defines
#define STRING_INDEX 2 // where is the string
#define WEIGHT_INDEX 0 // where is the string

/// namespace
using namespace std;


void getStringsInfo(string kernelName, vector<string> filesX, int exampleCountX, int cutWeight, string output, StringInfo *StringsX)
{
    /// calculate kernels XX
    #pragma omp parallel for schedule(dynamic,1) shared(StringsX, exampleCountX, kernelName, filesX, cutWeight, output)
    for (int iteX = 0; iteX < exampleCountX; iteX++) // get file name
    {
        /// prepare string X
        StringsX[iteX].init(filesX[iteX], STRING_INDEX, WEIGHT_INDEX);

        /// create a kernel package
        KernelInfo KernelXX;
        KernelXX.type = kernelName;

        if(KernelXX.type == "blended" || KernelXX.type == "k")
        {
            std::map<string,string> dictionary = buildDictionary(StringsX[iteX].originalString);
            string shortStringX = getShortString(StringsX[iteX].originalString, dictionary);
            string copiedX1 = shortStringX;
            string copiedX2 = shortStringX;

            for(int c = StringsX[iteX].tokenCount; c > 0 ; c--)
            {
                /// compare and store values
                calculateKernels(shortStringX,shortStringX,StringsX[iteX].weights,StringsX[iteX].weights,StringsX[iteX].tokenCount,StringsX[iteX].tokenCount,KernelXX,c,cutWeight,copiedX1,copiedX2,output);
            }
        }
        else if(KernelXX.type == "kast" || KernelXX.type == "kast1" || KernelXX.type == "kast2")
        {
            KernelXX.product = StringsX[iteX].totalWeight;
        }

        StringsX[iteX].productXX = KernelXX.product;
    }
}


vector<string> getFileVector(string listName)
{
    vector<string> files;
    ifstream fileStream(listName.c_str()); // create a file stream from the list file
    if (!fileStream.is_open()) // valid?
    {
        cout << "Source file does not exist" << endl ;
        exit(0); // bye bye
    }

    string filename;
    while ( getline (fileStream,filename) ) // get file name
    {
        files.push_back(filename);
    }
    fileStream.close();
    return files;
}

/**
function: printTime
description: print time
arguments: none
returns void
*/
void printTime()
{
    time_t t = time(0); // get time now
    struct tm * now = localtime( & t ); // convert to local
    cout << (now->tm_year + 1900) << '-' // pretty printing
         << (now->tm_mon + 1) << '-'
         <<  now->tm_mday << ' '
         << now->tm_hour << ':'
         << now->tm_min << ':'
         << now->tm_sec
         << endl;
}

/**
function: getTotalTokenWeight
description: obtain the weight
arguments:
@targetString weight string
returns the weight
*/
int getTotalTokenWeight(string &targetString)
{
    stringstream ss(targetString); // make a stream
    string s; // collector string
    int sumation = 0; // initialize sum
    while (getline(ss, s, ';')) // tokenize
    {
        sumation += atoi(s.c_str()); // sum up all the weights
    }

    /// return the sum of the weights
    return sumation;
}

std::map<string,string> buildDictionary(string &originalString)
{
    stringstream ss(originalString); // make a stream
    string s; // collector string
    std::map <string,string> dictionary;
    int counter = 0;
    while (getline(ss, s, ';')) // tokenize
    {
        if(dictionary.find(s) == dictionary.end())
        {

            string str;          //The string
            ostringstream temp;  //temp as in temporary
            temp << counter;
            string newToken = "[" + temp.str() + "]";
            dictionary.insert (pair<string,string>(s,newToken));
            //cout << s << " " << newToken << endl;
            counter++;
        }
    }

    /// return the sum of the weights
    return dictionary;
}

string getShortString(string &originalString, map<string,string> &dictionary)
{
    string shortString = "";
    stringstream ss(originalString); // make a stream
    string s; // collector string
    while (getline(ss, s, ';')) // tokenize
    {
        shortString += dictionary[s] + ";";
    }

    /// return the sum of the weights
    return shortString;
}

int countTokens(string &s, int position)
{
    int count = 0;

    for (int i = 0; i < position; i++)
    {
      if (s[i] == ';')
      {
          count++;
      }
    }
    return count;
}

int positionAtToken(string &s, int tokenCount, int startPosition)
{
    int count = 0;
    int i = 0;

    for (i = 0; i < s.size() && count < tokenCount; i++)
    {
      if (s[i] == ';')
      {
          count++;
      }
    }
    return i;
}

/**
function: getSubBareString
description: obtain the weight substring
arguments:
@StringX string info
@subBigString substring
@position start position of the substring in the original one
returns the weight substring
*/
int getSubstringWeight(string &shortStringX, vector<int> &weightsX, string &subBigString, int subBigStringTokenCount, int position)
{
    int preBigStringTokenCount = countTokens(shortStringX,position);

    int sumation = 0;
    for(int i=preBigStringTokenCount; i < preBigStringTokenCount+subBigStringTokenCount; i++)
    {
        sumation += weightsX[i];
    }
    return sumation;
}

bool collectKast(string &shortStringX, vector<int> &weightsX, string &subString, int subStringTokenCount, size_t posx, int cutWeight, string &copiedX, size_t sizeSubString, int &independentWeightsX, int &containedWeightsX)
{
    bool foundX = false;
    int tokenWeightX = 0;
    do
    {
        tokenWeightX = getSubstringWeight(shortStringX, weightsX,subString,subStringTokenCount,posx); // get the weight of X

        /// weight is correct
        if(tokenWeightX >= cutWeight)
        {
            foundX = true;
            /// if not registered yet
            if (copiedX.substr(posx,sizeSubString) == subString)
            {
                /// register match
                independentWeightsX += tokenWeightX; // collect independent weights of X
                copiedX.replace(posx,sizeSubString,sizeSubString,'_'); // delete the ocurrence to avoid substrings matching
            }
            else // already registered
            {
                containedWeightsX += tokenWeightX; // collect contained weights
            }
        }
        posx = posx + sizeSubString; // search after the last match
    }while(( posx = shortStringX.find(subString, posx)) != string::npos);

    return foundX;
}

bool collectKast1(string &shortStringX, vector<int> &weightsX, string &subString, int subStringTokenCount, size_t posx, int cutWeight, string &copiedX, size_t sizeSubString, int &independentWeightsX, int &nonReplacedTokensX)
{
    bool foundX = false;
    int tokenWeightX = 0;
    size_t oldposx = posx;
    if(nonReplacedTokensX >= subStringTokenCount)
    {
        while(( posx = copiedX.find(subString, posx)) != string::npos)
        {
            tokenWeightX = getSubstringWeight(shortStringX, weightsX, subString,subStringTokenCount,posx); // get the weight of X

            /// weight is correct
            if(tokenWeightX >= cutWeight)
            {
                foundX = true;
                /// if not registered yet
                /// register match
                independentWeightsX += tokenWeightX; // collect independent weights of X
                copiedX.replace(posx,sizeSubString,sizeSubString,'_'); // delete the ocurrence to avoid substrings matching
                nonReplacedTokensX -= subStringTokenCount;
            }

            posx = posx + sizeSubString; // search after the last match
        }
    }
    if(foundX == false)
    {
        posx = oldposx;
        while(( posx = shortStringX.find(subString, posx)) != string::npos)
        {
            tokenWeightX = getSubstringWeight(shortStringX, weightsX, subString,subStringTokenCount,posx);

            /// weight is correct
            if(tokenWeightX >= cutWeight)
            {
                foundX = true;
                break;
            }
            posx = posx + sizeSubString; // search after the last match
        }
    }
    return foundX;
}

bool collectKast2(string &shortStringX, vector<int> &weightsX, string &subString, int subStringTokenCount, size_t posx, int cutWeight, string &copiedX, size_t sizeSubString, int &independentWeightsX, int &independentCountX, int &nonReplacedTokensX)
{
    bool foundX = false;
    int tokenWeightX = 0;
    size_t oldposx = posx;
    if(nonReplacedTokensX >= subStringTokenCount)
    {
        while(( posx = copiedX.find(subString, posx)) != string::npos)
        {
            tokenWeightX = getSubstringWeight(shortStringX, weightsX, subString,subStringTokenCount,posx); // get the weight of X

            /// weight is correct
            if(tokenWeightX >= cutWeight)
            {
                foundX = true;
                /// register match
                independentWeightsX += tokenWeightX; // collect independent weights of X
                copiedX.replace(posx,sizeSubString,sizeSubString,'_'); // delete the ocurrence to avoid substrings matching
                nonReplacedTokensX -= subStringTokenCount;
                independentCountX++;
            }
            posx = posx + sizeSubString; // search after the last match
        }
    }
    if(foundX == false)
    {
        posx = oldposx;
        while(( posx = shortStringX.find(subString, posx)) != string::npos)
        {
            tokenWeightX = getSubstringWeight(shortStringX, weightsX, subString,subStringTokenCount,posx);
            /// weight is correct
            if(tokenWeightX >= cutWeight)
            {
                foundX = true;
                break;
            }
            posx = posx + sizeSubString; // search after the last match
        }
    }

    return foundX;
}

bool collectBlended(string &shortStringX, vector<int> &weightsX, string &subString, int subStringTokenCount, size_t posx, int cutWeight, string &copiedX, size_t sizeSubString, int &allWeightsX, int &counter, string &mode, KernelInfo &thisKernel)
{
    bool foundX = false;
    int tokenWeightX = 0;
    do
    {
        tokenWeightX = getSubstringWeight(shortStringX,weightsX,subString,subStringTokenCount,posx); // get the weight of X
        if(tokenWeightX <= cutWeight)
        {
            foundX = true;
            /// restricted mode
            if(mode == "restricted")
            {
                /// take into account only if it was not found already
                if(find(thisKernel.repeatedStrings.begin(),thisKernel.repeatedStrings.end(),subString) == thisKernel.repeatedStrings.end())
                {
                    allWeightsX += tokenWeightX; // collect all
                    counter++; // how many cases
                }
                else // already found
                {
                    break; // stop the search
                }
            }
            else // not restricted
            {
                allWeightsX += tokenWeightX; // collect all
            }
        }
        posx = posx + sizeSubString; // search after the last match
    }while(( posx = shortStringX.find(subString, posx)) != string::npos);

    return foundX;
}

bool collectK(string &shortStringX,vector<int> &weightsX, string &subString, int &subStringTokenCount, size_t &posx, int &cutWeight, string &copiedX, size_t &sizeSubString, int &allWeightsX, int &counter, string &mode, KernelInfo &thisKernel)
{
    bool foundX = false;
    size_t lastPosx = 0;
    int tokenWeightX = 0;
    do
    {
        tokenWeightX = getSubstringWeight(shortStringX,weightsX,subString,subStringTokenCount,posx); // get the weight of X
        /// weight is correct
        if(tokenWeightX == cutWeight)
        {
            foundX = true;
            /// restricted mode
            if(mode == "restricted")
            {
                /// take into account only if it was not found already
                if(find(thisKernel.repeatedStrings.begin(),thisKernel.repeatedStrings.end(),subString) == thisKernel.repeatedStrings.end())
                {
                    allWeightsX += tokenWeightX; // collect all
                    counter++; // how many
                }
                else // already in database
                {
                    break; // stop the search
                }
            }
            else // unrestricted mode
            {
                allWeightsX += tokenWeightX; // collect all
            }
        }
        lastPosx = posx + sizeSubString; // search after the last match
    }while(( posx = shortStringX.find(subString, lastPosx)) != string::npos);

    return foundX;
}



/**
function: collectMatchesAndWeights
description: collect the info from a string, its matches and more
arguments:
@stringX original string info
@thisKernel the current kernel
@indexString store in A or B
@subString the substring
@sizeSubString its size
@cutWeight the cut value
@mode report all matches or restrict the matching
returns nothing
*/
bool collectMatchesAndWeights(string &shortStringX, vector<int> &weightsX,KernelInfo &thisKernel,string indexString, string &subString,int subStringTokenCount,size_t sizeSubString,int cutWeight,string &copiedX, string mode, size_t lastPosx)
{
    int nonReplacedTokensX = 0;

    if(indexString == "B")
    {
        if(( lastPosx = shortStringX.find(subString, lastPosx)) == string::npos)
        {
            thisKernel.containedWeightsB = 0;
            thisKernel.independentWeightsB = 0;
            thisKernel.independentCountB = 0;
            thisKernel.allWeightsB = 0;
            return false;
        }
        nonReplacedTokensX = thisKernel.nonReplacedTokensB;
    }
    else if(indexString == "A")
    {
        nonReplacedTokensX = thisKernel.nonReplacedTokensA;
    }

    bool foundX = false;
    int allWeightsX = 0;
    int independentWeightsX = 0;
    int containedWeightsX = 0;
    int independentCountX = 0;


    if(thisKernel.type == "kast") // our kernel
    {
        foundX = collectKast(shortStringX, weightsX, subString, subStringTokenCount, lastPosx, cutWeight, copiedX, sizeSubString, independentWeightsX, containedWeightsX);
    }
    else if(thisKernel.type == "kast1") // our kernel
    {
        foundX = collectKast1(shortStringX, weightsX, subString, subStringTokenCount, lastPosx, cutWeight, copiedX, sizeSubString, independentWeightsX, nonReplacedTokensX);
    }
    else if(thisKernel.type == "kast2") // our kernel
    {
        foundX = collectKast2(shortStringX, weightsX, subString, subStringTokenCount, lastPosx, cutWeight, copiedX, sizeSubString, independentWeightsX, independentCountX, nonReplacedTokensX);
    }
    else if(thisKernel.type == "blended")
    {
        int counter = 0;

        foundX = collectBlended(shortStringX, weightsX, subString, subStringTokenCount, lastPosx, cutWeight, copiedX, sizeSubString,allWeightsX, counter, mode, thisKernel);

        if(counter > 1)
        {
            thisKernel.repeatedStrings.push_back(subString); // add it to the database
        }
    }
    else if(thisKernel.type == "k")
    {
        int counter = 0;

        foundX = collectK(shortStringX, weightsX, subString, subStringTokenCount, lastPosx, cutWeight, copiedX, sizeSubString,allWeightsX, counter, mode, thisKernel);

        if(counter > 1)
        {
            thisKernel.repeatedStrings.push_back(subString); // add it to the database
        }
    }
    if(indexString == "A")
    {
        thisKernel.allWeightsA = allWeightsX;
        thisKernel.independentWeightsA = independentWeightsX;
        thisKernel.independentCountA = independentCountX;
        thisKernel.containedWeightsA = containedWeightsX;
        thisKernel.nonReplacedTokensA = nonReplacedTokensX;
    }
    else if(indexString == "B")
    {
        thisKernel.allWeightsB = allWeightsX;
        thisKernel.independentWeightsB = independentWeightsX;
        thisKernel.independentCountB = independentCountX;
        thisKernel.containedWeightsB = containedWeightsX;
        thisKernel.nonReplacedTokensB = nonReplacedTokensX;
    }
    return foundX;
}

/**
function: calculateKernels
description: performs the inner product calculation
arguments:
@kernelName name of the kernel all blended k kast
@StringA String object A
@StringB String object B
@currentTokenCount how many tokens to take into account
@cutWeight the minimum weight
@verbose no -> print basics, yes -> print debug info, vector -> print the similarity vector only
returns nothing
*/
void calculateKernels(string &shortStringA,string &shortStringB,vector<int> &weightsA,vector<int> &weightsB,  int tokenCountA, int tokenCountB, KernelInfo &KernelAB, int currentTokenCount, int cutWeight, string &copiedA, string &copiedB, string output)
{
    /// variable preparation
	size_t frontTokenStartPosition = 0; // front token start position
	size_t backTokenEndPosition = 0 ; // back token end position ;
	string subString = ""; // substring
    size_t sizeSubString = 0; // substring size
    int remainingTokens = tokenCountA;

	/// search for strings with a given efective token size
	do
	{
	    if(remainingTokens < currentTokenCount)
        {
            break;
        }
        /// prepare variables
        backTokenEndPosition = frontTokenStartPosition; // shift to the next token start

        /// check if there are enough tokens
        for(int i = 0; i < currentTokenCount; i++)
        {
            backTokenEndPosition = shortStringA.find(";", backTokenEndPosition + 1);
        }

        if(backTokenEndPosition == string::npos)
        {
            break;
        }

        /// now the string has the desired number of tokens, extract the substring
        sizeSubString = backTokenEndPosition - frontTokenStartPosition + 1; // calculate size in chars
        subString = shortStringA.substr(frontTokenStartPosition,sizeSubString); // extract the substring from A

        /// reject substrings larger as B
        if(subString.length() > shortStringB.length())
        {
            continue; // check next string
        }

        /// now the string has an acceptable size

        /// calculate the first weight of the substring in string A

        int tokenWeightReference = getSubstringWeight(shortStringA, weightsA, subString, currentTokenCount,frontTokenStartPosition); // get the sum of real tokens

        if(KernelAB.type  == "k")
        {
            /// blended spectrum kernel
            if(tokenWeightReference == cutWeight)
            {
                /// search for the all the ocurrences of substring in A
                if(collectMatchesAndWeights(shortStringB,weightsB,KernelAB,"B", subString,currentTokenCount,sizeSubString,cutWeight,copiedB, "unrestricted", 0)) // but only if there were matches in B
                {
                    /// continue searching in A
                    collectMatchesAndWeights(shortStringA,weightsA,KernelAB,"A",subString, currentTokenCount,sizeSubString,cutWeight, copiedA,"restricted", frontTokenStartPosition);

                    KernelAB.product += KernelAB.allWeightsA * KernelAB.allWeightsB;

                }
            }
        }
        else if(KernelAB.type  == "blended")
        {
            /// blended spectrum kernel
            if(tokenWeightReference <= cutWeight)
            {
                /// search for the all the ocurrences of substring in A
                if(collectMatchesAndWeights(shortStringB,weightsB,KernelAB,"B", subString,currentTokenCount,sizeSubString,cutWeight,copiedB, "unrestricted", 0)) // but only if there were matches in B
                {
                    /// continue searching in A
                    collectMatchesAndWeights(shortStringA,weightsA,KernelAB,"A",subString, currentTokenCount,sizeSubString,cutWeight, copiedA,"restricted", frontTokenStartPosition);

                    KernelAB.product += KernelAB.allWeightsA * KernelAB.allWeightsB;

                }
            }
        }
        else if(KernelAB.type  == "kast")
        {
            /// kast kernel
            if(tokenWeightReference >= cutWeight)
            {
                /// search for the all the ocurrences of substring in A
                if(collectMatchesAndWeights(shortStringB,weightsB,KernelAB,"B",subString,currentTokenCount,sizeSubString,cutWeight,copiedB, "indiferent", 0)) // but only if there were matches in B
                {
                    /// continue searching in A
                    collectMatchesAndWeights(shortStringA,weightsA,KernelAB,"A",subString,currentTokenCount,sizeSubString,cutWeight, copiedA, "indiferent", frontTokenStartPosition);

                    /// our kernel only uses these values if the substring was at least once independent
                    if(KernelAB.independentWeightsA > 0 || KernelAB.independentWeightsB > 0)
                    {
                        KernelAB.product += (KernelAB.independentWeightsA + KernelAB.containedWeightsA)
                            * (KernelAB.independentWeightsB + KernelAB.containedWeightsB);;
                    }
                }
            }
        }
        else if(KernelAB.type  == "kast1")
        {
            /// kast kernel
            if(tokenWeightReference >= cutWeight)
            {
                /// search for the all the ocurrences of substring in A
                if(collectMatchesAndWeights(shortStringB,weightsB,KernelAB,"B",subString,currentTokenCount,sizeSubString,cutWeight,copiedB, "indiferent", 0)) // but only if there were matches in B
                {
                    /// continue searching in A
                    collectMatchesAndWeights(shortStringA,weightsA,KernelAB,"A",subString,currentTokenCount,sizeSubString,cutWeight, copiedA, "indiferent", frontTokenStartPosition);

                    if(KernelAB.independentWeightsA == 0 && KernelAB.independentWeightsB > 0)
                    {
                        /// our kernel only uses these values if the substring was at least once independent
                        KernelAB.product += KernelAB.independentWeightsB;
                    }
                    else if(KernelAB.independentWeightsB == 0 && KernelAB.independentWeightsA > 0)
                    {
                        /// our kernel only uses these values if the substring was at least once independent
                        KernelAB.product += KernelAB.independentWeightsA;
                    }
                    else
                    {
                        /// our kernel only uses these values if the substring was at least once independent
                        KernelAB.product += KernelAB.independentWeightsA*KernelAB.independentWeightsB;
                    }
//                    if(output == "debug")
//                    {
//                        if(KernelAB.independentWeightsA > 0 || KernelAB.independentWeightsB > 0)
//                        {
//                            cout << "A: " << KernelAB.independentWeightsA << endl;
//                            cout << "B: " << KernelAB.independentWeightsB << endl;
//                            cout << "A X B: "<< KernelAB.product << endl;
//                            cout << "String: " << subString << endl;
//                            cout << "Gapped A: "<< copiedA << endl;
//                            cout << "Gapped B: "<< copiedB << endl << endl;
//                        }
//                    }
                }
            }
        }
        else if(KernelAB.type  == "kast2")
        {
            /// kast kernel
            if(tokenWeightReference >= cutWeight)
            {
                /// search for the all the ocurrences of substring in A
                if(collectMatchesAndWeights(shortStringB,weightsB,KernelAB,"B",subString,currentTokenCount,sizeSubString,cutWeight,copiedB, "indiferent", 0)) // but only if there were matches in B
                {
                    /// continue searching in A
                    collectMatchesAndWeights(shortStringA,weightsA,KernelAB,"A",subString,currentTokenCount,sizeSubString,cutWeight, copiedA, "indiferent", frontTokenStartPosition);

                    KernelAB.collectedWeightsA += KernelAB.independentWeightsA;
                    KernelAB.collectedWeightsB += KernelAB.independentWeightsB;
                    KernelAB.collectedCountA += KernelAB.independentCountA;
                    KernelAB.collectedCountB += KernelAB.independentCountB;
//                    if(output == "debug")
//                    {
//                        if(KernelAB.independentWeightsA > 0 || KernelAB.independentWeightsB > 0)
//                        {
//                            cout << "A: " << KernelAB.collectedWeightsA << endl;
//                            cout << "B: " << KernelAB.collectedWeightsB << endl;
//                            cout << "Count A: "<< KernelAB.collectedCountA << endl;
//                            cout << "Count B: "<< KernelAB.collectedCountB << endl;
//                            cout << "String: " << subString << endl;
//                            cout << "Gapped A: "<< copiedA << endl;
//                            cout << "Gapped B: "<< copiedB << endl << endl;
//                        }
//                    }

                }
            }
        }
        remainingTokens--;
    }
    while((frontTokenStartPosition = shortStringA.find(";",frontTokenStartPosition) + 1) < string::npos); // if there are enouh tokens
}


