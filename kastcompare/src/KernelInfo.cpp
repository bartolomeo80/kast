/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// own headers
#include "kastcompare/KernelInfo.h"

/**
function: KernelInfo
descriptor: constructor
arguments: none
returns the object
*/
KernelInfo::KernelInfo()
{
    /// kernel attributes
    type = "";
    product = 0;

    /// auxiliary attributes
    BMatchFound = false; // reset
    independentWeightsA = 0; // to store the weights of A
    independentWeightsB = 0; // and B
    containedWeightsA = 0; // to store the contained weights of A
    containedWeightsB = 0; // and B
    allWeightsA = 0; // to store the contained weights of A
    allWeightsB = 0; // and B
    collectedWeightsA = 0;
    collectedWeightsB = 0;
    independentCountA = 0;
    independentCountB = 0;
    collectedCountA = 0;
    collectedCountB = 0;
    nonReplacedTokensA = 0;
    nonReplacedTokensB = 0;
}
