/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// standard libraries
#include<iostream> // cout functions
#include<sstream> // string stream functions

/// own headers
#include "kastcompare/ArgumentValidator.h" // validation of arguments

/// defines
#define MAX_ARGUMENTS 13 // each new argument increases this amount by 2

/// namespace
using namespace std;

/**
function: ArgumentValidator
description: validates arguments
arguments:
@argc number of arguments
@argv array of arguments
returns the object
*/
ArgumentValidator::ArgumentValidator(int argc, char **argv)
{
    /// initialize members
    listNameA = "";
    listNameB = "";
    cutWeight = "";
    kernelName = "";
    valid = false; // by default non valid
    timeMeasured = false; //

    /// variable preparation
    string listOptionA = "";
    string listOptionB = "";
    string cutWeightOption = "";
    string outputOption = "";
    string kernelNameOption = "";
    string timeOption = "";
    int argumentNum = MAX_ARGUMENTS; // expected number of arguments

    /// too few arguments
    if(argc < argumentNum)
    {
        cout << "Too few arguments. " << argumentNum - 1  << " expected" << endl ;
        return;
    }

    /// too many arguments
    if(argc > argumentNum)
    {
        cout << "Too many arguments. " << argumentNum << " expected" << endl ;
        return;
    }

    /// loop the arguments
    for(int i = 1;i < argc ; i = i + 2) // loop each two
    {
        string commandArgument(argv[i]); // convert to string
        if(commandArgument == "-A") // or list file
        {
            listOptionA = commandArgument;
            listNameA = argv[i+1];
        }
        else if(commandArgument == "-B") // or list file
        {
            listOptionB = commandArgument;
            listNameB = argv[i+1];
        }
        else if(commandArgument == "-c") // minimum weight
        {
            cutWeightOption = commandArgument;
            cutWeight = argv[i+1];
        }
        else if(commandArgument == "-o") // verbose level
        {
            outputOption = commandArgument;
            output = argv[i+1];
        }
        else if(commandArgument == "-k") // kernel
        {
            kernelNameOption = commandArgument;
            kernelName = argv[i+1];
        }
        else if(commandArgument == "-t") // time level
        {
            timeOption = commandArgument;
            string tempTime = argv[i+1];
            if(tempTime == "yes")
            {
                timeMeasured = true;
            }
        }
    }

    /// list A should be given
    if(listOptionA == "")
    {
        cout << "List A (-A) was not specified" << endl ;
        return;
    }

    /// list B should be given
    if(listOptionB == "")
    {
        cout << "List B (-B) was not specified" << endl ;
        return;
    }
    /// minimum weight is mandatory
    if(cutWeightOption == "")
    {
        cout << "Cut weight argument (-c) was not specified" << endl ;
        return;
    }

    /// verbose level is mandatory
    if(outputOption == "")
    {
        cout << "Output configuration (-o) was not specified" << endl ;
        return;
    }

    /// kernel name is mandatory
    if(kernelNameOption == "")
    {
        cout << "Kernel name (-k) was not specified" << endl ;
        return;
    }

    /// kernel name is mandatory
    if(timeOption == "")
    {
        cout << "Time measuring option (-t) was not specified" << endl ;
        return;
    }

    /// only here the arguments are valid
    valid = true;
}

/**
function: getListNameA
description: get list name A
returns list name A
*/
string ArgumentValidator::getListNameA()
{
    return listNameA;
}

/**
function: getListNameB
description: get list name B
returns list name B
*/
string ArgumentValidator::getListNameB()
{
    return listNameB;
}

/**
function: getCutWeight
description: get Cut Weight
returns cut Weight
*/
string ArgumentValidator::getCutWeight()
{
    return cutWeight;
}

/**
function: getOutput
description: get output
returns output
*/
string ArgumentValidator::getOutput()
{
    return output;
}

/**
function: getKernelName
description: get Kernel Name
returns kernel name
*/
string ArgumentValidator::getKernelName()
{
    return kernelName;
}

/**
function: isTimeMeasured
description: get time measuring option
returns valid or not
*/
bool ArgumentValidator::isTimeMeasured()
{
    return timeMeasured;
}

/**
function: isValid
description: get validity
returns valid or not
*/
bool ArgumentValidator::isValid()
{
    return valid;
}
