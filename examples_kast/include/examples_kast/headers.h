/// group 1
unsigned int group01_function01_version00(char *stringA, char *stringB, unsigned int k);
unsigned int  group01_function01_version01(  char  *stringA,  char  *stringB,  unsigned int  k);
int group01_function01_version02(char *cadenaA, char *cadenaB, int n);
int group01_function01_version03(int n, char *cadenaA, char *cadenaB);
void group01_function01_version04(char *cadenaA, char *cadenaB, int n, float *valorKernel, int normalizado);

unsigned int group01_function02_version00(const char *stringA, const char *stringB, unsigned int k);
unsigned  int  group01_function02_version01(const  char  *stringA,  const char  *stringB,  unsigned  int  k);
int group01_function02_version02(char *cadenaA, char *cadenaB, int n);
int group01_function02_version03(int n, char *cadenaA, char *cadenaB);
void group01_function02_version04(char *cadenaA, char *cadenaB, int n, float *valorKernel, int normalizado);

unsigned int group01_function03_version00(const char *stringA, const char *stringB);
unsigned  int  group01_function03_version01(const  char  *stringA,  const  char  *stringB);
int group01_function03_version02(char *cadenaA, char *cadenaB);
int group01_function03_version03(char *cadenaA, char *cadenaB);
void group01_function03_version04(char *cadenaA, char *cadenaB, float *valorKernel, int normalizado);

unsigned int group01_function04_version00(const char *stringA, const char *stringB, const char separator);
unsigned  int  group01_function04_version01(const  char  *stringA,  const  char  *stringB,  const  char  separator);
int group01_function04_version02(char *cadenaA, char *cadenaB, char separator);
int group01_function04_version03(char separator, char *cadenaA, char *cadenaB);
void group01_function04_version04(char *cadenaA, char *cadenaB, char separator, float *valorKernel, int normalizado);

unsigned int group01_function05_version00(const char *stringA, const char *stringB, const char openChar, const char closeChar);
unsigned  int  group01_function05_version01(const  char  *stringA,  const  char  *stringB,  const  char  openChar,  const  char  closeChar);
int group01_function05_version02(char *cadenaA, char *cadenaB, char apertura, char cierre);
int group01_function05_version03(char apertura, char cierre, char *cadenaA, char *cadenaB);
void group01_function05_version04(char *cadenaA, char *cadenaB, char apertura, char cierre, float *valorKernel, int normalizado);

/// group 2
void group02_function01_version00(unsigned int n, const char **stringValues);
void  group02_function01_version01(unsigned  int  n,  const  char  **stringValues);
void group02_function01_version02(int p, char **cadenasValores);
void group02_function01_version03(char **cadenasValores, int p);
void group02_function01_version04(int p, char **cadenasValores);

void group02_function02_version00(unsigned int n, const char **stringValues);
void  group02_function02_version01(unsigned  int  n,  const char  **stringValues);
void group02_function02_version02(int p, char **cadenasValores);
void group02_function02_version03(char **cadenasValores, int p);
void group02_function02_version04(int p, char **cadenasValores);

void group02_function03_version00(unsigned int n, const char **stringValues);
void  group02_function03_version01(unsigned  int  n,  const char  **stringValues);
void group02_function03_version02(int p, char **cadenasValores);
void group02_function03_version03(char **cadenasValores, int p);
void group02_function03_version04(int p, char **cadenasValores);

void group02_function04_version00(unsigned int n, const char **stringValues);
void  group02_function04_version01(unsigned  int  n,  const  char  **stringValues);
void group02_function04_version02(int p, char **cadenasValores);
void group02_function04_version03(char **cadenasValores, int p);
void group02_function04_version04(int p, char **cadenasValores);

void group02_function05_version00(unsigned int n, const char **stringValues);
void  group02_function05_version01(unsigned  int  n,  const  char  **stringValues);
void group02_function05_version02(int p, char **cadenasValores);
void group02_function05_version03(char **cadenasValores, int p);
void group02_function05_version04(int p, char **cadenasValores);

/// group 3
void group03_function01_version00(unsigned int n, const char **stringValues);
void  group03_function01_version01(unsigned  int  n,  const  char  **stringValues);
void group03_function01_version02(int p, char **cadenaValores);
void group03_function01_version03(char **cadenaValores, int p);
void group03_function01_version04(int p, char **cadenaValores);

void group03_function02_version00(unsigned int n, const char **stringValues);
void  group03_function02_version01(unsigned  int  n,  const  char  **stringValues);
void group03_function02_version02(int p, char **cadenaValores);
void group03_function02_version03(char **cadenaValores, int p);
void group03_function02_version04(int p, char **cadenaValores);

void group03_function03_version00(unsigned int n, const char **stringValues);
void  group03_function03_version01(unsigned  int  n,  const  char  **stringValues);
void group03_function03_version02(int p, char **cadenaValores);
void group03_function03_version03(char **cadenaValores, int p);
void group03_function03_version04(int p, char **cadenaValores);

void group03_function04_version00(unsigned int n, const char **stringValues);
void  group03_function04_version01(unsigned  int  n,  const  char  **stringValues);
void group03_function04_version02(int p, char **cadenaValores);
void group03_function04_version03(char **cadenaValores, int p);
void group03_function04_version04(int p, char **cadenaValores);

void group03_function05_version00(unsigned int n, const char **stringValues);
void  group03_function05_version01(unsigned  int  n,  const  char  **stringValues);
void group03_function05_version02(int p, char **cadenaValores);
void group03_function05_version03(char **cadenaValores, int p);
void group03_function05_version04(int p, char **cadenaValores);

/// group 4
void group04_function01_version00(unsigned int n, const char **stringValues);
void  group04_function01_version01(unsigned  int  n,  const  char  **stringValues);
void group04_function01_version02(int p, char **cadenaValores);
void group04_function01_version03(char **cadenaValores, int p);
void group04_function01_version04(int p, char **cadenaValores);

void group04_function02_version00(unsigned int n, const char **stringValues);
void  group04_function02_version01(unsigned  int  n,  const  char  **stringValues);
void group04_function02_version02(int p, char **cadenaValores);
void group04_function02_version03(char **cadenaValores, int p);
void group04_function02_version04(int p, char **cadenaValores);

void group04_function03_version00(unsigned int n, const char **stringValues);
void  group04_function03_version01(unsigned  int  n,  const  char  **stringValues);
void group04_function03_version02(int p, char **cadenaValores);
void group04_function03_version03(char **cadenaValores, int p);
void group04_function03_version04(int p, char **cadenaValores);

void group04_function04_version00(unsigned int n, const char **stringValues);
void  group04_function04_version01(unsigned  int  n,  const  char  **stringValues);
void group04_function04_version02(int p, char **cadenaValores);
void group04_function04_version03(char **cadenaValores, int p);
void group04_function04_version04(int p, char **cadenaValores);

void group04_function05_version00(unsigned int n, const char **stringValues);
void  group04_function05_version01(unsigned  int  n,  const  char  **stringValues);
void group04_function05_version02(int p, char **cadenaValores);
void group04_function05_version03(char **cadenaValores, int p);
void group04_function05_version04(int p, char **cadenaValores);

