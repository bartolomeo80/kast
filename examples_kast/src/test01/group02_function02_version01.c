/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <stdio.h>
#include <stdlib.h>
#include "examples_kast/headers.h"
void  group02_function02_version01(unsigned  int  n,  const char  **stringValues){
    unsigned int i=0;
    unsigned int j=0;
    int temp=0;
    int list[n];
    for(i=0;i<n;i++){
        list[i]=atoi(stringValues[i]);
    }
    for(i=1;i<n;i++){
        for(j=i;j>0;j--){
            if(list[j]<list[j-1]){
                temp=list[j];
                list[j]=list[j-1];
                list[j-1]=temp;
            }
            else{
                break;
            }
        }
    }
    for(j=0;j<n;j++){
        printf("%d ",list[j]);
    }
    printf("\n");
}
