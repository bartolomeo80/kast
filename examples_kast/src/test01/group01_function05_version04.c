/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <string.h>
#include <math.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group01_function05_version04
descripcion: sentence kernel
argumentos:
cadenaA cadena A
cadenaB cadena B
apertura sentence delimiter
cierre sentence delimiter
valorKernel valor kernel
normalizado normalizado
retorna: vacio
*/
void group01_function05_version04(char *cadenaA, char *cadenaB, char apertura, char cierre, float *valorKernel, int normalizado)
{
    /// preparar variables
    char cadenasRepetidas[strlen(cadenaB)][strlen(cadenaB)]; // lista de cadenas repetidas
    int cantidadRepetidas = 0; // y la cantidad de ellas
    char *apuntador = cadenaB; // apuntador a una cadena
    int contador1 = 0;
    char *inicioPalabra = NULL;
    unsigned int n = 0; // tamano palabra
    /// iterar en la cadena B en busca de subcadenas de tamano n
    while(contador1 < strlen(cadenaB))
    {
        /// apertura
        if(cadenaB[contador1] == apertura && contador1 < strlen(cadenaB) - 2 && cadenaB[contador1+1] != apertura && cadenaB[contador1+1] != cierre && inicioPalabra == NULL)
        {
            n = 0;
            inicioPalabra = apuntador;
        }
        /// cierre
        if(cadenaB[contador1] == cierre && contador1 > 1 && cadenaB[contador1-1] != apertura && cadenaB[contador1-1] != cierre && inicioPalabra != NULL)
        {
            n = n + 1;
            /// extraer la subcadena
            char subcadena[n+1]; // subcadena actual
            memset(subcadena, 0, (n+1)*sizeof(char)); // inicializar en ceros
            strncpy(subcadena, inicioPalabra, n);
            int conteoA = 0;
            int conteoB = 0;
            /// buscar todas las ocurrencias en B
            conteoB++; // una ya se tiene por adelantado
            char *apuntadorTemporal = inicioPalabra + n;
            while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
            {
                conteoB++;
                apuntadorTemporal++;
            }
            /// buscar todas las ocurrencias en A
            apuntadorTemporal = cadenaA;
            while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
            {
                conteoA++;
                apuntadorTemporal++;
            }
            int contador2 = 0;
            while(contador2 < cantidadRepetidas) // iterar
            {
                if(strcmp(cadenasRepetidas[contador2],subcadena)==0) // comparar
                {
                    break; // salir del ciclo
                }
                contador2++;
            }
            /// no estaba registrada
            if(contador2 == cantidadRepetidas)
            {
                /// actualizar el valor del kernel
                (*valorKernel) += conteoA * conteoB;
                /// mas de una coincidencia en B? registrar como repetida para evitar doble calculo
                if(conteoB > 1)
                {
                    if(conteoA > 0) // hubo coincidencias?
                    {
                        /// agregar a la lista de repetidas
                        strcpy(cadenasRepetidas[cantidadRepetidas],subcadena);
                        cantidadRepetidas++;
                    }
                }
            }
            n = 0;
            inicioPalabra = NULL;
        }
        /// adelantar apuntador y contador
        contador1++;
        apuntador++;
        n++;
    }
    /// si se requiere normalizar
    if(normalizado == 1)
    {
        float valorKernelAA = 0.0;
        float valorKernelBB = 0.0;
        group01_function05_version04(cadenaA,cadenaA,apertura,cierre,&valorKernelAA,0); // calcular kernel AA
        group01_function05_version04(cadenaB,cadenaB,apertura,cierre,&valorKernelBB,0); // calcular kernel BB
        /// actualizar el kernel con el valor normalizado
        (*valorKernel)=(float)(*valorKernel)/(float)sqrt(valorKernelAA*valorKernelBB);

    }
}
