/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group02_function01_version03
descripcion: bubble sort
argumentos:
cadenasValores valores como cadenas
p numero de elementos
retorna: nada
*/
void group02_function01_version03(char **cadenasValores, int p)
{
    /// preparar variables
    int contador1 = 0;
    int lista[p];
    memset(lista,0,p*sizeof(int));
    /// iterar y convertir a entero
    for(contador1 = 0; contador1 < p; contador1++)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
    }
    /// iterar
    for(contador1 = 0 ; contador1 <=  p - 2; contador1++)
    {
        /// iterar
        int contador2 = 0;
        for(contador2 = 0 ; contador2 < p - contador1 - 1; contador2++)
        {
            /// intercambiar valores
            if(lista[contador2] > lista[contador2+1])
            {
                int temporal = lista[contador2+1];
                lista[contador2+1] = lista[contador2];
                lista[contador2] = temporal;
            }
        }
    }
    /// imprimir en pantalla
    int contador2 = 0;
    for(contador2 = 0; contador2 < p; contador2++)
    {
        printf("%d ", lista[contador2]);
    }
    printf("\n");
}
