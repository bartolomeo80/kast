/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <stdio.h>
#include <stdlib.h>
#include "examples_kast/headers.h"
void  group02_function04_version01(unsigned  int  n,  const char  **stringValues){
    unsigned int i=0;
    int j=0;
    int temp=0;
    int list[n];
    int c=0;
    int root=0;
    for(i=0;i<n;i++){
        list[i]=atoi(stringValues[i]);
    }
    for(i=1;i<n;i++){
        c=i;
        do{
            root=(c-1)/2;
            if (list[root]<list[c]){
                temp=list[root];
                list[root]=list[c];
                list[c]=temp;
            }
            c=root;
        } while (c != 0);
    }
    for(j=n-1;j>=0;j--){
        temp=list[0];
        list[0]=list[j];
        list[j]=temp;
        root=0;
        do{
            c=2*root+1;
            if((list[c]<list[c+1])&&c<j-1){
                c++;
            }
            if(list[root]<list[c]&&c<j){
                temp=list[root];
                list[root]=list[c];
                list[c]=temp;
            }
            root=c;
        }while(c<j);
    }
    for(j=0;j<n;j++){
        printf("%d ",list[j]);
    }
    printf("\n");
}
