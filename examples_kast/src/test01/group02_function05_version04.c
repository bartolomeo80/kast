/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
void juntar(int *A,int *L,int leftCount,int *R,int rightCount)
{
	int i = 0;
	int j = 0;
	int k = 0;
	while(i<leftCount && j< rightCount)
    {
		if(L[i]  < R[j])
		{
		    A[k] = L[i];
		    k++;
		    i++;
		}
		else
        {
            A[k] = R[j];
            k++;
		    j++;
        }
	}
	while(i < leftCount)
    {
        A[k] = L[i];
        k++;
        i++;
    }
	while(j < rightCount)
    {
        A[k] = R[j];
        k++;
        j++;
    }
}
void ordenamientoJuntar(int *A,int n)
{
    if(n < 2)
    {
        return;
    }
	int i = 0;
	int mid = n/2;
	int *L = (int*)malloc(mid*sizeof(int));
	int *R = (int*)malloc((n- mid)*sizeof(int));
	for(i = 0;i < mid;i++)
    {
        L[i] = A[i];
    }
	for(i = mid;i < n;i++)
    {
        R[i-mid] = A[i];
    }
	ordenamientoJuntar(L, mid);
	ordenamientoJuntar(R, n-mid);
	juntar(A, L, mid, R, n-mid);
    free(L);
    free(R);
}
/**
funcion: group02_function05_version04
descripcion: merge sort
argumentos:
p numero de elementos
cadenasValores valores como cadenas
retorna: nada
*/
void group02_function05_version04(int p, char **cadenasValores)
{
    /// preparar variables
    int contador1 = p - 1;
    int *lista = (int*) malloc(p*sizeof(int));
    memset(lista,0,p*sizeof(int));
    /// iterar y convertir a entero
    while(contador1 >= 0)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
        contador1--;
    }
    /// ordenar
	ordenamientoJuntar(lista, p);
    /// imprimir en pantalla
    int contador2 = 0;
    while(contador2 < p)
    {
        printf("%d ", lista[contador2]);
        contador2++;
    }
    printf("\n");
    /// liberar memoria
    free(lista);
}
