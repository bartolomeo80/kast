/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string.h>

/// own headers
#include "examples_kast/headers.h"

/**
function: group01_function05_version00
description: sentence kernel
arguments:
stringA string A
stringB string B
openChar sentence delimiter
closeChar sentence delimiter
returns: kernel evaluation
*/
unsigned int group01_function05_version00(const char *stringA, const char *stringB, const char openChar, const char closeChar)
{
    /// check for correct lenghts
    unsigned int lenA = (unsigned int) strlen(stringA);

    /// variable preparation
    unsigned int kernelValue = 0; // kernel value
    char repeatedStrings[lenA][lenA]; // list of reapeated strings
    unsigned int repeatedLenght = 0; // and its current lenght
    char *pointer = 0; // string pointer
    unsigned int i = 0;
    char *wordStart = NULL;
    unsigned int k = 0; // word size

    /// iterate in search of separator on string A
    for(i = 0, pointer = stringA; i < lenA; k++,i++, pointer++)
    {
        /// if it is the start of a word
        if(stringA[i] == openChar && i < lenA - 2 && stringA[i+1] != openChar && stringA[i+1] != closeChar && wordStart == NULL)
        {
            k = 0;
            wordStart = pointer;
        }

        /// if it is the end of a word
        if(stringA[i] == closeChar && i > 1 && stringA[i-1] != openChar && stringA[i-1] != closeChar && wordStart != NULL)
        {
            k = k + 1;
            char substring[k+1]; // current substring
            memset(substring, 0, (k+1)*sizeof(char)); // zero it
            /// get the substring
            strncpy(substring, wordStart, k);

            /// search in the repeated list, reject if found
            unsigned int j = 0;
            unsigned int skip = 0;
            for(j = 0; j < repeatedLenght; j++) // iterate
            {
                if(strcmp(repeatedStrings[j],substring)==0) // compare
                {
                    skip = 1; // found
                    break; // stop
                }
            }
            if(skip == 1) // if found
            {
                k = 0;
                wordStart = NULL;
                continue; // go to the next substring
            }

            /// search all cases in A
            unsigned int countA = 0;
            const char *tmp = stringA;
            while((tmp = strstr(tmp, substring)) != 0)
            {
                countA++;
                tmp++;
            }

            /// search all cases in B
            unsigned int countB = 0;
            tmp = stringB;
            while((tmp = strstr(tmp, substring)) != 0)
            {
                countB++;
                tmp++;
            }

            /// no matches? continue to next string
            if(countB == 0)
            {
                k = 0;
                wordStart = NULL;
                continue;
            }

            /// more than 1 match in A? register as repeated to avoid recalculation
            if(countA > 1)
            {
                strcpy(repeatedStrings[repeatedLenght],substring);
                repeatedLenght++;
            }

            /// calculate feature value
            unsigned int featureValue = countA * countB;

            /// add the feature to the inner product
            kernelValue += featureValue;
            k = 0;
            wordStart = NULL;
        }
    }

    /// return the kernel value
    return kernelValue;
}
