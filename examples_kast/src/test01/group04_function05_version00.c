/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <stdio.h>
#include <stdlib.h>

/// own headers
#include "examples_kast/headers.h"

/**
function: group04_function05_version00
description: 2D stencil: another non-compact stencil with summation
arguments:
n number of elements
stringValues values as strings
returns: nothing
*/
void group04_function05_version00(unsigned int n, const char **stringValues)
{
    /// prepare variables
    int originalMatrix[n+6][n+6];
    memset(originalMatrix, 0, sizeof(originalMatrix));
    int calculatedMatrix[n+6][n+6];
    memset(calculatedMatrix, 0, sizeof(calculatedMatrix));
    unsigned int i = 0;
    unsigned int j = 0;

    /// iterate and convert to int
    for(i = 3; i <= n+2; i++)
    {
        for(j = 3; j <= n+2; j++)
        {
            originalMatrix[i][j] = atoi(stringValues[j-3]);
        }
    }

    /// stencil calculation
    for(i = 3; i <= n+2; i++)
    {
        for(j = 3; j <= n+2; j++)
        {
            calculatedMatrix[i][j] =
                originalMatrix[i-1][j+0] +
                originalMatrix[i+0][j-1] +
                originalMatrix[i+0][j+0] +
                originalMatrix[i+0][j+1] +
                originalMatrix[i+1][j+0] +
                originalMatrix[i-2][j+0] +
                originalMatrix[i+0][j-2] +
                originalMatrix[i+0][j+2] +
                originalMatrix[i+2][j+0] +
                originalMatrix[i-3][j+0] +
                originalMatrix[i+0][j-3] +
                originalMatrix[i+0][j+3] +
                originalMatrix[i+3][j+0] ;
        }
    }

    /// print
    for(i = 0; i < n+6; i++)
    {
        for(j = 0; j < n+6; j++)
        {
            printf("%d ", calculatedMatrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
