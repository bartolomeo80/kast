/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <string.h>
#include "examples_kast/headers.h"
unsigned  int  group01_function02_version01(const  char  *stringA,  const  char  *stringB,  unsigned  int  k){
    unsigned int lenA=(unsigned int) strlen(stringA);
    unsigned int lenB=(unsigned int) strlen(stringB);
    unsigned int counter=0;
    unsigned int kernelValue=0;
    for(counter=1;counter<= k;counter++){
        if(counter>lenA||counter>lenB||counter<=0){
            return 0;
        }
        char substring[counter+1];
        memset(substring,0,(counter+1)*sizeof(char));
        char repeatedStrings[lenA][counter+1];
        unsigned int repeatedLenght=0;
        char *pointer=0;
        unsigned int i=0;
        for(i=0,pointer=stringA;i<lenA-counter+1;i++,pointer++){
            strncpy(substring,pointer,counter);
            unsigned int j=0;
            unsigned int skip=0;
            for(j=0;j<repeatedLenght;j++){
                if(strcmp(repeatedStrings[j],substring)==0){
                    skip=1;
                    break;
                }
            }
            if(skip==1){
                continue;
            }
            unsigned int countA=0;
            const char *tmp=stringA;
            while((tmp=strstr(tmp, substring))!=0){
                countA++;
                tmp++;
            }
            unsigned int countB=0;
            tmp=stringB;
            while((tmp=strstr(tmp, substring))!=0){
                countB++;
                tmp++;
            }
            if(countB==0){
                continue;
            }
            if(countA>1){
                strcpy(repeatedStrings[repeatedLenght],substring);
                repeatedLenght++;
            }
            unsigned int featureValue=countA*countB;
            kernelValue+=featureValue;
        }
    }
    return kernelValue;
}
