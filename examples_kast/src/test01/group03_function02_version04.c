/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
function: group03_function02_version04
descripcion: 3D stencil: side stencil with summation
argumentos:
p numero de elementos
cadenaValores valores como cadenas
retorna: nada
*/
void group03_function02_version04(int p, char **cadenaValores)
{
    /// preparar variables
    long ***matrizOriginal = (long ***)malloc((p+2)*sizeof(long **));
    long ***matrizCalculada = (long ***)malloc((p+2)*sizeof(long **));
    int contador1 = 0;
    /// iterar y convertir a entero
    for(contador1 = 0; contador1 < p+2; contador1++)
    {
        matrizOriginal[contador1] = (long **)malloc((p+2)*sizeof(long*));
        matrizCalculada[contador1] = (long **)malloc((p+2)*sizeof(long*));
        int contador2 = 0;
        for(contador2 = 0; contador2 < p+2; contador2++)
        {
            matrizOriginal[contador1][contador2] = (long *)malloc((p+2)*sizeof(long));
            matrizCalculada[contador1][contador2] = (long *)malloc((p+2)*sizeof(long));
            int contador3 = 0;
            for(contador3 = 0; contador3 < p +2; contador3++)
            {
                if(contador1 > 0 && contador1 < p + 1 && contador2 > 0 && contador2 < p + 1 && contador3 > 0 && contador3 < p + 1)
                {
                     matrizOriginal[contador1][contador2][contador3] = atol(cadenaValores[contador3-1]);
                }
                else
                {
                    matrizOriginal[contador1][contador2][contador3] = 0;
                }
                matrizCalculada[contador1][contador2][contador3] = 0;
            }
        }
    }
    /// calcular el stencil
    for(contador1 = 1; contador1 <= p; contador1++)
    {
        int contador2 = 0;
        for(contador2 = 1; contador2 <= p; contador2++)
        {
            int contador3 = 0;
            for(contador3 = 1; contador3 <= p; contador3++)
            {
                int offset1 = 0;
                for(offset1 = -1; offset1 < 2; offset1++)
                {
                    int offset2 = 0;
                    for(offset2 = -1; offset2 < 2; offset2++)
                    {
                        int offset3 = 0;
                        for(offset3 = -1; offset3 < 2; offset3++)
                        {
                            if(
                               (offset1 == 0 && offset2 == 0) ||
                               (offset1 == 0 && offset3 == 0) ||
                               (offset2 == 0 && offset3 == 0)
                               )
                            {
                                matrizCalculada[contador1][contador2][contador3] += matrizOriginal[contador1+offset1][contador2+offset2][contador3+offset3];
                            }
                        }
                    }
                }
            }
        }
    }
    /// imprimir en pantalla
    for(contador1 = 0; contador1 < p+2; contador1++)
    {
        int contador2 = 0;
        for(contador2 = 0; contador2 < p+2; contador2++)
        {
            int contador3 = 0;
            for(contador3 = 0; contador3 < p+2; contador3++)
            {
                printf("%ld ", matrizCalculada[contador1][contador2][contador3]);
            }
            free(matrizOriginal[contador1][contador2]);
            free(matrizCalculada[contador1][contador2]);
            printf("\n");
        }
        free(matrizOriginal[contador1]);
        free(matrizCalculada[contador1]);
        printf("\n\n");
    }
    free(matrizOriginal);
    free(matrizCalculada);
}

