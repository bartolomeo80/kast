/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group02_function04_version04
descripcion: heap sort
argumentos:
p numero de elementos
cadenasValores valores como cadenas
retorna: nada
*/
void group02_function04_version04(int p, char **cadenasValores)
{
    /// preparar variables
    int contador1 = p - 1;
    int *lista = (int*) malloc(p*sizeof(int));
    memset(lista,0,p*sizeof(int));
    /// iterar y convertir a entero
    while(contador1 >= 0)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
        contador1--;
    }
    contador1 = 1;
    int stage = 1;
    while (contador1 >= 0)
    {
        int contenedor = 0;
        int raiz = 0;
        int temporal = 0;
        /// preparacion de la pila
        if(stage == 1)
        {
            contenedor = contador1;
            while (contenedor != 0)
            {
                raiz = (contenedor - 1) / 2;
                if (lista[raiz] < lista[contenedor])
                {
                    temporal = lista[contenedor];
                    lista[contenedor] = lista[raiz];
                    lista[raiz] = temporal;
                }
                contenedor = raiz;
            }
            contador1++;
            if(contador1 == p)
            {
                stage = 2;
                contador1--;
            }
        }
        else if(stage == 2) /// ordenar
        {
            temporal = lista[contador1];
            lista[contador1] = lista[0];
            lista[0] = temporal;
            while (contenedor < contador1)
            {
                contenedor = 2 * raiz + 1;
                if ((lista[contenedor] < lista[contenedor + 1]) && contenedor < contador1 - 1)
                {
                    contenedor++;
                }
                if (lista[raiz] < lista[contenedor] && contenedor < contador1)
                {
                    temporal = lista[contenedor];
                    lista[contenedor] = lista[raiz];
                    lista[raiz] = temporal;
                }
                raiz = contenedor;
            }
            contador1--;
        }
    }
    /// imprimir en pantalla
    int contador2 = 0;
    while(contador2 < p)
    {
        printf("%d ", lista[contador2]);
        contador2++;
    }
    printf("\n");
    /// liberar memoria
    free(lista);
}
