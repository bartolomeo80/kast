/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string.h>

/// own headers
#include "examples_kast/headers.h"

/**
function: group01_function02_version00
description: blended spectrum kernel
arguments:
stringA string A
stringB string B
k target lenght
returns: kernel evaluation
*/
unsigned int group01_function02_version00(const char *stringA, const char *stringB, unsigned int k)
{
    /// check for correct lenghts
    unsigned int lenA = (unsigned int) strlen(stringA);
    unsigned int lenB = (unsigned int) strlen(stringB);
    unsigned int counter = 0;
    unsigned int kernelValue = 0; // kernel value

    /// iterate each valua till k and accumulate
    for(counter = 1; counter <= k; counter++)
    {
        if(counter > lenA || counter > lenB || counter <= 0)
        {
            return 0; // terminate
        }

        /// variable preparation
        char substring[counter+1]; // current substring
        memset(substring, 0, (counter+1)*sizeof(char)); // zero it
        char repeatedStrings[lenA][counter+1]; // list of reapeated strings
        unsigned int repeatedLenght = 0; // and its current lenght
        char *pointer = 0; // string pointer
        unsigned int i = 0;

        /// iterate in search of substrings of size counter on string A
        for(i = 0, pointer = stringA; i < lenA - counter + 1; i++, pointer++)
        {
            /// get the substring
            strncpy(substring, pointer, counter);

            /// search in the repeated list, reject if found
            unsigned int j = 0;
            unsigned int skip = 0;
            for(j = 0; j < repeatedLenght; j++) // iterate
            {
                if(strcmp(repeatedStrings[j],substring)==0) // compare
                {
                    skip = 1; // found
                    break; // stop
                }
            }
            if(skip == 1) // if found
            {
                continue; // go to the next substring
            }

            /// search all cases in A
            unsigned int countA = 0;
            const char *tmp = stringA;
            while((tmp = strstr(tmp, substring)) != 0)
            {
                countA++;
                tmp++;
            }

            /// search all cases in B
            unsigned int countB = 0;
            tmp = stringB;
            while((tmp = strstr(tmp, substring)) != 0)
            {
                countB++;
                tmp++;
            }

            /// no matches? continue to next string
            if(countB == 0)
            {
                continue;
            }

            /// more than 1 match in A? register as repeated to avoid recalculation
            if(countA > 1)
            {
                strcpy(repeatedStrings[repeatedLenght],substring);
                repeatedLenght++;
            }

            /// calculate feature value
            unsigned int featureValue = countA * countB;

            /// add the feature to the inner product
            kernelValue += featureValue;
        }
    }

    /// return the kernel value
    return kernelValue;
}
