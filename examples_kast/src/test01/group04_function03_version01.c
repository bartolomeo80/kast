/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <stdio.h>
#include <stdlib.h>
#include "examples_kast/headers.h"
void  group04_function03_version01(unsigned  int  n,  const  char  **stringValues){
    int originalMatrix[n+2][n+2];
    memset(originalMatrix,0,sizeof(originalMatrix));
    int calculatedMatrix[n+2][n+2];
    memset(calculatedMatrix,0,sizeof(calculatedMatrix));
    unsigned int i=0;
    unsigned int j=0;
    for(i=1;i<=n;i++){
        for(j=1;j<=n;j++){
            originalMatrix[i][j]=atoi(stringValues[j-1]);
        }
    }
    for(i=1;i<=n;i++){
        for(j=1;j<=n;j++){
            calculatedMatrix[i][j]=
                originalMatrix[i-1][j-1]+
                originalMatrix[i-1][j+1]+
                originalMatrix[i+0][j+0]+
                originalMatrix[i+1][j-1]+
                originalMatrix[i+1][j+1];
        }
    }
    for(i=0;i<n+2;i++){
        for(j=0;j<n+2;j++){
            printf("%d ",calculatedMatrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
