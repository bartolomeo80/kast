/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
function: group03_function05_version03
descripcion: 3D stencil: non-compact stencil with summation
argumentos:
p numero de elementos
cadenaValores valores como cadenas
retorna: nada
*/
void group03_function05_version03(char **cadenaValores, int p)
{
    /// preparar variables
    long matrizOriginal[p+4][p+4][p+4];
    memset(matrizOriginal, 0, sizeof(matrizOriginal));
    int contador1 = 0;
    /// iterar y convertir a entero
    for(contador1 = 2; contador1 <= p+1; contador1++)
    {
         int contador2 = 0;
        for(contador2 = 2; contador2 <= p+1; contador2++)
        {
            int contador3 = 0;
            for(contador3 = 2; contador3 <= p+1; contador3++)
            {
                matrizOriginal[contador1][contador2][contador3] = atol(cadenaValores[contador3-2]);
            }
        }
    }
    long matrizCalculada[p+4][p+4][p+4];
    memset(matrizCalculada, 0, sizeof(matrizCalculada));
    /// calcular el stencil
    for(contador1 = 2; contador1 <= p+1; contador1++)
    {
        int contador2 = 0;
        for(contador2 = 2; contador2 <= p+1; contador2++)
        {
            int contador3 = 0;
            for(contador3 = 2; contador3 <= p+1; contador3++)
            {
                /// matriz superior
                /// fila 2
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1-1][contador2][contador3] ;
                /// matriz interna
                /// fila 1
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1][contador2-1][contador3] ;
                /// fila 2
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1][contador2][contador3-1] +
                    matrizOriginal[contador1][contador2][contador3] +
                    matrizOriginal[contador1][contador2][contador3+1] ;
                /// fila 3
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1][contador2+1][contador3] ;
                /// matriz inferior
                /// fila 2
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1+1][contador2][contador3] ;
                /// matriz superior
                /// fila 2
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1-2][contador2][contador3] ;
                /// matriz interna
                /// fila 1
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1][contador2-2][contador3] ;
                /// fila 2
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1][contador2][contador3-2] +
                    matrizOriginal[contador1][contador2][contador3+2] ;
                /// fila 3
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1][contador2+2][contador3] ;
                /// matriz inferior
                /// fila 2
                matrizCalculada[contador1][contador2][contador3] +=
                    matrizOriginal[contador1+2][contador2][contador3] ;
            }
        }
    }
    /// imprimir en pantalla
    for(contador1 = 0; contador1 < p+4; contador1++)
    {
        int contador2 = 0;
        for(contador2 = 0; contador2 < p+4; contador2++)
        {
            int contador3 = 0;
            for(contador3 = 0; contador3 < p+4; contador3++)
            {
                printf("%ld ", matrizCalculada[contador1][contador2][contador3]);
            }
            printf("\n");
        }
        printf("\n\n");
    }
}

