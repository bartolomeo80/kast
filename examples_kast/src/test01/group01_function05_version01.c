/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <string.h>
#include "examples_kast/headers.h"
unsigned  int  group01_function05_version01(const  char  *stringA,  const  char  *stringB,  const  char  openChar,  const  char  closeChar){
    unsigned int lenA=(unsigned int)strlen(stringA);
    unsigned int kernelValue=0;
    char repeatedStrings[lenA][lenA];
    unsigned int repeatedLenght=0;
    char *pointer=0;
    unsigned int i=0;
    char *wordStart=NULL;
    unsigned int k=0;
    for(i=0,pointer=stringA;i<lenA;k++,i++,pointer++){
        if(stringA[i]==openChar&&i <lenA-2&&stringA[i+1]!=openChar&&stringA[i+1]!=closeChar&&wordStart==NULL){
            k=0;
            wordStart=pointer;
        }
        if(stringA[i]==closeChar&&i>1&&stringA[i-1]!=openChar&&stringA[i-1]!=closeChar&&wordStart!=NULL){
            k = k + 1;
            char substring[k+1];
            memset(substring,0,(k+1)*sizeof(char));
            strncpy(substring,wordStart,k);
            unsigned int j=0;
            unsigned int skip=0;
            for(j=0;j<repeatedLenght;j++){
                if(strcmp(repeatedStrings[j],substring)==0){
                    skip=1;
                    break;
                }
            }
            if(skip==1){
                k=0;
                wordStart=NULL;
                continue;
            }
            unsigned int countA=0;
            const char *tmp=stringA;
            while((tmp=strstr(tmp,substring))!=0){
                countA++;
                tmp++;
            }
            unsigned int countB=0;
            tmp=stringB;
            while((tmp=strstr(tmp,substring))!=0){
                countB++;
                tmp++;
            }
            if(countB==0){
                k=0;
                wordStart=NULL;
                continue;
            }
            if(countA>1){
                strcpy(repeatedStrings[repeatedLenght],substring);
                repeatedLenght++;
            }
            unsigned int featureValue=countA*countB;
            kernelValue+=featureValue;
            k=0;
            wordStart=NULL;
        }
    }
    return kernelValue;
}
