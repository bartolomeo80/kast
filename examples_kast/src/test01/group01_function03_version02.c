/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <string.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group01_function03_version02
descripcion: bag of characters kernel
argumentos:
cadenaA cadena A
cadenaB cadena B
retorn: valor del kernel
*/
int group01_function03_version02(char *cadenaA, char *cadenaB)
{
    /// chequear tamanos correctos
    int tamanoA = (int)strlen(cadenaA);
    int tamanoB = (int)strlen(cadenaB);
    if(tamanoA < 1 || tamanoB < 1)
    {
        return 0; // terminar
    }
    /// preparar variables
    int valorKernel = 0; // valor kernel
    char subcadena[2]; // subcadena actual
    memset(subcadena, 0, (2)*sizeof(char)); // inicializar en ceros
    char cadenasRepetidas[tamanoA][2]; // lista de cadenas repetidas
    int cantidadRepetidas = 0; // y la cantidad de ellas
    char *apuntador = NULL; // apuntador a una cadena
    int contador1 = 0;
    /// iterar en la cadena A en busca de subcadenas de tamano 1
    for(contador1 = 0, apuntador = cadenaA; contador1 < tamanoA; contador1++, apuntador++)
    {
        /// extraer la subcadena
        strncpy(subcadena, apuntador, 1);
        /// buscar en la lista de subcadenas y rechazar si se encuentra
        int contador2 = 0;
        int saltar = 0;
        for(contador2 = 0; contador2 < cantidadRepetidas; contador2++) // iterar
        {
            if(strcmp(cadenasRepetidas[contador2],subcadena)==0) // comparar
            {
                saltar = 1; // encontrada
                break; // salir del ciclo
            }
        }
        if(saltar == 1) // si se encontro
        {
            continue; // pasar directamente a la siguiente cadena
        }
        /// buscar todas las ocurrencias en A
        int conteoA = 0;
        char *apuntadorTemporal = cadenaA;
        while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
        {
            conteoA++;
            apuntadorTemporal++;
        }
        /// buscar todas las ocurrencias en B
        int conteoB = 0;
        apuntadorTemporal = cadenaB;
        while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
        {
            conteoB++;
            apuntadorTemporal++;
        }
        /// no hay coincidencias? continuar a la siguiente cadena
        if(conteoB == 0)
        {
            continue;
        }
        /// mas de una coincidencia en A? registrar como repetida para evitar doble calculo
        if(conteoA > 1)
        {
            strcpy(cadenasRepetidas[cantidadRepetidas],subcadena);
            cantidadRepetidas++;
        }
        /// calcular el valor de esta caracteristica
        int valorCaracteristica = conteoA * conteoB;
        /// sumar al producto interno
        valorKernel += valorCaracteristica;
    }
    /// develver el valor del kernel
    return valorKernel;
}
