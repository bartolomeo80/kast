/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <stdio.h>
#include <stdlib.h>

/// own headers
#include "examples_kast/headers.h"

/**
function: main
description: entry point
arguments:
argc number of arguments
argv arguments
returns 0
*/
int main(int argc, const char **argv)
{
    /// quick argument validation
    if(argc < 3)
    {
        printf("Usage: examples_kast <group> <stringA> <stringB> <k>\n");
        return 0;
    }

    if(strcmp(argv[1],"group1") == 0)
    {
        if(argc != 5)
        {
            printf("Usage: examples_kast <group> <stringA> <stringB> <k>\n");
            return 0;
        }
        /// prepare variables
        const char *stringA = argv[2];
        const char *stringB = argv[3];
        unsigned int k = (unsigned int) atoi(argv[4]);
        unsigned int kernelValue = 0;
        float normalizedKernelValue = 0.0;

        /// group 1

        /// k spectrum kernel
        kernelValue = group01_function01_version00(stringA, stringB, k);
        printf("Kernel value group01_function01_version00: %d\n", kernelValue);

        kernelValue = group01_function01_version01(stringA, stringB, k);
        printf("Kernel value group01_function01_version01: %d\n", kernelValue);

        kernelValue = group01_function01_version02(stringA, stringB, k);
        printf("Kernel value group01_function01_version02: %d\n", kernelValue);

        kernelValue = group01_function01_version03(k, stringA, stringB);
        printf("Kernel value group01_function01_version03: %d\n", kernelValue);

        normalizedKernelValue = 0.0;
        group01_function01_version04(stringA, stringB, k, &normalizedKernelValue, 1);
        printf("Kernel value group01_function01_version04: %f\n", normalizedKernelValue);

        /// blended spectrum kernel
        kernelValue = group01_function02_version00(stringA, stringB, k);
        printf("Kernel value group01_function02_version00: %d\n", kernelValue);

        kernelValue = group01_function02_version01(stringA, stringB, k);
        printf("Kernel value group01_function02_version01: %d\n", kernelValue);

        kernelValue = group01_function02_version02(stringA, stringB, k);
        printf("Kernel value group01_function02_version02: %d\n", kernelValue);

        kernelValue = group01_function02_version03(k, stringA, stringB);
        printf("Kernel value group01_function02_version03: %d\n", kernelValue);

        normalizedKernelValue = 0.0;
        group01_function02_version04(stringA, stringB, k, &normalizedKernelValue, 1);
        printf("Kernel value group01_function02_version04: %f\n", normalizedKernelValue);

        /// bag of characters kernel
        kernelValue = group01_function03_version00(stringA, stringB);
        printf("Kernel value group01_function03_version00: %d\n", kernelValue);

        kernelValue = group01_function03_version01(stringA, stringB);
        printf("Kernel value group01_function03_version01: %d\n", kernelValue);

        kernelValue = group01_function03_version02(stringA, stringB);
        printf("Kernel value group01_function03_version02: %d\n", kernelValue);

        kernelValue = group01_function03_version03(stringA, stringB);
        printf("Kernel value group01_function03_version03: %d\n", kernelValue);

        normalizedKernelValue = 0.0;
        group01_function03_version04(stringA, stringB, &normalizedKernelValue, 1);
        printf("Kernel value group01_function03_version04: %f\n", normalizedKernelValue);

        /// bag of words kernel
        kernelValue = group01_function04_version00(stringA, stringB, '_');
        printf("Kernel value group01_function04_version00: %d\n", kernelValue);

        kernelValue = group01_function04_version01(stringA, stringB, '_');
        printf("Kernel value group01_function04_version01: %d\n", kernelValue);

        kernelValue = group01_function04_version02(stringA, stringB, '_');
        printf("Kernel value group01_function04_version02: %d\n", kernelValue);

        kernelValue = group01_function04_version03('_', stringA, stringB);
        printf("Kernel value group01_function04_version03: %d\n", kernelValue);

        normalizedKernelValue = 0.0;
        group01_function04_version04(stringA, stringB, '_', &normalizedKernelValue, 1);
        printf("Kernel value group04_function01_version04: %f\n", normalizedKernelValue);

        kernelValue = group01_function05_version00(stringA, stringB, '(', ')');
        printf("Kernel value group01_function05_version00: %d\n", kernelValue);

        kernelValue = group01_function05_version01(stringA, stringB, '(', ')');
        printf("Kernel value group01_function05_version01: %d\n", kernelValue);

        kernelValue = group01_function05_version02(stringA, stringB, '(', ')');
        printf("Kernel value group01_function05_version02: %d\n", kernelValue);

        kernelValue = group01_function05_version03('(', ')', stringA, stringB);
        printf("Kernel value group01_function05_version03: %d\n", kernelValue);

        normalizedKernelValue = 0.0;
        group01_function05_version04(stringA, stringB, '(', ')', &normalizedKernelValue, 1);
        printf("Kernel value group01_function05_version04: %f\n", normalizedKernelValue);
    }
    if(strcmp(argv[1],"group2") == 0)
    {
        const char **stringValues = argv + 2;

        /// bubble sort
        group02_function01_version00(argc-2, stringValues);
        group02_function01_version01(argc-2, stringValues);
        group02_function01_version02(argc-2, stringValues);
        group02_function01_version03(stringValues, argc-2);
        group02_function01_version04(argc-2, stringValues);

        /// insert sort
        group02_function02_version00(argc-2, stringValues);
        group02_function02_version01(argc-2, stringValues);
        group02_function02_version02(argc-2, stringValues);
        group02_function02_version03(stringValues, argc-2);
        group02_function02_version04(argc-2, stringValues);

        /// selection sort
        group02_function03_version00(argc-2, stringValues);
        group02_function03_version01(argc-2, stringValues);
        group02_function03_version02(argc-2, stringValues);
        group02_function03_version03(stringValues, argc-2);
        group02_function03_version04(argc-2, stringValues);

        /// heap sort
        group02_function04_version00(argc-2, stringValues);
        group02_function04_version01(argc-2, stringValues);
        group02_function04_version02(argc-2, stringValues);
        group02_function04_version03(stringValues, argc-2);
        group02_function04_version04(argc-2, stringValues);

        /// merge sort
        group02_function05_version00(argc-2, stringValues);
        group02_function05_version01(argc-2, stringValues);
        group02_function05_version02(argc-2, stringValues);
        group02_function05_version03(stringValues, argc-2);
        group02_function05_version04(argc-2, stringValues);
    }
    if(strcmp(argv[1],"group3") == 0)
    {
        const char **stringValues = argv + 2;

        /// 3D compact stencil
        group03_function01_version00(argc-2, stringValues);
        group03_function01_version01(argc-2, stringValues);
        group03_function01_version02(argc-2, stringValues);
        group03_function01_version03(stringValues, argc-2);
        group03_function01_version04(argc-2, stringValues);

        /// 3D side stencil
        group03_function02_version00(argc-2, stringValues);
        group03_function02_version01(argc-2, stringValues);
        group03_function02_version02(argc-2, stringValues);
        group03_function02_version03(stringValues, argc-2);
        group03_function02_version04(argc-2, stringValues);

        /// 3D edge stencil
        group03_function03_version00(argc-2, stringValues);
        group03_function03_version01(argc-2, stringValues);
        group03_function03_version02(argc-2, stringValues);
        group03_function03_version03(stringValues, argc-2);
        group03_function03_version04(argc-2, stringValues);

        /// 3D vertex stencil
        group03_function04_version00(argc-2, stringValues);
        group03_function04_version01(argc-2, stringValues);
        group03_function04_version02(argc-2, stringValues);
        group03_function04_version03(stringValues, argc-2);
        group03_function04_version04(argc-2, stringValues);

        /// 3D non-compact stencil
        group03_function05_version00(argc-2, stringValues);
        group03_function05_version01(argc-2, stringValues);
        group03_function05_version02(argc-2, stringValues);
        group03_function05_version03(stringValues, argc-2);
        group03_function05_version04(argc-2, stringValues);
    }
    if(strcmp(argv[1],"group4") == 0)
    {
        const char **stringValues = argv + 2;

        /// 2D compact stencil
        group04_function01_version00(argc-2, stringValues);
        group04_function01_version01(argc-2, stringValues);
        group04_function01_version02(argc-2, stringValues);
        group04_function01_version03(stringValues, argc-2);
        group04_function01_version04(argc-2, stringValues);

        /// 2D edge stencil
        group04_function02_version00(argc-2, stringValues);
        group04_function02_version01(argc-2, stringValues);
        group04_function02_version02(argc-2, stringValues);
        group04_function02_version03(stringValues, argc-2);
        group04_function02_version04(argc-2, stringValues);

        /// 2D vertex stencil
        group04_function03_version00(argc-2, stringValues);
        group04_function03_version01(argc-2, stringValues);
        group04_function03_version02(argc-2, stringValues);
        group04_function03_version03(stringValues, argc-2);
        group04_function03_version04(argc-2, stringValues);

        /// 2D non-compact stencil
        group04_function04_version00(argc-2, stringValues);
        group04_function04_version01(argc-2, stringValues);
        group04_function04_version02(argc-2, stringValues);
        group04_function04_version03(stringValues, argc-2);
        group04_function04_version04(argc-2, stringValues);

        /// 2D row column stencil
        group04_function05_version00(argc-2, stringValues);
        group04_function05_version01(argc-2, stringValues);
        group04_function05_version02(argc-2, stringValues);
        group04_function05_version03(stringValues, argc-2);
        group04_function05_version04(argc-2, stringValues);
    }

    /// return to system
    return 0;
}
