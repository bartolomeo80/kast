/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <stdio.h>
#include <stdlib.h>
#include "examples_kast/headers.h"
void  group02_function03_version01(unsigned  int  n,  const char  **stringValues){
    unsigned int i=0;
    unsigned int j=0;
    unsigned int position=0;
    int temp=0;
    int list[n];
    for(i=0;i<n;i++){
        list[i]=atoi(stringValues[i]);
    }
    for(i=0;i<n-1;i++){
        position=i;
        for(j=i+1;j<n;j++){
            if(list[position]>list[j]){
                position=j;
            }
        }
        if (position!=i){
            temp=list[i];
            list[i]=list[position];
            list[position]=temp;
        }
    }
    for(j=0;j<n;j++){
        printf("%d ",list[j]);
    }
    printf("\n");
}
