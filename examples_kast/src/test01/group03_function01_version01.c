/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <stdio.h>
#include <stdlib.h>
#include "examples_kast/headers.h"
void  group03_function01_version01(unsigned  int  n,  const  char  **stringValues){
    int originalMatrix[n+2][n+2][n+2];
    memset(originalMatrix,0,sizeof(originalMatrix));
    int calculatedMatrix[n+2][n+2][n+2];
    memset(calculatedMatrix,0,sizeof(calculatedMatrix));
    unsigned int i=0;
    unsigned int j=0;
    unsigned int k=0;
    for(i=1;i<=n;i++){
        for(j=1;j<=n;j++){
            for(k=1;k<=n;k++){
                originalMatrix[i][j][k]=atoi(stringValues[k-1]);
            }
        }
    }
    for(i=1;i<=n;i++){
        for(j=1;j<=n;j++){
            for(k=1;k<=n;k++){
                calculatedMatrix[i][j][k]=
                    originalMatrix[i-1][j-1][k-1]+
                    originalMatrix[i-1][j-1][k+0]+
                    originalMatrix[i-1][j-1][k+1]+
                    originalMatrix[i-1][j+0][k-1]+
                    originalMatrix[i-1][j+0][k+0]+
                    originalMatrix[i-1][j+0][k+1]+
                    originalMatrix[i-1][j+1][k-1]+
                    originalMatrix[i-1][j+1][k+0]+
                    originalMatrix[i-1][j+1][k+1]+
                    originalMatrix[i+0][j-1][k-1]+
                    originalMatrix[i+0][j-1][k+0]+
                    originalMatrix[i+0][j-1][k+1]+
                    originalMatrix[i+0][j+0][k-1]+
                    originalMatrix[i+0][j+0][k+0]+
                    originalMatrix[i+0][j+0][k+1]+
                    originalMatrix[i+0][j+1][k-1]+
                    originalMatrix[i+0][j+1][k+0]+
                    originalMatrix[i+0][j+1][k+1]+
                    originalMatrix[i+1][j-1][k-1]+
                    originalMatrix[i+1][j-1][k+0]+
                    originalMatrix[i+1][j-1][k+1]+
                    originalMatrix[i+1][j+0][k-1]+
                    originalMatrix[i+1][j+0][k+0]+
                    originalMatrix[i+1][j+0][k+1]+
                    originalMatrix[i+1][j+1][k-1]+
                    originalMatrix[i+1][j+1][k+0]+
                    originalMatrix[i+1][j+1][k+1];
            }
        }
    }
    for(i=0;i<n+2;i++){
        for(j=0;j<n+2;j++){
            for(k=0;k<n+2;k++){
                printf("%d ",calculatedMatrix[i][j][k]);
            }
            printf("\n");
        }
        printf("\n\n");
    }
}
