/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group02_function03_version04
descripcion: selection sort
argumentos:
p numero de elementos
cadenasValores valores como cadenas
retorna: nada
*/
void group02_function03_version04(int p, char **cadenasValores)
{
    /// preparar variables
    int contador1 = p - 1;
    int *lista = (int*) malloc(p*sizeof(int));
    memset(lista,0,p*sizeof(int));
    /// iterar y convertir a entero
    while(contador1 >= 0)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
        contador1--;
    }
    /// iterar
    contador1 = p - 1;
    while(contador1 >= 1)
    {
        int posicion = contador1;
        /// iterar
        int contador2 = contador1 - 1;
        while(contador2 >= 0)
        {
            if(lista[posicion] > lista[contador2])
            {
                posicion = contador2;
            }
            contador2--;
        }
        /// intercambiar valores
        if(posicion != contador1)
        {
            int temporal = lista[contador1];
            lista[contador1] = lista[posicion];
            lista[posicion] = temporal;
        }
        contador1--;
    }
    /// imprimir en pantalla
    int contador2 = p - 1;
    while(contador2 >= 0 )
    {
        printf("%d ", lista[contador2]);
        contador2--;
    }
    printf("\n");
    /// liberar memoria
    free(lista);
}
