/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <stdio.h>
#include <stdlib.h>

/// own headers
#include "examples_kast/headers.h"

/**
function: group02_function02_version00
description: insert sort
arguments:
n number of elements
stringValues values as strings
returns: nothing
*/
void group02_function02_version00(unsigned int n, const char **stringValues)
{
    /// variable preparation
    unsigned int i = 0;
    unsigned int j = 0;
    int temp = 0;
    int list[n];

    /// iterate and convert to int
    for(i = 0; i < n; i++)
    {
        list[i] = atoi(stringValues[i]);
    }

    /// iterate
    for(i = 1 ; i <  n; i++)
    {
        /// iterate
        for(j = i ; j > 0; j--)
        {
            /// swap
            if(list[j] < list[j-1])
            {
                temp = list[j];
                list[j] = list[j-1];
                list[j-1] = temp;
            }
            else
            {
                break;
            }
        }
    }

    /// print
    for(j = 0; j < n; j++)
    {
        printf("%d ", list[j]);
    }
    printf("\n");
}
