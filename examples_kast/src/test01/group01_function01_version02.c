/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <string.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group01_function01_version02
descripcion: n spectrum kernel
argumentos:
cadenaA cadena A
cadenaB cadena B
n tamano cadena objetivo
retorn: valor del kernel
*/
int group01_function01_version02(char *cadenaA, char *cadenaB, int n)
{
    /// chequear tamanos correctos
    int tamanoA = (int)strlen(cadenaA);
    int tamanoB = (int)strlen(cadenaB);
    if(n > tamanoA || n > tamanoB || n <= 0)
    {
        return 0; // terminar
    }
    /// preparar variables
    int valorKernel = 0; // valor kernel
    char subcadena[n+1]; // subcadena actual
    memset(subcadena, 0, (n+1)*sizeof(char)); // inicializar en ceros
    char cadenasRepetidas[tamanoA][n+1]; // lista de cadenas repetidas
    int cantidadRepetidas = 0; // y la cantidad de ellas
    char *apuntador = NULL; // apuntador a una cadena
    int contador1 = 0;
    /// iterar en la cadena A en busca de subcadenas de tamano n
    for(contador1 = 0, apuntador = cadenaA; contador1 < tamanoA - n + 1; contador1++, apuntador++)
    {
        /// preparar variables
        int conteoA = 0;
        int conteoB = 0;
        char *apuntadorTemporal = NULL;
        int contador2 = 0;
        int saltar = 0;
        /// extraer la subcadena
        strncpy(subcadena, apuntador, n);
        /// buscar en la lista de subcadenas y rechazar si se encuentra
        for(contador2 = 0; contador2 < cantidadRepetidas; contador2++) // iterar
        {
            if(strcmp(cadenasRepetidas[contador2],subcadena)==0) // comparar
            {
                saltar = 1; // encontrada
                break; // salir del ciclo
            }
        }
        if(saltar == 1) // si se encontro
        {
            continue; // pasar directamente a la siguiente cadena
        }
        /// buscar todas las ocurrencias en A
        apuntadorTemporal = cadenaA;
        while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
        {
            conteoA++;
            apuntadorTemporal++;
        }
        /// buscar todas las ocurrencias en B
        apuntadorTemporal = cadenaB;
        while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
        {
            conteoB++;
            apuntadorTemporal++;
        }
        /// no hay coincidencias? continuar a la siguiente cadena
        if(conteoB == 0)
        {
            continue;
        }
        /// mas de una coincidencia en A? registrar como repetida para evitar doble calculo
        if(conteoA > 1)
        {
            strcpy(cadenasRepetidas[cantidadRepetidas],subcadena);
            cantidadRepetidas++;
        }
        /// sumar al producto interno
        valorKernel += conteoA * conteoB;
    }
    /// develver el valor del kernel
    return valorKernel;
}
