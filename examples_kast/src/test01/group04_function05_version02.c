/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
function: group04_function05_version02
descripcion: 2D stencil: another non-compact stencil with summation
argumentos:
p numero de elementos
cadenaValores valores como cadenas
retorna: nada
*/
void group04_function05_version02(int p, char **cadenaValores)
{
    /// preparar variables
    long matrizOriginal[p+6][p+6];
    memset(matrizOriginal, 0, sizeof(matrizOriginal));
    long matrizCalculada[p+6][p+6];
    memset(matrizCalculada, 0, sizeof(matrizCalculada));
    int contador1 = 0;
    int contador2 = 0;
    /// iterar y convertir a entero
    for(contador1 = 3; contador1 <= p+2; contador1++)
    {
        for(contador2 = 3; contador2 <= p+2; contador2++)
        {
            matrizOriginal[contador1][contador2] = atol(cadenaValores[contador2-3]);
        }
    }
    /// calcular el stencil
    for(contador1 = 3; contador1 <= p+2; contador1++)
    {
        for(contador2 = 3; contador2 <= p+2; contador2++)
        {
             matrizCalculada[contador1][contador2] =
                    matrizOriginal[contador1-1][contador2+0] +
                    matrizOriginal[contador1+0][contador2-1] +
                    matrizOriginal[contador1+0][contador2+0] +
                    matrizOriginal[contador1+0][contador2+1] +
                    matrizOriginal[contador1+1][contador2+0] +
                    matrizOriginal[contador1-2][contador2+0] +
                    matrizOriginal[contador1+0][contador2-2] +
                    matrizOriginal[contador1+0][contador2+2] +
                    matrizOriginal[contador1+2][contador2+0] +
                    matrizOriginal[contador1-3][contador2+0] +
                    matrizOriginal[contador1+0][contador2-3] +
                    matrizOriginal[contador1+0][contador2+3] +
                    matrizOriginal[contador1+3][contador2+0] ;
        }
    }
    /// imprimir en pantalla
    for(contador1 = 0; contador1 < p+6; contador1++)
    {
        for(contador2 = 0; contador2 < p+6; contador2++)
        {
            printf("%ld ", matrizCalculada[contador1][contador2]);
        }
        printf("\n");
    }
    printf("\n");
}
