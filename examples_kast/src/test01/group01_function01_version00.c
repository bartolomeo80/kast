/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <string.h>

/// own headers
#include "examples_kast/headers.h"

/**
function: group01_function01_version00
description: k spectrum kernel
arguments:
stringA string A
stringB string B
k target lenght
returns: kernel evaluation
*/
unsigned int group01_function01_version00(char *stringA, char *stringB, unsigned int k)
{
    /// check for correct lenghts
    unsigned int lenA = (unsigned int) strlen(stringA);
    unsigned int lenB = (unsigned int) strlen(stringB);
    if(k > lenA || k > lenB || k <= 0)
    {
        return 0; // terminate
    }

    /// variable preparation
    unsigned int kernelValue = 0; // kernel value
    char substring[k+1]; // current substring
    memset(substring, 0, (k+1)*sizeof(char)); // zero it
    char repeatedStrings[lenA][k+1]; // list of repeated strings
    unsigned int repeatedLength = 0; // and its current length
    char *pointer = 0; // string pointer
    unsigned int i = 0;

    /// iterate in search of substrings of size k on string A
    for(i = 0, pointer = stringA; i < lenA - k + 1; i++, pointer++)
    {
        /// prepare variables
        unsigned int countA = 0;
        unsigned int countB = 0;
        unsigned int j = 0;
        unsigned int skip = 0;
        const char *tmp = 0;

        /// get the substring
        strncpy(substring, pointer, k);

        /// search in the repeated list, reject if found
        for(j = 0; j < repeatedLength; j++) // iterate
        {
            if(strcmp(repeatedStrings[j],substring)==0) // compare
            {
                skip = 1; // found
                break; // stop
            }
        }
        if(skip == 1) // if found
        {
            continue; // go to the next substring
        }

        /// search all cases in A
        tmp = stringA;
        while((tmp = strstr(tmp, substring)) != 0)
        {
            countA++;
            tmp++;
        }

        /// search all cases in B
        tmp = stringB;
        while((tmp = strstr(tmp, substring)) != 0)
        {
            countB++;
            tmp++;
        }

        /// no matches? continue to next string
        if(countB == 0)
        {
            continue;
        }

        /// more than 1 match in A? register as repeated to avoid recalculation
        if(countA > 1)
        {
            strcpy(repeatedStrings[repeatedLength],substring);
            repeatedLength++;
        }

        /// add the feature to the inner product
        kernelValue += countA*countB;
    }

    /// return the kernel value
    return kernelValue;
}
