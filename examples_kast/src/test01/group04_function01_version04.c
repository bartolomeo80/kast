/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
function: group04_function01_version04
descripcion: 2D stencil: compact stencil with summation
argumentos:
p numero de elementos
cadenaValores valores como cadenas
retorna: nada
*/
void group04_function01_version04(int p, char **cadenaValores)
{
    /// preparar variables
    long **matrizOriginal = (long **)malloc((p+2)*sizeof(long *));
    long **matrizCalculada = (long **)malloc((p+2)*sizeof(long *));
    int contador1 = 0;
    /// iterar y convertir a entero
    for(contador1 = 0; contador1 < p+2; contador1++)
    {
        matrizOriginal[contador1] = (long *)malloc((p+2)*sizeof(long));
        matrizCalculada[contador1] = (long *)malloc((p+2)*sizeof(long));
        int contador2 = 0;
        for(contador2 = 0; contador2 < p+2; contador2++)
        {
            if(contador1 > 0 && contador1 < p + 1 && contador2 > 0 && contador2 < p + 1)
            {
                 matrizOriginal[contador1][contador2] = atol(cadenaValores[contador2-1]);
            }
            else
            {
                matrizOriginal[contador1][contador2] = 0;
            }
            matrizCalculada[contador1][contador2] = 0;
        }
    }
    /// calcular el stencil
    for(contador1 = 1; contador1 <= p; contador1++)
    {
        int contador2 = 0;
        for(contador2 = 1; contador2 <= p; contador2++)
        {
            int offset1 = 0;
            for(offset1 = -1; offset1 < 2; offset1++)
            {
                int offset2 = 0;
                for(offset2 = -1; offset2 < 2; offset2++)
                {
                    matrizCalculada[contador1][contador2] += matrizOriginal[contador1+offset1][contador2+offset2];
                }
            }
        }
    }
    /// imprimir en pantalla
    for(contador1 = 0; contador1 < p+2; contador1++)
    {
        int contador2 = 0;
        for(contador2 = 0; contador2 < p+2; contador2++)
        {
            printf("%ld ", matrizCalculada[contador1][contador2]);
        }
        free(matrizOriginal[contador1]);
        free(matrizCalculada[contador1]);
        printf("\n");
    }
    printf("\n");
    free(matrizOriginal);
    free(matrizCalculada);
}

