/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/

/// libraries
#include <stdio.h>
#include <stdlib.h>

/// own headers
#include "examples_kast/headers.h"

/**
function: group03_function02_version00
description: 3D stencil: side stencil with summation
arguments:
n number of elements
stringValues values as strings
returns: nothing
*/
void group03_function02_version00(unsigned int n, const char **stringValues)
{
    /// prepare variables
    int originalMatrix[n+2][n+2][n+2];
    memset(originalMatrix, 0, sizeof(originalMatrix));
    int calculatedMatrix[n+2][n+2][n+2];
    memset(calculatedMatrix, 0, sizeof(calculatedMatrix));
    unsigned int i = 0;
    unsigned int j = 0;
    unsigned int k = 0;

    /// iterate and convert to int
    for(i = 1; i <= n; i++)
    {
        for(j = 1; j <= n; j++)
        {
            for(k = 1; k <= n; k++)
            {
                originalMatrix[i][j][k] = atoi(stringValues[k-1]);
            }
        }
    }

    /// stencil calculation
    for(i = 1; i <= n; i++)
    {
        for(j = 1; j <= n; j++)
        {
            for(k = 1; k <= n; k++)
            {
                calculatedMatrix[i][j][k] =
                    originalMatrix[i-1][j+0][k+0] +
                    originalMatrix[i+0][j-1][k+0] +
                    originalMatrix[i+0][j+0][k-1] +
                    originalMatrix[i+0][j+0][k+0] +
                    originalMatrix[i+0][j+0][k+1] +
                    originalMatrix[i+0][j+1][k+0] +
                    originalMatrix[i+1][j+0][k+0] ;
            }
        }
    }

    /// print
    for(i = 0; i < n+2; i++)
    {
        for(j = 0; j < n+2; j++)
        {
            for(k = 0; k < n+2; k++)
            {
                printf("%d ", calculatedMatrix[i][j][k]);
            }
            printf("\n");
        }
        printf("\n\n");
    }
}
