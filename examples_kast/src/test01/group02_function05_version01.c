/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <stdio.h>
#include <stdlib.h>
#include "examples_kast/headers.h"
void  group02_function05_version01(unsigned  int  n,  const char  **stringValues){
    unsigned int i=0;
    unsigned int j=0;
    unsigned int k=0;
    unsigned int left=0;
    unsigned int rght=0;
    unsigned int rend=0;
    unsigned int m=0;
    int temp=0;
    int list[n];
    int listB[n];
    for(i=0;i<n;i++){
        list[i]=atoi(stringValues[i]);
    }
    for (k=1;k<n;k*=2){
        for (left=0;left+k<n;left+=k*2){
            rght=left+k;
            rend=rght+k;
            if (rend>n){
                rend=n;
            }
            m=left;
            i=left;
            j=rght;
            while (i<rght&&j<rend){
                if (list[i]<=list[j]){
                    listB[m]=list[i];
                    i++;
                }
                else{
                    listB[m]=list[j];
                    j++;
                }
                m++;
            }
            while (i<rght){
                listB[m]=list[i];
                i++;
                m++;
            }
            while (j<rend){
                listB[m]=list[j];
                j++;
                m++;
            }
            for (m=left;m<rend;m++){
                list[m]=listB[m];
            }
        }
    }
    for(j=0;j<n;j++){
        printf("%d ",list[j]);
    }
    printf("\n");
}
