/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group02_function01_version02
descripcion: bubble sort
argumentos:
p numero de elementos
cadenasValores valores como cadenas
retorna: nada
*/
void group02_function01_version02(int p, char **cadenasValores)
{
    /// preparar variables
    int contador1 = 0;
    int contador2 = 0;
    int temporal = 0;
    int lista[p];
    /// iterar y convertir a entero
    for(contador1 = 0; contador1 < p; contador1++)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
    }
    /// iterar
    for(contador1 = 0 ; contador1 <  p - 1; contador1++)
    {
        /// iterar
        for(contador2 = 0 ; contador2 < p - contador1 - 1; contador2++)
        {
            /// intercambiar valores
            if(lista[contador2] > lista[contador2+1])
            {
                temporal = lista[contador2];
                lista[contador2] = lista[contador2+1];
                lista[contador2+1] = temporal;
            }
        }
    }
    /// imprimir en pantalla
    for(contador2 = 0; contador2 < p; contador2++)
    {
        printf("%d ", lista[contador2]);
    }
    printf("\n");
}
