/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <string.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group01_function01_version03
descripcion: n spectrum kernel
argumentos:
n tamano cadena objetivo
cadenaA cadena A
cadenaB cadena B
retorn: valor del kernel
*/
int group01_function01_version03(int n, char *cadenaA, char *cadenaB)
{
    /* deletion */
    /// chequear tamanos correctos
    if(n > strlen(cadenaA) || n > strlen(cadenaB) || n <= 0) /* modification */
    {
        return 0; // terminar
    }
    /// preparar variables
    int valorKernel = 0; // valor kernel
    char subcadena[n+1]; // subcadena actual
    memset(subcadena, 0, (n+1)*sizeof(char)); // inicializar en ceros
    char cadenasRepetidas[strlen(cadenaA)][n+1]; // lista de cadenas repetidas
    int cantidadRepetidas = 0; // y la cantidad de ellas
    char *apuntador = NULL; // apuntador a una cadena
    int contador1 = 0;
    /// iterar en la cadena A en busca de subcadenas de tamano n
    for(contador1 = 0, apuntador = cadenaA; contador1 < strlen(cadenaA) - n + 1; contador1++, apuntador++)
    {
        /// preparar variables
        int featureValue = 0; /* addition */
        int conteoA = 0;
        int conteoB = 0;
        int contador2 = 0;
        int saltar = 0;
        char *apuntadorTemporal = NULL;
        /// extraer la subcadena
        strncpy(subcadena, apuntador, n);
        /// buscar en la lista de subcadenas y rechazar si se encuentra
        for(contador2 = 0; contador2 < cantidadRepetidas; contador2++) // iterar
        {
            if(strcmp(cadenasRepetidas[contador2],subcadena)==0) // comparar
            {
                saltar = 1; // encontrada
                break; // salir del ciclo
            }
        }
        if(saltar == 0) // si se encontro /* modification */
        {
            /// buscar todas las ocurrencias en A
            apuntadorTemporal = cadenaA;
            while(apuntadorTemporal = strstr(apuntadorTemporal, subcadena))
            {
                conteoA++;
                apuntadorTemporal++;
            }
            /// buscar todas las ocurrencias en B
            apuntadorTemporal = cadenaB;
            while(apuntadorTemporal = strstr(apuntadorTemporal, subcadena))
            {
                conteoB++;
                apuntadorTemporal++;
            }
            /// hay coincidencias? calcular la caracteristica
            if(conteoB == 0)
            {
                continue;
            }
            /// mas de una coincidencia en A? registrar como repetida para evitar doble calculo
            if(conteoA > 1)
            {
                strcpy(cadenasRepetidas[cantidadRepetidas],subcadena);
                cantidadRepetidas++;
            }
            /// calcular caracteristica
            featureValue = conteoA * conteoB; /* addition */
            /// sumar al producto interno
            valorKernel += featureValue;
        }
    }
    /// develver el valor del kernel
    return valorKernel;
}
