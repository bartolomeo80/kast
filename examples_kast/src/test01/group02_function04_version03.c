/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group02_function04_version03
descripcion: heap sort
argumentos:
cadenasValores valores como cadenas
p numero de elementos
retorna: nada
*/
void group02_function04_version03(char **cadenasValores, int p)
{
    /// preparar variables
    int contador1 = 0;
    int lista[p];
    memset(lista,0,p*sizeof(int));
    /// iterar y convertir a entero
    for(contador1 = 0; contador1 < p; contador1++)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
    }
    /// preparacion de la pila
    for (contador1 = 1; contador1 < p; contador1++)
    {
        int contenedor = contador1;
        while (contenedor != 0)
        {
            int raiz = (contenedor - 1) / 2;
            if (lista[raiz] < lista[contenedor])
            {
                int temporal = lista[contenedor];
                lista[contenedor] = lista[raiz];
                lista[raiz] = temporal;
            }
            contenedor = raiz;
        }
    }
    /// ordenar
    int contador2 = 0;
    for (contador2 = p - 1; contador2 >= 0; contador2--)
    {
        int temporal = lista[contador2];
        lista[contador2] = lista[0];
        lista[0] = temporal;
        int raiz = 0;
        int contenedor = 0;
        while (contenedor < contador2)
        {
            contenedor = 2 * raiz + 1;
            if ((lista[contenedor] < lista[contenedor + 1]) && contenedor < contador2 - 1)
            {
                contenedor++;
            }
            if (lista[raiz] < lista[contenedor] && contenedor < contador2)
            {
                temporal = lista[contenedor];
                lista[contenedor] = lista[raiz];
                lista[raiz] = temporal;
            }
            raiz = contenedor;
        }
    }
    /// imprimir en pantalla
    for(contador2 = 0; contador2 < p; contador2++)
    {
        printf("%d ", lista[contador2]);
    }
    printf("\n");
}
