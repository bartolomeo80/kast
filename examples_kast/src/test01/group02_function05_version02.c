/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group02_function01_version02
descripcion: bubble sort
argumentos:
p numero de elementos
cadenasValores valores como cadenas
retorna: nada
*/
void group02_function05_version02(int p, char **cadenasValores)
{
    /// preparar variables
    int contador1 = 0;
    int contador2 = 0;
    int contador3 = 0;
    int izquierda = 0;
    int derecha = 0;
    int derechafin = 0;
    int medio = 0;
    int temporal = 0;
    int lista[p];
    int listaB[p];
    /// iterar y convertir a entero
    for(contador1 = 0; contador1 < p; contador1++)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
    }
    /// ordenar
    for (contador3 = 1; contador3 < p; contador3 *= 2 )
    {
        for (izquierda = 0; izquierda + contador3 < p; izquierda += contador3*2)
        {
            derecha = izquierda + contador3;
            derechafin = derecha + contador3;
            if (derechafin > p)
            {
                derechafin = p;
            }
            medio = izquierda;
            contador1 = izquierda;
            contador2 = derecha;
            while (contador1 < derecha && contador2 < derechafin)
            {
                if (lista[contador1] <= lista[contador2])
                {
                    listaB[medio] = lista[contador1];
                    contador1++;
                }
                else
                {
                    listaB[medio] = lista[contador2];
                    contador2++;
                }
                medio++;
            }
            while (contador1 < derecha)
            {
                listaB[medio] = lista[contador1];
                contador1++;
                medio++;
            }
            while (contador2 < derechafin)
            {
                listaB[medio] = lista[contador2];
                contador2++;
                medio++;
            }
            for (medio = izquierda; medio < derechafin; medio++)
            {
                lista[medio] = listaB[medio];
            }
        }
    }
    /// imprimir en pantalla
    for(contador2 = 0; contador2 < p; contador2++)
    {
        printf("%d ", lista[contador2]);
    }
    printf("\n");
}
