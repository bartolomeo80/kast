/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <string.h>
#include <math.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group01_function01_version04
descripcion: n spectrum kernel
argumentos:
cadenaA cadena A
cadenaB cadena B
n tamano cadena objetivo
valorKernel valor kernel
normalizado normalizado
retorna: vacio
*/
void group01_function01_version04(char *cadenaA, char *cadenaB, int n, float *valorKernel, int normalizado)
{
    if(n > 0) // mayor que cero
    {
        if(n <= strlen(cadenaB)) // menor o igual que el tamano de B
        {
            if(n <= strlen(cadenaA)) // menor o igual que el tamano de A
            {
                /// preparar variables
                char cadenasRepetidas[strlen(cadenaB)][n+1]; // lista de cadenas repetidas
                int cantidadRepetidas = 0; // y la cantidad de ellas
                char *apuntador = cadenaB; // apuntador a una cadena
                int contador1 = 0;
                /// iterar en la cadena B en busca de subcadenas de tamano n
                while(contador1 < strlen(cadenaB) - n + 1)
                {
                    /// extraer la subcadena
                    char subcadena[n+1]; // subcadena actual
                    memset(subcadena, 0, (n+1)*sizeof(char)); // inicializar en ceros
                    strncpy(subcadena, apuntador, n);
                    int conteoA = 0;
                    int conteoB = 0;
                    /// buscar todas las ocurrencias en B
                    conteoB++; // una ya se tiene por adelantado
                    char *apuntadorTemporal = apuntador + n;
                    while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
                    {
                        conteoB++;
                        apuntadorTemporal++;
                    }
                    /// buscar todas las ocurrencias en A
                    apuntadorTemporal = cadenaA;
                    while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
                    {
                        conteoA++;
                        apuntadorTemporal++;
                    }
                    int contador2 = 0;
                    while(contador2 < cantidadRepetidas) // iterar
                    {
                        if(strcmp(cadenasRepetidas[contador2],subcadena)==0) // comparar
                        {
                            break; // salir del ciclo
                        }
                        contador2++;
                    }
                    /// no estaba registrada
                    if(contador2 == cantidadRepetidas)
                    {
                        /// actualizar el valor del kernel
                        (*valorKernel) += conteoA * conteoB;
                        /// mas de una coincidencia en B? registrar como repetida para evitar doble calculo
                        if(conteoB > 1)
                        {
                            if(conteoA > 0) // hubo coincidencias?
                            {
                                /// agregar a la lista de repetidas
                                strcpy(cadenasRepetidas[cantidadRepetidas],subcadena);
                                cantidadRepetidas++;
                            }
                        }
                    }
                    /// adelantar apuntador y contador
                    contador1++;
                    apuntador++;
                }
                /// si se requiere normalizar
                if(normalizado == 1)
                {
                    float valorKernelAA = 0.0;
                    float valorKernelBB = 0.0;
                    group01_function01_version04(cadenaA,cadenaA,n,&valorKernelAA,0); // calcular kernel AA
                    group01_function01_version04(cadenaB,cadenaB,n,&valorKernelBB,0); // calcular kernel BB
                    /// actualizar el kernel con el valor normalizado
                    (*valorKernel)=(float)(*valorKernel)/(float)sqrt(valorKernelAA*valorKernelBB);
                }
            }
        }
    }
}
