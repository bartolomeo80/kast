/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <string.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group01_function04_version03
descripcion: bag of words kernel
argumentos:
separador separador de palabras
cadenaA cadena A
cadenaB cadena B
retorn: valor del kernel
*/
int group01_function04_version03(char separador, char *cadenaA, char *cadenaB)
{
    int valorKernel = 0; // valor kernel
    /// incrementar el tamano de A y B para mejorar el proceso de busqueda
    char *copiaA = NULL;
    asprintf(&copiaA, "%c%s%c", separador, cadenaA, separador);
    char *copiaB = NULL;
    asprintf(&copiaB, "%c%s%c", separador, cadenaB, separador);
    /// preparar variables
    char cadenasRepetidas[strlen(copiaA)][strlen(copiaA)]; // lista de cadenas repetidas
    int cantidadRepetidas = 0; // y la cantidad de ellas
    char *apuntador = NULL; // apuntador a una cadena
    int contador1 = 0;
    char *inicioPalabra = NULL;
    int n = 0; // tamano palabra
    /// iterar en la cadena A en busca de subcadenas de tamano n
    for(contador1 = 0, apuntador = copiaA; contador1 < strlen(copiaA); n++, contador1++, apuntador++)
    {
        /// comienzo de palabra?
        if(copiaA[contador1] != separador && contador1 > 0 && copiaA[contador1-1] == separador)
        {
            n = 1;
            inicioPalabra = apuntador - 1;
        }
        /// fin de palabra?
        if(copiaA[contador1] != separador && contador1 < strlen(copiaA) -1 && copiaA[contador1+1] == separador)
        {
            n = n + 2;
            /// preparar variables
            int conteoA = 0;
            int conteoB = 0;
            int contador2 = 0;
            int saltar = 0;
            char *apuntadorTemporal = NULL;
            /// extraer la subcadena
            char subcadena[n+1]; // subcadena actual
            memset(subcadena, 0, (n+1)*sizeof(char)); // inicializar en ceros
            strncpy(subcadena, inicioPalabra, n);
            /// buscar en la lista de subcadenas y rechazar si se encuentra
            for(contador2 = 0; contador2 < cantidadRepetidas; contador2++) // iterar
            {
                if(strcmp(cadenasRepetidas[contador2],subcadena)==0) // comparar
                {
                    saltar = 1; // encontrada
                    break; // salir del ciclo
                }
            }
            if(saltar == 0) // si se encontro
            {
                /// buscar todas las ocurrencias en A
                apuntadorTemporal = copiaA;
                while(apuntadorTemporal = strstr(apuntadorTemporal, subcadena))
                {
                    conteoA++;
                    apuntadorTemporal++;
                }
                /// buscar todas las ocurrencias en B
                apuntadorTemporal = copiaB;
                while(apuntadorTemporal = strstr(apuntadorTemporal, subcadena))
                {
                    conteoB++;
                    apuntadorTemporal++;
                }
                /// hay coincidencias? calcular la caracteristica
                if(conteoB > 0)
                {
                    /// mas de una coincidencia en A? registrar como repetida para evitar doble calculo
                    if(conteoA > 1)
                    {
                        strcpy(cadenasRepetidas[cantidadRepetidas],subcadena);
                        cantidadRepetidas++;
                    }
                    /// sumar al producto interno
                    valorKernel += conteoA * conteoB;
                }
            }
        }
    }
    /// liberar apuntadores
    free(copiaA);
    free(copiaB);
    /// develver el valor del kernel
    return valorKernel;
}
