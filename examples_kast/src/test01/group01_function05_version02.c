/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <string.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group01_function05_version02
descripcion: sentence kernel
argumentos:
cadenaA cadena A
cadenaB cadena B
apertura sentence delimiter
cierre sentence delimiter
retorn: valor del kernel
*/
int group01_function05_version02(char *cadenaA, char *cadenaB, char apertura, char cierre)
{
    /// chequear tamanos correctos
    int tamanoA = (int)strlen(cadenaA);
    /// preparar variables
    int valorKernel = 0; // valor kernel
    char cadenasRepetidas[tamanoA][tamanoA]; // lista de cadenas repetidas
    int cantidadRepetidas = 0; // y la cantidad de ellas
    char *apuntador = NULL; // apuntador a una cadena
    int contador1 = 0;
    char *inicioPalabra = NULL;
    int n = 0; // tamano palabra
    /// iterar en la cadena A en busca del separador dado
    for(contador1 = 0, apuntador = cadenaA; contador1 < tamanoA ; n++, contador1++, apuntador++)
    {
        /// apertura
        if(cadenaA[contador1] == apertura && contador1 < tamanoA - 2 && cadenaA[contador1+1] != apertura && cadenaA[contador1+1] != cierre && inicioPalabra == NULL)
        {
            n = 0;
            inicioPalabra = apuntador;
        }
        /// cierre
        if(cadenaA[contador1] == cierre && contador1 > 1 && cadenaA[contador1-1] != apertura && cadenaA[contador1-1] != cierre && inicioPalabra != NULL)
        {
            n = n + 1;
            char subcadena[n+1]; // subcadena actual
            memset(subcadena, 0, (n+1)*sizeof(char)); // inicializar en ceros
            strncpy(subcadena, inicioPalabra, n);
            /// buscar en la lista de subcadenas y rechazar si se encuentra
            int contador2 = 0;
            int saltar = 0;
            for(contador2 = 0; contador2 < cantidadRepetidas; contador2++) // iterar
            {
                if(strcmp(cadenasRepetidas[contador2],subcadena)==0) // comparar
                {
                    saltar = 1; // encontrada
                    break; // salir del ciclo
                }
            }
            if(saltar == 1) // si se encontro
            {
                n = 0;
                inicioPalabra = NULL;
                continue; // pasar directamente a la siguiente cadena
            }
            /// buscar todas las ocurrencias en A
            int conteoA = 0;
            char *apuntadorTemporal = cadenaA;
            while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
            {
                conteoA++;
                apuntadorTemporal++;
            }
            /// buscar todas las ocurrencias en B
            int conteoB = 0;
            apuntadorTemporal = cadenaB;
            while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
            {
                conteoB++;
                apuntadorTemporal++;
            }
            /// no hay coincidencias? continuar a la siguiente cadena
            if(conteoB == 0)
            {
                n = 0;
                inicioPalabra = NULL;
                continue;
            }
            /// mas de una coincidencia en A? registrar como repetida para evitar doble calculo
            if(conteoA > 1)
            {
                strcpy(cadenasRepetidas[cantidadRepetidas],subcadena);
                cantidadRepetidas++;
            }
            /// calcular el valor de esta caracteristica
            int valorCaracteristica = conteoA * conteoB;
            /// sumar al producto interno
            valorKernel += valorCaracteristica;
            n = 0;
            inicioPalabra = NULL;
        }
    }
    /// devolver el valor del kernel
    return valorKernel;
}
