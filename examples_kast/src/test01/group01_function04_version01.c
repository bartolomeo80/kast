/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <string.h>
#include "examples_kast/headers.h"
unsigned  int  group01_function04_version01(const  char  *stringA,  const  char  *stringB,  const  char  separator){
    char *copyA = NULL;
    asprintf(&copyA,"%c%s%c",separator,stringA,separator);
    char *copyB = NULL;
    asprintf(&copyB,"%c%s%c",separator,stringB,separator);
    unsigned int lenA=(unsigned int)strlen(copyA);
    unsigned int kernelValue=0;
    char repeatedStrings[lenA][lenA];
    unsigned int repeatedLenght=0;
    char *pointer=0;
    unsigned int i=0;
    char *wordStart=NULL;
    unsigned int k=0;
    for(i=0,pointer=copyA;i<lenA;k++,i++,pointer++){
        if(copyA[i]!=separator&&i>0&&copyA[i-1]==separator){
            k=1;
            wordStart=pointer-1;
        }
        if(copyA[i]!=separator&&i<lenA-1&&copyA[i+1]==separator){
            k=k+2;
            char substring[k+1];
            memset(substring,0,(k+1)*sizeof(char));
            strncpy(substring,wordStart,k);
            unsigned int j=0;
            unsigned int skip=0;
            for(j=0;j<repeatedLenght;j++){
                if(strcmp(repeatedStrings[j],substring)==0){
                    skip=1;
                    break;
                }
            }
            if(skip==1){
                continue;
            }
            unsigned int countA=0;
            const char *tmp=copyA;
            while((tmp=strstr(tmp,substring))!=0){
                countA++;
                tmp++;
            }
            unsigned int countB=0;
            tmp=copyB;
            while((tmp=strstr(tmp,substring))!=0){
                countB++;
                tmp++;
            }
            if(countB==0){
                continue;
            }
            if(countA>1){
                strcpy(repeatedStrings[repeatedLenght],substring);
                repeatedLenght++;
            }
            unsigned int featureValue=countA*countB;
            kernelValue+=featureValue;
        }
    }
    free(copyA);
    free(copyB);
    return kernelValue;
}
