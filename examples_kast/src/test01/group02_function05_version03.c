/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group02_function05_version03
descripcion: merge sort
argumentos:
cadenasValores valores como cadenas
p numero de elementos
retorna: nada
*/
void group02_function05_version03(char **cadenasValores, int p)
{
    /// preparar variables
    int contador1 = 0;
    int lista[p];
    memset(lista,0,p*sizeof(int));
    int listaB[p];
    memset(listaB,0,p*sizeof(int));
    /// iterar y convertir a entero
    for(contador1 = 0; contador1 < p; contador1++)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
    }
    /// ordenar
    int contador3 = 0;
    for (contador3 = 1; contador3 < p; contador3 *= 2 )
    {
        int izquierda = 0;
        for (izquierda = 0; izquierda + contador3 < p; izquierda += contador3*2)
        {
            int derecha = izquierda + contador3;
            int derechafin = derecha + contador3;
            if (derechafin > p)
            {
                derechafin = p;
            }
            int medio = izquierda;
            contador1 = izquierda;
            int contador2 = derecha;
            while (contador1 <= derecha - 1 && contador2 <= derechafin -1)
            {
                if (lista[contador1] > lista[contador2])
                {
                    listaB[medio] = lista[contador2];
                    contador2++;
                }
                else
                {
                    listaB[medio] = lista[contador1];
                    contador1++;
                }
                medio++;
            }
            while (contador1 <= derecha -1)
            {
                listaB[medio] = lista[contador1];
                contador1++;
                medio++;
            }
            while (contador2 <= derechafin -1)
            {
                listaB[medio] = lista[contador2];
                contador2++;
                medio++;
            }
            for (medio = izquierda; medio <= derechafin -1; medio++)
            {
                lista[medio] = listaB[medio];
            }
        }
    }
    /// imprimir en pantalla
    int contador2 = 0;
    for(contador2 = 0; contador2 < p; contador2++)
    {
        printf("%d ", lista[contador2]);
    }
    printf("\n");
}
