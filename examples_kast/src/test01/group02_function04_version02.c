/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group02_function01_version02
descripcion: heap sort
argumentos:
p numero de elementos
cadenasValores valores como cadenas
retorna: nada
*/
void group02_function04_version02(int p, char **cadenasValores)
{
    /// preparar variables
    int contador1 = 0;
    int contador2 = 0;
    int temporal = 0;
    int lista[p];
    int contenedor = 0;
    int raiz = 0;
    /// iterar y convertir a entero
    for(contador1 = 0; contador1 < p; contador1++)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
    }
    /// preparacion de la pila
    for (contador1 = 1; contador1 < p; contador1++)
    {
        contenedor = contador1;
        do
        {
            raiz = (contenedor - 1) / 2;
            if (lista[raiz] < lista[contenedor])
            {
                temporal = lista[raiz];
                lista[raiz] = lista[contenedor];
                lista[contenedor] = temporal;
            }
            contenedor = raiz;
        } while (contenedor != 0);
    }
    /// ordenar
    for (contador2 = p - 1; contador2 >= 0; contador2--)
    {
        temporal = lista[0];
        lista[0] = lista[contador2];
        lista[contador2] = temporal;
        raiz = 0;
        do
        {
            contenedor = 2 * raiz + 1;
            if ((lista[contenedor] < lista[contenedor + 1]) && contenedor < contador2 - 1)
            {
                contenedor++;
            }
            if (lista[raiz] < lista[contenedor] && contenedor < contador2)
            {
                temporal = lista[raiz];
                lista[raiz] = lista[contenedor];
                lista[contenedor] = temporal;
            }
            raiz = contenedor;
        } while (contenedor < contador2);
    }
    /// imprimir en pantalla
    for(contador2 = 0; contador2 < p; contador2++)
    {
        printf("%d ", lista[contador2]);
    }
    printf("\n");
}
