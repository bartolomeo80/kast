/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <string.h>
#include "examples_kast/headers.h"
unsigned  int  group01_function03_version01(const  char  *stringA,  const  char  *stringB){
    unsigned int lenA=(unsigned int) strlen(stringA);
    unsigned int lenB=(unsigned int) strlen(stringB);
    if(lenA<1||lenB<1){
        return 0;
    }
    unsigned int kernelValue=0;
    char substring[2];
    memset(substring,0,(2)*sizeof(char));
    char repeatedStrings[lenA][2];
    unsigned int repeatedLenght=0;
    char *pointer=0;
    unsigned int i=0;
    for(i=0,pointer=stringA;i<lenA;i++,pointer++){
        strncpy(substring,pointer,1);
        unsigned int j=0;
        unsigned int skip=0;
        for(j=0;j<repeatedLenght;j++){
            if(strcmp(repeatedStrings[j],substring)==0){
                skip=1;
                break;
            }
        }
        if(skip==1){
            continue;
        }
        unsigned int countA=0;
        const char *tmp=stringA;
        while((tmp=strstr(tmp, substring))!=0){
            countA++;
            tmp++;
        }
        unsigned int countB=0;
        tmp=stringB;
        while((tmp=strstr(tmp, substring))!=0){
            countB++;
            tmp++;
        }
        if(countB==0){
            continue;
        }
        if(countA>1){
            strcpy(repeatedStrings[repeatedLenght],substring);
            repeatedLenght++;
        }
        unsigned int featureValue=countA*countB;
        kernelValue+=featureValue;
    }
    return kernelValue;
}
