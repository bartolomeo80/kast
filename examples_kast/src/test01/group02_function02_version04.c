/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group02_function02_version04
descripcion: insert sort
argumentos:
p numero de elementos
cadenasValores valores como cadenas
retorna: nada
*/
void group02_function02_version04(int p, char **cadenasValores)
{
    /// preparar variables
    int contador1 = p - 1;
    int *lista = (int*) malloc(p*sizeof(int));
    memset(lista,0,p*sizeof(int));
    /// iterar y convertir a entero
    while(contador1 >= 0)
    {
        lista[contador1] = atoi(cadenasValores[contador1]);
        contador1--;
    }
    /// iterar
    contador1 = p - 2;
    while(contador1 >= 0)
    {
        /// iterar
        int contador2 = contador1;
        while(contador2 < p - 1)
        {
            /// intercambiar valores
            if(lista[contador2] < lista[contador2+1])
            {
                int temporal = lista[contador2+1];
                lista[contador2+1] = lista[contador2];
                lista[contador2] = temporal;
            }
            else
            {
                break;
            }
            contador2++;
        }
        contador1--;
    }
    /// imprimir en pantalla
    int contador2 = p - 1;
    while(contador2 >= 0 )
    {
        printf("%d ", lista[contador2]);
        contador2--;
    }
    printf("\n");
    /// liberar memoria
    free(lista);
}
