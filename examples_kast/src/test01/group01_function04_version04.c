/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <string.h>
#include <math.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group01_function04_version04
descripcion: bag of words kernel
argumentos:
cadenaA cadena A
cadenaB cadena B
separador separador de palabras
valorKernel valor kernel
normalizado normalizado
retorna: vacio
*/
void group01_function04_version04(char *cadenaA, char *cadenaB, char separador, float *valorKernel, int normalizado)
{
    /// incrementar el tamano de A y B para mejorar el proceso de busqueda
    char *copiaA = NULL;
    asprintf(&copiaA, "%c%s%c", separador, cadenaA, separador);
    char *copiaB = NULL;
    asprintf(&copiaB, "%c%s%c", separador, cadenaB, separador);
    /// preparar variables
    char cadenasRepetidas[strlen(copiaB)][strlen(copiaB)]; // lista de cadenas repetidas
    int cantidadRepetidas = 0; // y la cantidad de ellas
    char *apuntador = copiaB; // apuntador a una cadena
    int contador1 = 0;
    char *inicioPalabra = NULL;
    unsigned int n = 0; // tamano palabra
    /// iterar en la cadena B en busca de subcadenas de tamano n
    while(contador1 < strlen(copiaB))
    {
        /// comienzo de palabra?
        if(copiaB[contador1] != separador && contador1 > 0 && copiaB[contador1-1] == separador)
        {
            n = 1;
            inicioPalabra = apuntador - 1;
        }
        /// fin de palabra?
        if(copiaB[contador1] != separador && contador1 < strlen(copiaB) -1 && copiaB[contador1+1] == separador)
        {
            n = n + 2;
            /// extraer la subcadena
            char subcadena[n+1]; // subcadena actual
            memset(subcadena, 0, (n+1)*sizeof(char)); // inicializar en ceros
            strncpy(subcadena, inicioPalabra, n);
            int conteoA = 0;
            int conteoB = 0;
            /// buscar todas las ocurrencias en B
            conteoB++; // una ya se tiene por adelantado
            char *apuntadorTemporal = inicioPalabra + n;
            while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
            {
                conteoB++;
                apuntadorTemporal++;
            }
            /// buscar todas las ocurrencias en A
            apuntadorTemporal = copiaA;
            while((apuntadorTemporal = strstr(apuntadorTemporal, subcadena)) != 0)
            {
                conteoA++;
                apuntadorTemporal++;
            }
            int contador2 = 0;
            while(contador2 < cantidadRepetidas) // iterar
            {
                if(strcmp(cadenasRepetidas[contador2],subcadena)==0) // comparar
                {
                    break; // salir del ciclo
                }
                contador2++;
            }
            /// no estaba registrada
            if(contador2 == cantidadRepetidas)
            {
                /// actualizar el valor del kernel
                (*valorKernel) += conteoA * conteoB;
                /// mas de una coincidencia en B? registrar como repetida para evitar doble calculo
                if(conteoB > 1)
                {
                    if(conteoA > 0) // hubo coincidencias?
                    {
                        /// agregar a la lista de repetidas
                        strcpy(cadenasRepetidas[cantidadRepetidas],subcadena);
                        cantidadRepetidas++;
                    }
                }
            }
        }
        /// adelantar apuntador y contador
        contador1++;
        apuntador++;
        n++;
    }
    /// si se requiere normalizar
    if(normalizado == 1)
    {
        float valorKernelAA = 0.0;
        float valorKernelBB = 0.0;
        group01_function04_version04(cadenaA,cadenaA,separador,&valorKernelAA,0); // calcular kernel AA
        group01_function04_version04(cadenaB,cadenaB,separador,&valorKernelBB,0); // calcular kernel BB
        /// actualizar el kernel con el valor normalizado
        (*valorKernel)=(float)(*valorKernel)/(float)sqrt(valorKernelAA*valorKernelBB);

    }
    /// liberar apuntadores
    free(copiaA);
    free(copiaB);

}
