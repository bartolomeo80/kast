/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
function: group03_function03_version02
descripcion: 3D stencil: edge stencil with summation
argumentos:
p numero de elementos
cadenaValores valores como cadenas
retorna: nada
*/
void group03_function03_version02(int p, char **cadenaValores)
{
    /// preparar variables
    long matrizOriginal[p+2][p+2][p+2];
    memset(matrizOriginal, 0, sizeof(matrizOriginal));
    long matrizCalculada[p+2][p+2][p+2];
    memset(matrizCalculada, 0, sizeof(matrizCalculada));
    int contador1 = 0;
    int contador2 = 0;
    int contador3 = 0;
    /// iterar y convertir a entero
    for(contador1 = 1; contador1 <= p; contador1++)
    {
        for(contador2 = 1; contador2 <= p; contador2++)
        {
            for(contador3 = 1; contador3 <= p; contador3++)
            {
                matrizOriginal[contador1][contador2][contador3] = atol(cadenaValores[contador3-1]);
            }
        }
    }
    /// calcular el stencil
    for(contador1 = 1; contador1 <= p; contador1++)
    {
        for(contador2 = 1; contador2 <= p; contador2++)
        {
            for(contador3 = 1; contador3 <= p; contador3++)
            {
                matrizCalculada[contador1][contador2][contador3] =
                    matrizOriginal[contador1-1][contador2-1][contador3+0] +
                    matrizOriginal[contador1-1][contador2+0][contador3-1] +
                    matrizOriginal[contador1-1][contador2+0][contador3+1] +
                    matrizOriginal[contador1-1][contador2+1][contador3+0] +
                    matrizOriginal[contador1+0][contador2-1][contador3-1] +
                    matrizOriginal[contador1+0][contador2-1][contador3+1] +
                    matrizOriginal[contador1+0][contador2+0][contador3+0] +
                    matrizOriginal[contador1+0][contador2+1][contador3-1] +
                    matrizOriginal[contador1+0][contador2+1][contador3+1] +
                    matrizOriginal[contador1+1][contador2-1][contador3+0] +
                    matrizOriginal[contador1+1][contador2+0][contador3-1] +
                    matrizOriginal[contador1+1][contador2+0][contador3+1] +
                    matrizOriginal[contador1+1][contador2+1][contador3+0] ;
            }
        }
    }
    /// imprimir en pantalla
    for(contador1 = 0; contador1 < p+2; contador1++)
    {
        for(contador2 = 0; contador2 < p+2; contador2++)
        {
            for(contador3 = 0; contador3 < p+2; contador3++)
            {
                printf("%ld ", matrizCalculada[contador1][contador2][contador3]);
            }
            printf("\n");
        }
        printf("\n\n");
    }
}
