/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <stdio.h>
#include <stdlib.h>
#include "examples_kast/headers.h"
void  group04_function04_version01(unsigned  int  n,  const  char  **stringValues){
    int originalMatrix[n+4][n+4];
    memset(originalMatrix,0,sizeof(originalMatrix));
    int calculatedMatrix[n+4][n+4];
    memset(calculatedMatrix,0,sizeof(calculatedMatrix));
    unsigned int i=0;
    unsigned int j=0;
    for(i=2;i<=n+1;i++){
        for(j=2;j<=n+1;j++){
            originalMatrix[i][j]=atoi(stringValues[j-2]);
        }
    }
    for(i=2;i<=n+1;i++){
        for(j=2;j<=n+1;j++){
            calculatedMatrix[i][j]=
                originalMatrix[i-1][j+0]+
                originalMatrix[i+0][j-1]+
                originalMatrix[i+0][j+0]+
                originalMatrix[i+0][j+1]+
                originalMatrix[i+1][j+0]+
                originalMatrix[i-2][j+0]+
                originalMatrix[i+0][j-2]+
                originalMatrix[i+0][j+2]+
                originalMatrix[i+2][j+0];
        }
    }
    for(i=0;i<n+4;i++){
        for(j=0;j<n+4;j++){
            printf("%d ",calculatedMatrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
