/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <stdio.h>
#include <stdlib.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
function: group04_function04_version03
descripcion: 2D stencil: non-compact stencil with summation
argumentos:
p numero de elementos
cadenaValores valores como cadenas
retorna: nada
*/
void group04_function04_version03(char **cadenaValores, int p)
{
    /// preparar variables
    long matrizOriginal[p+4][p+4];
    memset(matrizOriginal, 0, sizeof(matrizOriginal));
    int contador1 = 0;
    /// iterar y convertir a entero
    for(contador1 = 2; contador1 <= p+1; contador1++)
    {
        int contador2 = 0;
        for(contador2 = 2; contador2 <= p+1; contador2++)
        {
            matrizOriginal[contador1][contador2] = atol(cadenaValores[contador2-2]);
        }
    }
    long matrizCalculada[p+4][p+4];
    memset(matrizCalculada, 0, sizeof(matrizCalculada));
    /// calcular el stencil
    for(contador1 = 2; contador1 <= p+1; contador1++)
    {
        int contador2 = 0;
        for(contador2 = 2; contador2 <= p+1; contador2++)
        {
                /// filas superiores
                matrizCalculada[contador1][contador2] +=
                    matrizOriginal[contador1-1][contador2] +
                    matrizOriginal[contador1-2][contador2] ;
                /// fila central
                matrizCalculada[contador1][contador2] +=
                    matrizOriginal[contador1][contador2-2]+
                    matrizOriginal[contador1][contador2-1]+
                    matrizOriginal[contador1][contador2] +
                    matrizOriginal[contador1][contador2+1] +
                    matrizOriginal[contador1][contador2+2] ;
                /// filas inferiores
                matrizCalculada[contador1][contador2] +=
                    matrizOriginal[contador1+1][contador2] +
                    matrizOriginal[contador1+2][contador2] ;
        }
    }
    /// imprimir en pantalla
    for(contador1 = 0; contador1 < p+4; contador1++)
    {
        int contador2 = 0;
        for(contador2 = 0; contador2 < p+4; contador2++)
        {
            printf("%ld ", matrizCalculada[contador1][contador2]);
        }
        printf("\n");
    }
    printf("\n");
}
