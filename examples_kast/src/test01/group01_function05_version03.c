/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
/// bibliotecas
#include <string.h>
/// cabeceras propias
#include "examples_kast/headers.h"
/**
funcion: group01_function05_version03
descripcion: sentence kernel kernel
argumentos:
apertura sentence delimiter
cierre sentence delimiter
cadenaA cadena A
cadenaB cadena B
retorn: valor del kernel
*/
int group01_function05_version03(char apertura, char cierre, char *cadenaA, char *cadenaB)
{
    int valorKernel = 0; // valor kernel
    /// preparar variables
    char cadenasRepetidas[strlen(cadenaA)][strlen(cadenaA)]; // lista de cadenas repetidas
    int cantidadRepetidas = 0; // y la cantidad de ellas
    char *apuntador = NULL; // apuntador a una cadena
    int contador1 = 0;
    char *inicioPalabra = NULL;
    int n = 0; // tamano palabra
    /// iterar en la cadena A en busca de subcadenas de tamano n
    for(contador1 = 0, apuntador = cadenaA; contador1 < strlen(cadenaA); n++, contador1++, apuntador++)
    {
        n = n + 1;
        /// apertura
        if(cadenaA[contador1] == apertura && contador1 < strlen(cadenaA) - 2 && cadenaA[contador1+1] != apertura && cadenaA[contador1+1] != cierre && inicioPalabra == NULL)
        {
            n = 0;
            inicioPalabra = apuntador;
        }
        /// cierre
        if(cadenaA[contador1] == cierre && contador1 > 1 && cadenaA[contador1-1] != apertura && cadenaA[contador1-1] != cierre && inicioPalabra != NULL)
        {
            /// preparar variables
            int conteoA = 0;
            int conteoB = 0;
            int contador2 = 0;
            int saltar = 0;
            char *apuntadorTemporal = NULL;
            /// extraer la subcadena
            char subcadena[n+1]; // subcadena actual
            memset(subcadena, 0, (n+1)*sizeof(char)); // inicializar en ceros
            strncpy(subcadena, inicioPalabra, n);
            /// buscar en la lista de subcadenas y rechazar si se encuentra
            for(contador2 = 0; contador2 < cantidadRepetidas; contador2++) // iterar
            {
                if(strcmp(cadenasRepetidas[contador2],subcadena)==0) // comparar
                {
                    saltar = 1; // encontrada
                    break; // salir del ciclo
                }
            }
            if(saltar == 0) // si se encontro
            {
                /// buscar todas las ocurrencias en A
                apuntadorTemporal = cadenaA;
                while(apuntadorTemporal = strstr(apuntadorTemporal, subcadena))
                {
                    conteoA++;
                    apuntadorTemporal++;
                }
                /// buscar todas las ocurrencias en B
                apuntadorTemporal = cadenaB;
                while(apuntadorTemporal = strstr(apuntadorTemporal, subcadena))
                {
                    conteoB++;
                    apuntadorTemporal++;
                }
                /// hay coincidencias? calcular la caracteristica
                if(conteoB > 0)
                {
                    /// mas de una coincidencia en A? registrar como repetida para evitar doble calculo
                    if(conteoA > 1)
                    {
                        strcpy(cadenasRepetidas[cantidadRepetidas],subcadena);
                        cantidadRepetidas++;
                    }
                    /// sumar al producto interno
                    valorKernel += conteoA * conteoB;
                }
            }
            n = 0;
            inicioPalabra = NULL;
        }
    }
    /// develver el valor del kernel
    return valorKernel;
}
