/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <stdio.h>
#include <stdlib.h>
#include "examples_kast/headers.h"
void  group04_function05_version01(unsigned  int  n,  const  char  **stringValues){
    int originalMatrix[n+6][n+6];
    memset(originalMatrix,0,sizeof(originalMatrix));
    int calculatedMatrix[n+6][n+6];
    memset(calculatedMatrix,0,sizeof(calculatedMatrix));
    unsigned int i=0;
    unsigned int j=0;
    for(i=3;i<=n+2;i++){
        for(j=3;j<=n+2;j++){
            originalMatrix[i][j]=atoi(stringValues[j-3]);
        }
    }
    for(i=3;i<=n+2;i++){
        for(j=3;j<=n+2;j++){
            calculatedMatrix[i][j]=
                originalMatrix[i-1][j+0]+
                originalMatrix[i+0][j-1]+
                originalMatrix[i+0][j+0]+
                originalMatrix[i+0][j+1]+
                originalMatrix[i+1][j+0]+
                originalMatrix[i-2][j+0]+
                originalMatrix[i+0][j-2]+
                originalMatrix[i+0][j+2]+
                originalMatrix[i+2][j+0]+
                originalMatrix[i-3][j+0]+
                originalMatrix[i+0][j-3]+
                originalMatrix[i+0][j+3]+
                originalMatrix[i+3][j+0];
        }
    }
    for(i=0;i<n+6;i++){
        for(j=0;j<n+6;j++){
            printf("%d ",calculatedMatrix[i][j]);
        }
        printf("\n");
    }
    printf("\n");
}
