/*
Project: K-AST-IR-IO
Author: Raul Torres
Email: raultorrescarvajal@gmail.com
Website: http://raetorresca.wordpress.com
Years: 2015-2018
*/
#include <string.h>
#include "examples_kast/headers.h"
unsigned  int  group01_function01_version01(char  *stringA,  char  *stringB,  unsigned  int  k){
    unsigned int lenA=(unsigned int) strlen(stringA);
    unsigned int lenB=(unsigned int) strlen(stringB);
    if(k>lenA||k>lenB||k<=0){
        return 0;
    }
    unsigned int kernelValue=0;
    char substring[k+1];
    memset(substring,0,(k+1)*sizeof(char));
    char repeatedStrings[lenA][k+1];
    unsigned int repeatedLenght=0;
    char *pointer=0;
    unsigned int i=0;
    for(i=0,pointer=stringA;i<lenA-k+1;i++,pointer++){
        unsigned int countA=0;
        unsigned int countB=0;
        unsigned int j=0;
        unsigned int skip=0;
        const char *tmp=0;
        strncpy(substring,pointer,k);
        for(j=0;j<repeatedLenght;j++){
            if(strcmp(repeatedStrings[j],substring)==0){
                skip=1;
                break;
            }
        }
        if(skip==1){
            continue;
        }
        tmp=stringA;
        while((tmp=strstr(tmp, substring))!=0){
            countA++;
            tmp++;
        }
        tmp=stringB;
        while((tmp=strstr(tmp, substring))!=0){
            countB++;
            tmp++;
        }
        if(countB==0){
            continue;
        }
        if(countA>1){
            strcpy(repeatedStrings[repeatedLenght],substring);
            repeatedLenght++;
        }
        kernelValue+=countA*countB;
    }
    return kernelValue;
}
