# !/bin/bash
SOURCE_FOLDER=${1%/}
TARGET_FOLDER=${2%/}
INCLUDES="" # "-I/path/to/include/folder1 -I/path/to/include/folder1 ..."
KASTSTRING="../kaststring/bin/Debug/kaststring" # "../kaststring/bin/Release/kaststring"
ls ${SOURCE_FOLDER}/*.c | while read fileName # change pattern according to the file structure
do
	echo ${fileName}
	BASENAME=$(basename ${fileName})
	echo ${BASENAME}
	clang -Xclang -ast-dump ${fileName} ${INCLUDES} -fsyntax-only -O0 > ${TARGET_FOLDER}/${BASENAME}.ast
	clang ${fileName} ${INCLUDES} -emit-llvm -S -c -g -o ${TARGET_FOLDER}/${BASENAME}.ll -O0 # -g makes sure the debug information is added to the assembly output
	${KASTSTRING} -ast ${TARGET_FOLDER}/${BASENAME}.ast -str ${TARGET_FOLDER}/${BASENAME}.ast.str -src ${fileName} # --searchAnnotations to search only annotated code
	${KASTSTRING} -lre ${TARGET_FOLDER}/${BASENAME}.ll -str ${TARGET_FOLDER}/${BASENAME}.ll.str -src ${fileName}  -aststr ${TARGET_FOLDER}/${BASENAME}.ast.str # --searchAnnotations to search only annotated code
done
