# !/bin/bash
SOURCE_PATTERN=$1 # provide this argument between "" to prevent the evaluation from bash
SOURCE_LIST=$2 # a text file with the a list of string files
TARGET_FOLDER=${3%/}

KASTCOMPARE="../kastcompare/bin/Debug/kastcompare" # "../kastcompare/bin/Release/kastcompare"

ls ${SOURCE_PATTERN} | while read fileName
do
	BASENAME=$(basename ${fileName})
	echo ${BASENAME}
	
	echo "Attributed"		
	${KASTCOMPARE} -a ${fileName} -L ${SOURCE_LIST} -t attributed > ${TARGET_FOLDER}/${BASENAME}_Attributed.txt
	echo "...done"

	echo "Non-attributed "
	${KASTCOMPARE} -a ${fileName} -L ${SOURCE_LIST} -t no > ${TARGET_FOLDER}/${BASENAME}_NOAttributed.txt
	echo "...done"
done
